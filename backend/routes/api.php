<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('user/login', 'Authentication\AuthenticationController@login');

Route::post('user/register', 'Authentication\AuthenticationController@register');
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::prefix('/annoScolastico')->group(function () {
    Route::get('/list', 'AnnoScolasticoController@list');
    Route::get('/corrente', 'AnnoScolasticoController@corrente');
    Route::get('/{id}', 'AnnoScolasticoController@get');
});


Route::prefix('/ambiti')->group(function () {
    Route::get('/codici/', 'AmbitiTerritorialiController@codici');

    Route::get('/statistiche/', 'AmbitiTerritorialiController@statistiche');
    Route::get('/', 'AmbitiTerritorialiController@index');
});


Route::prefix('/is')->group(function () {
    Route::get('/list', 'ISController@list');

    Route::get('/ciclo1', 'ISController@ciclo1');
    Route::get('/ciclo2', 'ISController@ciclo2');
    Route::get('/odof', 'ISController@odof');
    Route::get('/sovradimensionate', 'ISController@sovradimensionate');
    Route::get('/sottodimensionate', 'ISController@sottodimensionate');
    Route::get('/cerca/{codiceIS}', 'ISController@cercaByCodice');
    Route::get('/{idIS}', 'ISController@get');
});

Route::prefix('sediciclo1')->group(function () {
    Route::get('/', 'SediCiclo1Controller@index');
    Route::get('/istituzione/{codice}', 'SediCiclo1Controller@getSediByIstituzione');
    Route::get('/cerca/{codice}', 'SediCiclo1Controller@cercaByCodice');
    Route::get('/{idSede}', 'SediCiclo1Controller@get');

});

Route::prefix('sediciclo2')->group(function () {
    Route::get('/', 'SediCiclo2Controller@index');
    Route::get('/indirizzi', 'SediCiclo2Controller@indirizzi');
    Route::get('/istituzione/{codice}', 'SediCiclo2Controller@getSediByIstituzione');
    Route::get('/cerca/{codice}', 'SediCiclo2Controller@cercaByCodice');
    Route::get('/provenienze', 'SediCiclo2Controller@provenienze');
    Route::get('/{idSede}', 'SediCiclo2Controller@get');

});

Route::prefix('/cpia')->group(function () {
    Route::get('/dashboard', 'CPIAController@dashboard');
    Route::get('/list', 'CPIAController@list');
    Route::get('/livello1', 'CPIAController@livello1');
    Route::get('/livello2', 'CPIAController@livello2');
});

Route::prefix('/comuni')->group(function () {
    Route::get('/ambiti', 'ComuniController@ambiti');
    Route::get('/distanza', 'ComuniController@distanza');
    Route::get('/vicini', 'ComuniController@vicini');
    Route::get('/', 'ComuniController@list');
    Route::get('/{codiceCatastaleComune}', 'ComuniController@get');
});

Route::prefix('/trendDemografici')->group(function () {
    Route::get('/trend', 'TrendController@trend');
    Route::get('/trendIs', 'TrendController@trendIs');
    Route::get('/trendPe1', 'TrendController@trendPe1');
    Route::get('/trendPe2', 'TrendController@trendPe2');
    Route::get('/popolazioneScolastica', 'TrendController@popolazioneScolastica');
    Route::get('/', 'TrendController@list');
    Route::get('/{codiceCatastaleComune}', 'TrendController@get');
});

Route::prefix('/dispersioneScolastica')->group(function () {
    Route::get('/indicatori', 'DispersioneScolasticaController@indicatori');
    Route::get('/', 'DispersioneScolasticaController@list');
});

Route::prefix('/edifici/')->group(function () {
    Route::get('/nonUtilizzati', 'EdificiController@nonUtilizzati');
});

Route::prefix('/documentazione2021/')->group(function () {
    Route::get('/richiestePareriPiani', 'Documentazione2021Controller@rpp');
});
