<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SSD - Backend</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        pre code {
            text-align: left;
            background-color: #eee;
            border: 1px solid #999;
            display: block;
            padding: 20px;
        }
    </style>
</head>
<body>
<div class="position-ref full-height">


    <div class="content">
        <div class="title m-b-md">
            Importa pareri
        </div>


        @if ($pareri->nFileAggiunti!=0)
            <div class="alert alert-danger alert-block">

                <strong>Sono stati aggiunti con successo {{ $pareri->nFileAggiunti }} files</strong>
            </div>
        @endif

        @if ($pareri->nErrori!=0)
            <div class="alert alert-success alert-block">
                <strong>Si sono verificati {{ $pareri->nErrori }} errori</strong>
            </div>
        @endif

        <div class="flex-center">


        <pre>
            <code>{{$pareri->reportMsg}}</code>
         </pre>

        </div>
    </div>
</div>
</body>
</html>
