#Dimensionamento scolastico Arti


####Import dei dati

```
php artisan importa:ambiti
php artisan importa:comuni
php artisan importa:indirizziStudioCiclo2
php artisan importa:reteScolastica20192020
php artisan importa:reteScolastica20182019
php artisan importa:reteScolastica20172018
php artisan importa:provenienzeAlunni20172018
php artisan importa:provenienzeAlunni20182019
php artisan importa:cpia
php artisan importa:demografie
#php artisan importa:andamenti
php artisan importa:dispersioneScolastica
php artisan importa:edificiNonUtilizzati
php artisan importa:proiezioni
php artisan importa:distanze

```

####Pareri

```
php artisan importa:pareri
```

####Produzione GeoJSON confini
```
php artisan produci:geoJSON
```
