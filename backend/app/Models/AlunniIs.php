<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AlunniIs
 *
 * @property int $id
 * @property string $idIs
 * @property string $totaleAlunniOF
 * @property int $totaleAlunni
 * @property int $totaleAlunniDisabili
 * @property int $totaleClassi
 *
 * @package App\Models
 */
class AlunniIs extends Model
{
    protected $table = 'alunniIs';

    public $timestamps = false;


    protected $fillable = [
        'codiceIs',
        'totaleAlunni',
        'totaleAlunniDisabili',
        'totaleClassi',
    ];

    public function IstituzioneScolastica()
    {
        return $this->belongsTo(IstituzioneScolastica::class, 'codiceIs');
    }


    public static function nuovo(string $codiceIs, AnnoScolastico $codiceAnnoScolastico): ?AlunniIs
    {
        $is = IstituzioneScolastica::trovaCodiceAnno($codiceIs, $codiceAnnoScolastico->id);
        if (!$is) {
            throw new Exception("Is $codiceIs per anno scolastico $codiceAnnoScolastico->label non trovato");
        }
        $model = AlunniIs::query()->where([
            'idIs' => $is->id,
        ])->first();
        if ($model == null) {
            $model = new AlunniIs();
            $model->idIs = $is->id;
            $model->save();
        }
        return $model;
    }
}
