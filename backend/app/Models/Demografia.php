<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Demografia
 * @property string $codiceCatastaleComune
 * @property int $popTot2019
 * //3-13
 * @property int $pop_3_13_2015
 * @property int $pop_3_13_2019
 * @property float $var_3_13_2019_2015
 * @property int $prev_3_13_2023
 * @property float $var_3_13_2023_2019
 *
 * //3-5
 * @property int $pop_3_5_2015
 * @property int $pop_3_5_2019
 * @property float $var_3_5_2019_2015
 * @property int $prev_3_5_2023
 * @property float $var_3_5_2023_2019
 *
 * //6-10
 * @property int $pop_6_10_2015
 * @property int $pop_6_10_2019
 * @property float $var_6_10_2019_2015
 * @property int $prev_6_10_2023
 * @property float $var_6_10_2023_2019
 *
 * //11-13
 * @property int $pop_11_13_2015
 * @property int $pop_11_13_2019
 * @property float $var_11_13_2019_2015
 * @property int $prev_11_13_2023
 * @property float $var_11_13_2023_2019
 *
 * //14-18
 * @property int $pop_14_18_2015
 * @property int $pop_14_18_2019
 * @property float $var_14_18_2019_2015
 * @property int $prev_14_18_2023
 * @property float $var_14_18_2023_2019
 *
 * @package App\Models
 */
class Demografia extends Model
{
    protected $table = 'demografie';

    public $timestamps = false;


    protected $fillable = [

    ];

    public function comune()
    {
        return $this->belongsTo(Comune::class, 'codiceCatastale');
    }


}
