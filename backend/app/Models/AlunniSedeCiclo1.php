<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AlunniSedeCiclo1
 *
 * @property int $id
 * @property string $idSedeCiclo1
 * @property int $totaleAlunni
 * @property string $totaleAlunniOF
 * @property int $totaleAlunniDisabili
 * @property int $totaleClassi
 * @property array dettagli
 *
 * @package App\Models
 */
class AlunniSedeCiclo1 extends Model
{
    //
    protected $table = "alunniSedeCiclo1";

    public $timestamps = false;

    protected $casts = [
        'dettagli' => 'array'
    ];


    /**
     * @param $codiceSede
     * @param AnnoScolastico $annoScolastico
     * @return AlunniSedeCiclo1|null
     * @throws Exception
     */
    public static function nuovo($codiceSede, AnnoScolastico $annoScolastico): ?AlunniSedeCiclo1
    {
        $sede1 = SedeCiclo1::trovaCodiceAnno($codiceSede, $annoScolastico->id);
        if (!$sede1) {
            throw new Exception("Sede $codiceSede non trovata per anno scolastico $annoScolastico->label ");
        }

        $model = AlunniSedeCiclo1::query()->where([
            'idSedeCiclo1' => $sede1->id,
        ])->first();

        if ($model == null) {
            $model = new AlunniSedeCiclo1();
            $model->idSedeCiclo1 = $sede1->id;
            $model->save();
        }
        return $model;
    }
}
