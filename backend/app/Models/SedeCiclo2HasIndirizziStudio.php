<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

/**
 * Created by Reliese Model.
 * Date: Wed, 03 Apr 2019 23:14:26 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model;

/**
 * Class SediHasIndirizziStudio2Ciclo
 *
 * @property string $idSedeCiclo2
 * @property string $indirizzoStudioCiclo2_codice
 *
 * @property IndirizzoStudioCiclo2 $indirizziStudio2Ciclo
 * @property SedeCiclo2 $sede
 *
 * @package App\Models
 */
class SedeCiclo2HasIndirizziStudio extends Model
{
    protected $table = 'sedeCiclo2HasIndirizziStudio';
    protected $primaryKey = ['idSedeCiclo2', 'indirizzoStudioCiclo2_codice'];
    public $incrementing = false;
    public $timestamps = false;


    public static $snakeAttributes = false;

    public function indirizziStudio2Ciclo()
    {
        return $this->belongsTo(IndirizzoStudioCiclo2::class, 'indirizzoStudioCiclo2_codice');
    }

    public function sede()
    {
        return $this->belongsTo(SedeCiclo2::class, 'idSedeCiclo2');
    }
}
