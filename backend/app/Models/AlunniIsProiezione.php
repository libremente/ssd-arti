<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AlunniIs
 *
 * @property int $id
 * @property int $idAnnoProiezione
 * @property string $codiceIs
 * @property int $alunni
 *
 * @package App\Models
 */
class AlunniIsProiezione extends Model
{
    protected $table = 'alunniIsProiezioni';

    public $timestamps = false;


    protected $fillable = [
        'codiceIs',

    ];

    public function IstituzioneScolastica()
    {
        return $this->belongsTo(IstituzioneScolastica::class, 'codiceIs');
    }


    public static function nuovo(string $codiceIs, AnnoScolasticoProiezione $annoProiezione): ?AlunniIsProiezione
    {
        $model = AlunniIsProiezione::query()->where([
            'codiceIs' => $codiceIs,
            'idAnnoProiezione' => $annoProiezione->id
        ])->first();
        if ($model == null) {
            $model = new AlunniIsProiezione();
            $model->codiceIs = $codiceIs;
            $model->idAnnoProiezione = $annoProiezione->id;
            $model->save();
        }
        return $model;
    }
}
