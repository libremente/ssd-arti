<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

/**
 * Created by Reliese Model.
 * Date: Wed, 03 Apr 2019 23:14:26 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CPIATipiSede
 *
 * @property int $id
 * @property string $tipo
 *
 * @property Collection $cPIAPuntiErogazione2Livellos
 *
 * @package App\Models
 */
class CPIATipoSede extends Eloquent
{
    protected $table = 'CPIATipiSede';
    public $incrementing = false;
    public $timestamps = false;
    public static $snakeAttributes = false;

    protected $casts = [
        'id' => 'int'
    ];

    protected $fillable = [
        'tipo'
    ];

    public static function findByTipo($tipoSede): ?CPIATipoSede
    {
        return self::query()->where('tipo', '=', $tipoSede)->first();

    }

    public function cPIAPuntiErogazione2Livellos()
    {
        return $this->hasMany(CPIAPuntoErogazioneLivello2::class, 'tipoSede');
    }
}
