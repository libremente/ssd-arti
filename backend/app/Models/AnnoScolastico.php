<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AnnoScolastico
 * @package App\Models
 *
 * @property int $id
 * @property string $label
 * @property Carbon $inizio
 */
class AnnoScolastico extends Model
{
    protected $table = 'anniScolastici';
    public $timestamps = false;

    protected $dates = ['inizio'];

    /**
     * Crea anno scolastico o restituisce quello già esistente.
     *
     * @param $annoScolasticoInput
     * @return AnnoScolastico|null
     * @throws \Exception
     */
    public static function nuovoAnnoScolastico($annoScolasticoInput): ?AnnoScolastico
    {
        $annoScolastico = self::getAnnoScolastico($annoScolasticoInput);
        if ($annoScolastico != false) {
            $model = AnnoScolastico::query()->where('label', '=', $annoScolastico[0])->first();
            if ($model == null) {
                $model = new AnnoScolastico();
                $model->label = $annoScolastico[0];
                $inizio = new Carbon();
                $inizio->day = 1;
                $inizio->month = 9;
                $inizio->year = $annoScolastico[1];
                $model->inizio = $inizio;
                $model->save();
            }
            return $model;
        } else {
            return null;
        }

    }

    /**
     * @param $annoScolastico string anno scolastico in modo testuale
     * @return bool|array false se il formato dell'anno scolastico non è corretto, altrimenti un array così composto:
     * [
     *  0 => anno scolastico
     *  1 => anno primo quadrimestre
     *  2 => anno secondo quadrimestre
     * ]
     *
     */
    public static function getAnnoScolastico($annoScolastico)
    {
        preg_match('/\b(19|20\d\d)\/(19|20\d\d)\b/', $annoScolastico, $outputArray);
        if (isset($outputArray[0])) {
            return $outputArray;
        }
        return false;

    }

    public function scopeOrdered(Builder $query)
    {
        return $query->orderBy('inizio', 'desc')->get();
    }

    public static function corrente(): ?AnnoScolastico
    {
        return AnnoScolastico::query()->orderBy('inizio', 'desc')->first();
    }
}
