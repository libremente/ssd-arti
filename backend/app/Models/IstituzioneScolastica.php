<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

/**
 * Created by Reliese Model.
 * Date: Wed, 03 Apr 2019 23:14:26 +0000.
 */

namespace App\Models;

use App\Repositories\ISRepository;
use Illuminate\Database\Eloquent\Collection;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class IstituzioniScolastiche
 *
 * @property int $id
 * @property int $idAnnoScolastico
 * @property string $codice
 * @property string $codiceAmbito
 * @property int $tipoIstituto
 * @property string $denominazione
 * @property string $comuneSedeDirigenza
 * @property string $codiceCatastaleComuneDirigenza
 *
 * @property AnnoScolastico $annoScolastico
 * @property TipoIstituto $tipiIstituto
 * @property AmbitoTerritoriale $ambitiTerritoriali
 * @property CPIA $cpia
 * @property Collection $sediCiclo1
 * @property Collection $sediCiclo2
 * @property Collection $alunni
 *
 * @package App\Models
 */
class IstituzioneScolastica extends Eloquent
{

    protected $table = 'istituzioniScolastiche';

    public $timestamps = false;
    public static $snakeAttributes = false;

    protected $casts = [
        'tipoIstituto' => 'int',
    ];

    protected $fillable = [
        'codiceAmbito',
        'tipoIstituto',
        'denominazione',
        'comuneSedeDirigenza',
    ];

    public static function trovaCodiceAnno(string $codice, int $idAnnoScolastico): ?IstituzioneScolastica
    {
        return self::query()
            ->where('codice', '=', $codice)
            ->where('idAnnoScolastico', '=', $idAnnoScolastico)
            ->first();
    }

    public function annoScolastico()
    {
        return $this->belongsTo(AnnoScolastico::class, 'idAnnoScolastico');
    }

    public function tipoIstituto()
    {
        return $this->belongsTo(TipoIstituto::class, 'tipoIstituto');
    }

    public function ambitoTerritoriale()
    {
        return $this->belongsTo(AmbitoTerritoriale::class, 'codiceAmbito');
    }

    public function cpia()
    {
        return $this->belongsTo(CPIA::class, 'id');
    }

    public function sediCiclo1()
    {
        return $this->hasMany(SedeCiclo1::class, 'idIstituzioneScolastica');
    }


    public function sediCiclo2()
    {
        return $this->hasMany(SedeCiclo2::class, 'idIstituzioneScolastica');
    }

    public function alunni()
    {
        return $this->hasOne(AlunniIs::class, "idIs");
    }


    public function isSottoDimensionata()
    {
        /** @var AlunniIs $alunniIs */
        $alunniIs = $this->alunni()->get();
        $limite = ISRepository::limiteSottoDimensionataPerNomeComune(strtoupper($this->comuneSedeDirigenza));
        return $alunniIs->totaleAlunni < $limite;
    }

    public function isSovraDimensionata()
    {
        $alunniIs = $this->alunni()->get();
        return $alunniIs->totaleAlunni > ISRepository::limiteSovraDimensionata();
    }

}
