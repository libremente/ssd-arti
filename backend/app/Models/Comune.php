<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

/**
 * Created by Reliese Model.
 * Date: Wed, 03 Apr 2019 23:14:26 +0000.
 */

namespace App\Models;

use Phaza\LaravelPostgis\Eloquent\PostgisTrait;
use Phaza\LaravelPostgis\Geometries\Polygon;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Comune
 *
 * @property string $codiceCatastale
 * @property string $nome
 * @property string $provincia
 * @property string $regione
 * @property string $cap
 * @property string $codiceIstat
 * @property Polygon $confine
 * @property string $ambitoTerritoriale
 * @property integer $numero_distretto
 *
 * @property distretto
 * @package App\Models
 */
class Comune extends Eloquent
{
    use PostgisTrait;

    protected $table = 'comuni';
    protected $primaryKey = 'codiceCatastale';
    public $incrementing = false;
    public $timestamps = false;
    public static $snakeAttributes = false;

    protected $fillable = [
        'nome',
        'provincia',
        'regione',
        'cap'
    ];


    protected $casts = [
//        'confine' => 'Mult',
    ];

    protected $postgisFields = [
        'confine',
    ];

    protected $postgisTypes = [

        'confine' => [
            'geomtype' => 'geometry',
            'srid' => 27700
        ]
    ];

    public function ambito()
    {
        return $this->belongsTo('App\Models\AmbitoTerritoriale', 'ambitoTerritoriale')->select(array('codice', 'numero', 'provincia'));
    }

    public function ambitoAllData()
    {
        return $this->belongsTo('App\Models\AmbitoTerritoriale', 'ambitoTerritoriale');
    }

    public function distretto()
    {
        return $this->belongsTo('App\Models\Distretto', 'numero_distretto');
    }
}
