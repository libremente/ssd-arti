<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

/**
 * Created by Reliese Model.
 * Date: Wed, 03 Apr 2019 23:14:26 +0000.
 */

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Collection;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TipologieScuola
 *
 * @property string $tipologiaScuola
 * @property string $descrizione
 *
 * @property Collection $sediCiclo1s
 * @property Collection $sediCiclo2s
 *
 * @package App\Models
 */
class TipologiaScuola extends Eloquent
{
    protected $table = 'tipologieScuola';
    protected $primaryKey = 'tipologiaScuola';
    protected $keyType = 'string';
    public $incrementing = false;
    public $timestamps = false;
    public static $snakeAttributes = false;

    protected $fillable = [
        'descrizione'
    ];

    public function sediCiclo1()
    {
        return $this->hasMany(SedeCiclo1::class, 'tipologiaScuola');
    }

    public function sediCiclo2()
    {
        return $this->hasMany(SedeCiclo2::class, 'tipologiaScuola');
    }

    /**
     * @param $tipologiaScuola
     * @return string
     * @throws Exception
     */
    public static function getDescrizione($tipologiaScuola)
    {
        switch ($tipologiaScuola) {
            case "AA":
                return "scuola dell’infanzia";
            case "EE":
                return "scuola primaria";
            case "MM":
                return "secondaria di I grado";
            case "SS":
                return "secondaria di II grado";
            case "DD":
                return "direzione didattica";
            default:
                throw new Exception("Tiplogia scuola $tipologiaScuola non valida");
        }
    }

    /**
     * @param $tipologiaScuola
     * @return int
     * @throws Exception
     */
    public static function getCiclo($tipologiaScuola)
    {
        switch ($tipologiaScuola) {
            case "AA":
            case "EE":
            case "MM":
            case "DD":
                return 1;
            case "SS":
                return 2;
            default:
                throw new Exception("Tiplogia scuola $tipologiaScuola non valida");
        }
    }
}
