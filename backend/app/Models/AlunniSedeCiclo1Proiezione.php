<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AlunniSedeCiclo1
 *
 * @property int $id
 * @property int $idAnnoProiezione
 * @property string $codiceIs
 * @property string $codicePe
 * @property int $alunni
 *
 * @package App\Models
 */
class AlunniSedeCiclo1Proiezione extends Model
{
    //
    protected $table = "alunniSedeCiclo1Proiezioni";

    public $timestamps = false;

    protected $casts = [
        'dettagli' => 'array'
    ];


    /**
     * @param $codicePe
     * @param $codiceIs
     * @param AnnoScolasticoProiezione $annoProiezione
     * @return AlunniSedeCiclo1Proiezione|null
     */
    public static function nuovo($codicePe, $codiceIs, AnnoScolasticoProiezione $annoProiezione): ?AlunniSedeCiclo1Proiezione
    {


        $model = AlunniSedeCiclo1Proiezione::query()->where([
            'codicePe' => $codicePe,
            'idAnnoProiezione' => $annoProiezione->id
        ])->first();

        if ($model == null) {
            $model = new AlunniSedeCiclo1Proiezione();
            $model->idAnnoProiezione = $annoProiezione->id;
            $model->codiceIs = $codiceIs;
            $model->codicePe = $codicePe;
            $model->save();
        }
        return $model;
    }
}
