<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Phaza\LaravelPostgis\Eloquent\PostgisTrait;
use Phaza\LaravelPostgis\Geometries\Point;

/**
 * Class EdificiNonUtilizzati
 *
 * @property int $id
 * @property string $codiceCatastaleComune
 * @property string $comune
 * @property string $indirizzo
 * @property string $note
 * @property string $codiceEdificio
 * @property Point $coordinate
 *
 * @property Comune $comune
 * @property int idAnnoScolastico
 *
 * @package App\Models
 */
class EdificioNonUtilizzato extends Model
{
    use PostgisTrait;

    protected $table = 'edificiNonUtilizzati';


    public $timestamps = false;
    public static $snakeAttributes = false;

    protected $casts = [
        'coordinate' => 'geometry',
    ];

    protected $fillable = [
        'id',
        'codiceCatastaleComune',
        'coordinate',
        'comune',
        'indirizzo',

        'codiceEdificio'
    ];


    protected $postgisFields = [
        'coordinate',
    ];

    protected $postgisTypes = [

        'coordinate' => [
            'geomtype' => 'geometry',
            'srid' => 27700
        ]
    ];


    public function comune()
    {
        $this->belongsTo(Comune::class, 'codiceCatastaleComune');
    }
}
