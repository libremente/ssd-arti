<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

/**
 * Created by Reliese Model.
 * Date: Wed, 03 Apr 2019 23:14:26 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Phaza\LaravelPostgis\Eloquent\PostgisTrait;
use Phaza\LaravelPostgis\Geometries\Point;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SediCiclo2
 *
 * @property int $id
 * @property string $codice
 * @property int $idIstituzioneScolastica
 * @property Point $coordinate
 * @property string $codiceCatastaleComune
 * @property string $comune
 * @property string $indirizzo
 * @property string $denominazione
 * @property string $tipo
 * @property bool $sedeDirezioneAmministrativa
 * @property string $verticalizzazione
 * @property string $tipologiaScuola
 * @property string $codiceEdificio
 *
 *
 * @property IstituzioneScolastica $istituzioniScolastica
 * @property array $alunni
 * @property TipologiaScuola $tipologieScuola
 * @property Collection $cPIAPuntiErogazione2Livellos
 * @property Collection $distribuzionePlessoResidenzaAlunnis
 * @property Collection $sediHasIndirizziStudio2Ciclos
 * @property mixed $istituzioneScolastica
 * @property Collection $indirizziDiStudio
 *
 * @package App\Models
 */
class SedeCiclo2 extends Eloquent
{

    use PostgisTrait;


    protected $table = 'sediCiclo2';

    public $timestamps = false;
    public static $snakeAttributes = false;

    protected $casts = [
        'coordinate' => 'geometry',
        'sedeDirezioneAmministrativa' => 'bool'
    ];

    protected $postgisFields = [
        'coordinate',
    ];

    protected $postgisTypes = [

        'coordinate' => [
            'geomtype' => 'geometry',
            'srid' => 27700
        ]
    ];

    protected $fillable = [
        'codiceIstituzione',
        'coordinate',
        'codiceCatastaleComune',
        'comune',
        'indirizzo',
        'denominazione',
        'tipo',
        'sedeDirezioneAmministrativa',
        'verticalizzazione',
        'tipologiaScuola',
        'codiceEdificio',
        'indirizziDiStudio'
    ];


    public static function trovaCodiceAnno($codiceSede, int $idAnnoScolastico): ?SedeCiclo2
    {
        $query = DB::raw('select sede.* from "istituzioniScolastiche" as ist join "sediCiclo2" as sede on ist.id = sede."idIstituzioneScolastica"  where "idAnnoScolastico" = ? AND sede.codice = ? LIMIT 1');
        $items = SedeCiclo2::fromQuery($query, [$idAnnoScolastico, $codiceSede]);
        if (sizeof($items) == 0) {
            return null;
        }
        return $items[0];
    }

    public function istituzioneScolastica()
    {
        return $this->belongsTo(IstituzioneScolastica::class, 'idIstituzioneScolastica');
    }

    public function tipologiaScuola()
    {
        return $this->belongsTo(TipologiaScuola::class, 'tipologiaScuola');
    }

    public function CPIAPuntiErogazione2Livello()
    {
        return $this->hasMany(CPIAPuntoErogazioneLivello2::class, 'codiceMeccanograficoIS');
    }

    public function distribuzioneResidenzaAlunni()
    {
        return $this->hasOne(DistribuzioneResidenzaAlunniSedeCiclo2::class, 'idSedeCiclo2');
    }

    public function indirizziDiStudio()
    {
        return $this->belongsToMany(IndirizzoStudioCiclo2::class, SedeCiclo2HasIndirizziStudio::class,
            'idSedeCiclo2', 'indirizzoStudioCiclo2_codice');
    }

    public function alunni()
    {
        return $this->hasOne(AlunniSedeCiclo2::class, 'idSedeCiclo2');
    }

    /**
     * Per la sede corrente, restituisce il totale degli alunni nell'anno scolastico desiderato
     *
     * @return array|null
     */
    public function getTotaleAlunniPerSede()
    {
        $query = DB::raw('select  "idSedeCiclo2",
                            sum("totaleAlunni") as "totaleAlunni",
                            sum("totaleAlunniDisabili") as "totaleAlunniDisabili",
                            sum("totaleClassi") as "totaleClassi"
                            from ssd."alunniSedeCiclo2"
                            where
                                "idSedeCiclo2" = :idSedeCiclo2
                            group by "idSedeCiclo2";');
        $result = Db::select($query, [
            "idSedeCiclo2" => $this->id
        ]);
        if (isset($result[0])) {
            return $result[0];

        } else {
            return null;
        }
    }
}
