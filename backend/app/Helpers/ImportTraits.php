<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Helpers;


use App\Models\AlunniIs;
use App\Models\AlunniSedeCiclo1;
use App\Models\AlunniSedeCiclo2;
use App\Models\AnnoScolastico;
use App\Models\IstituzioneScolastica;
use App\Models\SedeCiclo1;
use App\Models\SedeCiclo2;
use App\Models\TipoIstituto;
use App\Models\TipologiaScuola;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

trait ImportTraits
{
    private function inserisciTipoIstituto($id, $nome)
    {
        $t = new TipoIstituto();
        $t->id = $id;
        $t->denominazione = $nome;
        try {
            $t->save();
        } catch (Exception $ignored) {
        }

    }

    private function importaTipiIstituti(array $istituzioniScolastiche)
    {
        $tipi = array_unique(array_reduce($istituzioniScolastiche, function ($carry, $item) {
            $tipoIs = trim($item['tipoIS']);
            $carry[] = $tipoIs;
            return $carry;
        }, []));

        DB::beginTransaction();
        $id = 0;
        foreach ($tipi as $tipo) {
//            print "inserting $id, $tipo\n";
            $this->inserisciTipoIstituto($id++, $tipo);
        }
        DB::commit();
    }

    private function eliminaAnnoScolastico(string $annoScolasticoStr)
    {
        $annoScolasticoModel = AnnoScolastico::query()->where(['label' => $annoScolasticoStr])->first();
        if ($annoScolasticoModel != null) {
            $annoScolasticoModel->truncate();
        }
    }


    public function inserisciIS($is, AnnoScolastico $annoScolastico)
    {
        if (isset($is['tipoIS'])) {
            $idTipoIs = TipoIstituto::query()->where(['denominazione' => $is['tipoIS']])->first()['id'];
        } else {
            $idTipoIs = null;
        }


        if (isset($is['codiceAmbito'])) {
            $codiceAmbito = $is['codiceAmbito'];
        } else {
            $codiceAmbito = null;
            $nomeComune = $is['comuneSedeDirigenza'];
            if ($nomeComune == null || $nomeComune == "") {
                $this->warn("Comune mancante per istituzione ${is['codiceIS']}, impossibile risalire al codice ambito");
            } else {

                $comune = ImportHelpers::comuneByNome($nomeComune);
                if ($comune == null) {
                    $this->warn("Comune non trovato ${nomeComune}, impossibile risalire al codice ambito");
                    $codiceAmbito = null;
                } else {
                    $codiceAmbito = $comune->ambitoTerritoriale;
                }
            }
        }
        try {
            $comune = ImportHelpers::comuneByNome($is['comuneSedeDirigenza']);
            $codiceCatastale = $comune->codiceCatastale;
        } catch (Exception $e) {
            $codiceCatastale = null;
        }

        $isModel = new IstituzioneScolastica();
        $isModel->codice = $is['codiceIS'];
        $isModel->denominazione = $is['denominazioneIs'];
        $isModel->idAnnoScolastico = $annoScolastico->id;
        $isModel->comuneSedeDirigenza = $is['comuneSedeDirigenza'];
        $isModel->codiceCatastaleComuneDirigenza = $codiceCatastale;
        $isModel->codiceAmbito = $codiceAmbito;
        $isModel->tipoIstituto = $idTipoIs;
        $isModel->saveOrFail();

    }

    private function inserisciSedi(array $sediVert, AnnoScolastico $annoScolastico)
    {
        foreach ($sediVert as $verticalizzazione => $sedi) {
//            print "'$verticalizzazione'\n";
            switch ($verticalizzazione) {
                case "I Ciclo":
                    foreach ($sedi as $sede) {
                        $this->inserisciSedeCiclo1($sede, $annoScolastico);
                    }
                    break;
                case "II Ciclo":
                    foreach ($sedi as $sede) {
                        $this->inserisciSedeCiclo2($sede, $annoScolastico);
                    }
                    break;
                default:
                    foreach ($sedi as $sede) {
                        $ciclo = TipologiaScuola::getCiclo($sede['tipologiaScuola']);
                        if ($ciclo == 1) {
                            $this->inserisciSedeCiclo1($sede, $annoScolastico);
                        } else {
                            $this->inserisciSedeCiclo2($sede, $annoScolastico);
                        }

                    }
                    break;
            }
        }
    }

    /**
     * @param array $sediVert
     * @param AnnoScolastico $annoScolastico
     * @param bool $skipCiclo2
     * @throws Exception
     */
    private function inserisciAlunniSedi(array $sediVert, AnnoScolastico $annoScolastico, $skipCiclo2 = false)
    {
        foreach ($sediVert as $verticalizzazione => $sedi) {
//            print "'$verticalizzazione'\n";
            switch ($verticalizzazione) {
                case "I Ciclo":
                    foreach ($sedi as $sede) {
                        $this->inserisciAlunniCiclo1($sede, $annoScolastico);
                    }
                    break;
                case "II Ciclo":
                    if ($skipCiclo2) continue;
                    foreach ($sedi as $sede) {
                        $this->inserisciAlunniCiclo2($sede, $annoScolastico);
                    }
                    break;
                default:
                    foreach ($sedi as $sede) {
                        $ciclo = TipologiaScuola::getCiclo($sede['tipologiaScuola']);
                        if ($ciclo == 1) {
                            $this->inserisciAlunniCiclo1($sede, $annoScolastico);

                        } else {
                            if ($skipCiclo2) continue;
                            $this->inserisciAlunniCiclo2($sede, $annoScolastico);

                        }

                    }
                    break;
            }
        }
    }

    protected function inserisciSedeCiclo1($sedeData, AnnoScolastico $annoScolastico)
    {
        if (($is = IstituzioneScolastica::trovaCodiceAnno($sedeData['codiceIS'], $annoScolastico->id)) == null) {
            $this->error("Impossibile aggiungere la sede " . $sedeData['codiceSede'] . " poiché manca l'istituzione scolastica " . $sedeData['codiceIS']);
            return;
        }

        $codiceCatastale = @ImportHelpers::comuneByNome($sedeData['comuneSede'])->codiceCatastale ?: null;

        $this->assicuraTipologiaScuola($sedeData['tipologiaScuola']);
        $sede = new SedeCiclo1();
        $sede->denominazione = $sedeData['denominazione'];
        $sede->codice = $sedeData['codiceSede'];
        $sede->idIstituzioneScolastica = $is->id;
        $sede->tipo = @$sedeData['tipo'] ?: null;
        $sede->sedeDirezioneAmministrativa = $sedeData['isDirezioneAmministrativa'];
        $sede->verticalizzazione = $sedeData['verticalizzazione'];
        $sede->codiceEdificio = $sedeData['codiceEdificio'];
        $sede->coordinate = ImportHelpers::makePoint($sedeData['lat'], $sedeData['lon']);
        $sede->indirizzo = $sedeData['indirizzoPlesso'];
        $sede->tipologiaScuola = $sedeData['tipologiaScuola'];
        $sede->codiceCatastaleComune = $codiceCatastale;
        $sede->comune = $sedeData['comuneSede'];
        try {
            $sede->save();
        } catch (QueryException $e) {
            $this->error($e->getMessage());
            print "\n";
        }
        if ($sede->coordinate == null) {
            $this->alert("Le coordinate della sede" . $sedeData['codiceSede'] . " non sono disponibili");
        }
    }

    protected function inserisciSedeCiclo2($sedeData, AnnoScolastico $annoScolastico)
    {
        if (($is = IstituzioneScolastica::trovaCodiceAnno($sedeData['codiceIS'], $annoScolastico->id)) == null) {
            $this->error("Impossibile aggiungere la sede " . $sedeData['codiceSede'] . " poiché manca l'istituzione scolastica " . $sedeData['codiceIS']);
            return;
        }
        $codiceCatastale = @ImportHelpers::comuneByNome($sedeData['comuneSede'])->codiceCatastale ?: null;


        $this->assicuraTipologiaScuola($sedeData['tipologiaScuola']);

        $sede = new SedeCiclo2();
        $sede->denominazione = $sedeData['denominazione'];
        $sede->codice = $sedeData['codiceSede'];
        $sede->idIstituzioneScolastica = $is->id;
        $sede->tipo = @$sedeData['tipo'] ?: null;
        $sede->sedeDirezioneAmministrativa = $sedeData['isDirezioneAmministrativa'];
        $sede->verticalizzazione = $sedeData['verticalizzazione'];
        $sede->codiceEdificio = $sedeData['codiceEdificio'];
        $sede->coordinate = ImportHelpers::makePoint($sedeData['lat'], $sedeData['lon']);
        $sede->indirizzo = $sedeData['indirizzoPlesso'];
        $sede->tipologiaScuola = $sedeData['tipologiaScuola'];
        $sede->codiceCatastaleComune = $codiceCatastale;
        $sede->comune = $sedeData['comuneSede'];

        try {
            $sede->save();
        } catch (QueryException $e) {
            $this->error($e->getMessage());
            print "\n";
        }
        if ($sede->coordinate == null) {
            $this->alert("Le coordinate della sede" . $sedeData['codiceSede'] . " non sono disponibili");
        }
        return $sede;
    }

    private function assicuraTipologiaScuola($tipologiaScuola)
    {

        if ($tipologiaScuola == "") return;

        $model = TipologiaScuola::query()->find($tipologiaScuola);
        if ($model == null) {
            $model = new TipologiaScuola();
            $model->tipologiaScuola = $tipologiaScuola;
            $model->descrizione = TipologiaScuola::getDescrizione($tipologiaScuola);
            $model->save();
        }
    }

    private function inserisciAlunniIs($is, AnnoScolastico $annoScolastico)
    {
        $alunniIs = AlunniIs::nuovo($is['codiceIS'], $annoScolastico);
        $organicoDiFatto = array_key_exists('organicoFatto', $is) ? $is['organicoFatto'] : null;

        $alunniIs->totaleAlunni = $is['nAlunni'];
        $alunniIs->totaleClassi = $is['nClassi'];
        $alunniIs->totaleAlunniDisabili = $is['nDisabili'];
        $alunniIs->totaleAlunniOF = $organicoDiFatto;

        $alunniIs->save();

    }

    /**
     * @param $item
     * @param AnnoScolastico $annoScolastico
     * @throws QueryException
     * @throws Exception
     */
    private function inserisciAlunniCiclo1($item, AnnoScolastico $annoScolastico)
    {
        $organicoDiFatto = array_key_exists('organicoFatto', $item) ? $item['organicoFatto'] : null;

        $alunniSede1 = AlunniSedeCiclo1::nuovo($item['codiceSede'], $annoScolastico);
        $alunniSede1->totaleAlunni = $item['nAlunni'];
        $alunniSede1->totaleAlunniOF = $organicoDiFatto;
        $alunniSede1->totaleClassi = $item['nClassi'];
        $alunniSede1->totaleAlunniDisabili = $item['nDisabili'];
        $alunniSede1->dettagli = $item['dettagli'];
        $alunniSede1->save();

    }

    /**
     * @param $item
     * @param AnnoScolastico $annoScolastico
     * @throws QueryException
     * @throws Exception
     */
    private function inserisciAlunniCiclo2($item, AnnoScolastico $annoScolastico)
    {
        $alunniSede2 = AlunniSedeCiclo2::nuovo($item['codiceSede'], $item['codiceIndirizzo'], $annoScolastico);
        $organicoDiFatto = array_key_exists('organicoFatto', $item) ? $item['organicoFatto'] : null;
        $alunniSede2->totaleAlunni = $item['nAlunni'];
        $alunniSede2->totaleAlunniOF = $organicoDiFatto;
        $alunniSede2->totaleClassi = $item['nClassi'];
        $alunniSede2->totaleAlunniDisabili = $item['nDisabili'];
        $alunniSede2->dettagli = $item['dettagli'];

        $alunniSede2->save();
    }
}
