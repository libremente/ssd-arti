<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Helpers;


use App\Models\AnnoScolastico;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ODOF1920_AGGImporter
{

    use ImportTraits;


    private const HEADER = [
        'A' => "Lat",
        'B' => "Lon",
        'C' => "Ciclo",
        'D' => "Provincia",
        'E' => "CodED1819",

        'F' => "CodIS1920",
        'G' => "DenIS1920",
        'H' => "ComuneIS",
        'I' => "IndirIS",
        'J' => "CodPE1920",
        'K' => "DenPE1920",
        'L' => "ComunePE",
        'M' => "IndirPE",
        'N' => "DA1819",
        'O' => "Tipo",

    ];
    const HEADER_ROW_IDX = 1;

    /**
     * @var string
     */
    private $fileName;
    /**
     * @var Worksheet
     */
    private $worksheet;

    public function handle()
    {
        $annoScolasticoStr = "2019/2020";
        $this->eliminaAnnoScolastico($annoScolasticoStr);
        $annoScolastico = AnnoScolastico::nuovoAnnoScolastico($annoScolasticoStr);
        $this->fileName = __DIR__ . "/../../dataset/20192020/19_20_OD+OF_agg.xlsx";


        $this->loadFile();
        $sedi = $this->loadSedi();
        $istituzioni = [];
        foreach ($sedi as $sediPerVerticalizzazione) {
            foreach ($sediPerVerticalizzazione as $sede) {
                $codiceIS = $sede['codiceIS'];
                //sfrutto l'indice per garantire l'unicità
                if (isset($istituzioni[$codiceIS])) {
                    $istituzione = $istituzioni[$codiceIS];

                    $organicoDiFattoSede = array_key_exists('organicoFatto', $sede) ? $sede['organicoFatto'] : null;

                    if ($istituzione['nAlunni'] != null) {
                        $istituzione['nAlunni'] += $sede['nAlunni'];
                    } else {
                        $istituzione['nAlunni'] = $sede['nAlunni'];
                    }
                    if ($istituzione['nClassi'] != null) {
                        $istituzione['nClassi'] += $sede['nClassi'];
                    } else {
                        $istituzione['nClassi'] = $sede['nClassi'];
                    }
                    if ($istituzione['nDisabili'] != null) {
                        $istituzione['nDisabili'] += $sede['nDisabili'];
                    } else {
                        $istituzione['nDisabili'] = $sede['nDisabili'];
                    }
                    if ($organicoDiFattoSede != null) {
                        if ($istituzione['organicoFatto'] != null) {
                            $istituzione['organicoFatto'] += $organicoDiFattoSede;
                        } else {
                            $istituzione['organicoFatto'] = $organicoDiFattoSede;
                        }
                    }
                    $istituzioni[$codiceIS] = $istituzione;
                } else {
                    $istituzioni[$codiceIS] = $sede;
                }
            }
        }


        foreach ($istituzioni as $istituzione) {
            $this->inserisciIS($istituzione, $annoScolastico);
            $this->inserisciAlunniIs($istituzione, $annoScolastico);
        }

        $this->inserisciSedi($sedi, $annoScolastico);
        $this->inserisciAlunniSedi($sedi, $annoScolastico, true);


    }

    /**
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function loadFile()
    {


        $reader = IOFactory::createReaderForFile($this->fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($this->fileName);


        $worksheet = $spreadsheet->getSheet(0);


        //perform "format" check
        ImportHelpers::performFormatCheck($worksheet, self::HEADER);

        $this->worksheet = $worksheet;


    }

    /**
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function loadSedi()
    {
        $sedi = [];
        $worksheet = $this->worksheet;

        foreach ($worksheet->getRowIterator(self::HEADER_ROW_IDX + 2) as $row) {
            $rowIndex = $row->getRowIndex();

            $codiceIS = $worksheet->getCell("F$rowIndex")->getValue();
            if (ImportHelpers::checkIfEmptyCell($codiceIS)) {
                continue;
            }
            $codiceSede = $worksheet->getCell("J$rowIndex")->getValue();
            $lat = $worksheet->getCell("A$rowIndex")->getValue();
            $lon = $worksheet->getCell("B$rowIndex")->getValue();
            if ($lat == "ND" || $lon == "ND") {
                $this->errori[] = "Coordinate non valide ($lat,$lon) per $codiceSede";
                continue;
            }
            $denominazione = $worksheet->getCell("K$rowIndex")->getValue();

            $isDirezioneAmministrativa = (boolean)$worksheet->getCell("N$rowIndex")->getValue() != "";


            $verticalizzazione = $worksheet->getCell("C$rowIndex")->getValue();
            $tipologiaScuola = trim($worksheet->getCell("O$rowIndex")->getValue());
            $codiceEdificio = $worksheet->getCell("E$rowIndex")->getValue();
            $comuneSede = $worksheet->getCell("L$rowIndex")->getValue() ?: "N/D";
            $indirizzoPlesso = $worksheet->getCell("M$rowIndex")->getValue();
            $comuneSedeDirigenza = $worksheet->getCell("H$rowIndex")->getValue();
//            $indirizzoIs =  $worksheet->getCell("L$rowIndex")->getValue();
            $denominazioneIs = $worksheet->getCell("G$rowIndex")->getValue();
            $organicoFatto = null;

            $lat = ImportHelpers::isCampoNonDisponibile($lat) ? null : $lat;
            $lon = ImportHelpers::isCampoNonDisponibile($lon) ? null : $lon;
            $verticalizzazioneIdx = $verticalizzazione;
            if ($verticalizzazioneIdx == "0") $verticalizzazioneIdx = "nd";

            $nAlunni = $worksheet->getCell("P$rowIndex")->getValue();
            $nClassi = null;
            $nDisabili = null;
            $codiceIndirizzo = null;

            $dettagli = null;

            $sedi[$verticalizzazioneIdx][] = compact(
                'codiceSede',
                'denominazione',
                'codiceIS',
                'isDirezioneAmministrativa',
                'verticalizzazione',
                'tipologiaScuola',
                'codiceEdificio',
                'lat',
                'lon',
                'comuneSede',
                'indirizzoPlesso',
                'comuneSedeDirigenza',
                'denominazioneIs',
                'codiceIndirizzo',
                'nAlunni',
                'nClassi',
                'nDisabili',
                'dettagli',
                'organicoFatto'
            );
        }

        return $sedi;

    }

    function warn($msg)
    {
        print $msg;
    }

    function error($msg)
    {
        print $msg;
    }

}
