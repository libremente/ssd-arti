<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Helpers;

use App\Models\FileDocumentazione2021;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\IOFactory;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;

class PareriImporter
{

    public $scanDir;
    public $reportMsg = "";
    public $nErrori = 0;
    public $nFileAggiunti = 0;
    public $nRecordAggiunti = 0;
    public $print = false;


    const HEADER_CSV = [
        "Provincia",
        "Codice istituzione scolastica",
        "Denominazione istituzione scolastica",
        "Comune/i punti di erogazione"
    ];

    public function __construct($dirToScan = null, $printDebugMsg = false)
    {

        $this->scanDir = $dirToScan;
        $this->print = $printDebugMsg;

        if ($dirToScan == null) {
            $searchPath = env("PARERI_SEARCH_PATH", "/sftpusers/datipuglia/");
            $this->scanDir = $searchPath;
        } else {
            $this->scanDir = $dirToScan;
        }

        DB::beginTransaction();
        DB::table('fileDocumentazione2021')->truncate();

        $csvFilesInfo = $this->trovaCsvInDir();
        $this->report("Csv trovati: " . count($csvFilesInfo));
        /** @var SplFileInfo $file */
        foreach ($csvFilesInfo as $file) {
            $fileNameWithRelativePath = substr($file->getPathname(), strlen($this->scanDir));
            if (!$this->aggiornatoRecentemente($file)) {
                $this->report("Inizio processing " . $fileNameWithRelativePath);
                try {
                    $this->importaDaCsv($file->getPathname());
                } catch (Exception $exception) {
                    $this->report($exception->getMessage());
                    $this->nErrori++;
                }
                $this->report("fine processing " . $fileNameWithRelativePath);
                $this->report("=========================================");
            } else {
                $this->report("Errore: file ignorato perché troppo recente " . $fileNameWithRelativePath);
                $this->nErrori++;
            }
        }
    }


    public function trovaCsvInDir()
    {

        $csvFilesInfo = [];
        $dir_iterator = new RecursiveDirectoryIterator($this->scanDir);
        $iterator = new RecursiveIteratorIterator($dir_iterator, RecursiveIteratorIterator::SELF_FIRST);
        /** @var SplFileInfo $file */
        foreach ($iterator as $file) {
            if ($file->isFile()) {
                if ($file->getExtension() === "csv") {
                    $csvFilesInfo[] = $file;
                    $msg = "Csv trovato: " . substr($file->getPathname(), strlen($this->scanDir)) . ": " . $file->getSize() . " B; modified " . date("Y-m-d H:i:s", $file->getMTime());
                    $this->report($msg);
                }
            }
        }
        return $csvFilesInfo;
    }

    public function importaDaCsv($csvPathName)
    {
        $inputDir = dirname($csvPathName);
        $csvExt = pathinfo($csvPathName, PATHINFO_EXTENSION);

        if ($csvExt === "csv") {
            $matriceFiles = $this->leggiMatriceFileCsv($csvPathName);
        } else {
            $this->nErrori++;
            $this->report("Formato $csvExt non supportato");
            return;
        }

        $re = '/(?<=datipuglia\/)(.*)(?=\/ssd)/m';


        preg_match_all($re, $inputDir, $matches, PREG_SET_ORDER, 0);
        if (isset($matches[0])) {
            $userDir = $matches[0][0] . "/";
        } else {
            $userDir = "";
        }

        foreach ($matriceFiles['data'] as $line) {

            $skipLine = false;
            for ($i = 0; $i < 4; $i++) {
                if (!isset($line[self::HEADER_CSV[$i]])) {
                    $skipLine = $skipLine || true;
                }
            }
            if ($skipLine) {
                continue;
            }

            $provincia = $line[self::HEADER_CSV[0]];
            $codice = $line[self::HEADER_CSV[1]];
            $denominazione = $line[self::HEADER_CSV[2]];
            $nomeComune = $line[self::HEADER_CSV[3]];

            if ($provincia == "" || $codice == "" || $nomeComune == "") {
                continue;
            }

            $this->report("\t+ $provincia $nomeComune $codice $denominazione ");
            foreach ($line as $nomeFile => $filePath) {
                if (in_array($nomeFile, self::HEADER_CSV)) {
                    continue;
                }
//                $this->report("$nomeFile => $filePath\n");
                if ($filePath != null) {
                    $dstFilePath = "documentazione2021/$userDir$filePath";
                    try {
                        $this->importaFile($inputDir, $filePath, $dstFilePath);
                        $this->nFileAggiunti++;
                    } catch (Exception $exception) {
                        $this->nErrori++;
                        $this->report("\t\tErrore: file $filePath non trovato \n");
                        $dstFilePath = null;
                    }
                } else {
                    $dstFilePath = null;
                }
                $model = new  FileDocumentazione2021();
                $model->provincia = $provincia;
                $model->comune = $nomeComune;
                $model->codice = $codice;
                $model->denominazione = $denominazione;
                $model->fileName = $nomeFile;
                $model->filePath = $dstFilePath;
                try {
                    $model->save();
                    $this->nRecordAggiunti++;
                } catch (Exception $exception) {
                    $this->nErrori++;
                }
            }
        }

    }

    /**
     * @param $fileName
     * @return array
     * @throws Exception
     */
    private function leggiMatriceFileCsv($fileName)
    {

        $handle = fopen($fileName, "r");
        $isHeader = true;
        $header = [];
        $data = [];
        while ($csvContent = fgetcsv($handle, 1000, ";")) {
            $nRColumns = count($csvContent);
            if ($isHeader) {
                $isHeader = false;
                for ($i = 0; $i < $nRColumns; $i++) {
                    $header[] = $csvContent[$i];
                }

                if (array_intersect(self::HEADER_CSV, $header) != self::HEADER_CSV) {
                    throw new Exception("Formato CSV non valido: la riga di intestazione deve iniziare con " . implode(";", self::HEADER_CSV));
                }
                continue;
            }
            $datum = [];
            for ($i = 0; $i < $nRColumns; $i++) {
                $datum[$header[$i]] = $csvContent[$i];
            }
            $data[] = $datum;
        };

        return [
            'header' => $header,
            'data' => $data
        ];


    }

    /**
     * @param $fileName
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    private function leggiMatriceFileXsl($fileName)
    {

        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileName);

        $checks = [

            'A1' => "Comune",

        ];

        $worksheet = $spreadsheet->getSheet(0);
        ImportHelpers::performFormatCheck($worksheet, $checks);


        $header = [];
        $data = [];
        $columns = [];
        foreach ($worksheet->getColumnIterator() as $column) {
            $columnIndex = $column->getColumnIndex();
            $columns[] = $columnIndex;
            $header[$columnIndex] = $worksheet->getCell("${columnIndex}1")->getCalculatedValue();
        }

        foreach ($worksheet->getRowIterator(2) as $row) {
            $rowIndex = $row->getRowIndex();
            $datum = [];
            foreach ($columns as $column) {

                $datum[$header[$column]] = $worksheet->getCell("${column}$rowIndex")->getCalculatedValue();
            }
            $data[] = $datum;


        }
        return [
            'header' => $header,
            'data' => $data
        ];


    }

    /**
     * @param $path
     * @param $fileName
     * @param $dstFileName
     * @throws Exception
     */
    private function importaFile($path, $fileName, $dstFileName)
    {
        $fileNameCompleto = $path . "/" . $fileName;
        if (file_exists($fileNameCompleto)) {
            Storage::disk('public')->put($dstFileName, file_get_contents($fileNameCompleto));
        } else {
            throw new Exception();
        }
    }

    private function report($msg)
    {
        $msg .= "\n";
        $this->reportMsg .= $msg;
        if ($this->print)
            echo $msg;
    }

    private function aggiornatoRecentemente(SplFileInfo $file): bool
    {
        $lastMod = $file->getMTime();//max($file->getMTime(), $file->getCTime());
        $lastValidDelta = strtotime("-5 minutes");
        return $lastMod >= $lastValidDelta;
    }

    public function commit()
    {
        DB::commit();
    }
}
