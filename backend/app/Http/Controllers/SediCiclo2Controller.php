<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Http\Controllers;

use App\Repositories\IndirizziDiStudioCiclo2Repository;
use App\Repositories\SediCiclo2Repository;
use Illuminate\Http\Request;

class SediCiclo2Controller extends Controller
{
    public function constructor()
    {

    }

    public function index(Request $request, SediCiclo2Repository $sediCiclo2)
    {
        $filtra = $request->input('filtra', null);
        $idAnno = $this->ottieniIdAnno($request);

        if ($filtra == null) {
            $data = $sediCiclo2->list(false, $idAnno);
        } else {
            switch ($filtra) {
                case 'sediamministrative':
                {
                    $data = $sediCiclo2->list(true, $idAnno);
                    break;
                }
                case 'indirizzoDiStudio':
                    {
                        $indirizzo = $request->input('indirizzo', null);
                        if ($indirizzo == null) {
                            abort(400, "nessun indirizzo fornito");
                        }
                        $data = $sediCiclo2->listByIndirizzo($indirizzo, $idAnno);
                    }
                    break;
                case 'indirizziDiStudio':
                    {
                        $indirizzi = $request->input('indirizzi', null);
                        if ($indirizzi == null) {
                            abort(400, "nessun indirizzo fornito");
                        }
                        $indirizzi = explode(',', $indirizzi);
                        $data = $sediCiclo2->listByIndirizzi($indirizzi, $idAnno);
                    }
                    break;
                default:
                    $data = [];
                    abort(400, "filtro non valido");
                    break;
            }
        }
        return response()->json(
            $data,
            200);

    }

    public function cercaByCodice(string $codice, SediCiclo2Repository $sediCiclo2, Request $request)
    {
        $idAnno = $this->ottieniIdAnno($request);
        $sede = $sediCiclo2->get($codice, $idAnno);
        if ($sede == null) {
            abort(404, "Sede non trovata");
        }
        return response()->json($sede);

    }


    public function get($idSede, SediCiclo2Repository $sediCiclo2Repository)
    {
        if (!is_numeric($idSede)) {
            abort(400, 'idSede non valido');
        }
        $sede = $sediCiclo2Repository->find($idSede);
        if ($sede == null) {
            abort(404, "Sede non trovata");
        }
        return response()->json($sede, 200);

    }

    public function getSediByIstituzione($codiceIstituzione, SediCiclo2Repository $sediCiclo2)
    {
        return response()->json(
            $sediCiclo2->getSediByIstituzione($codiceIstituzione),
            200);

    }

    public function indirizzi(IndirizziDiStudioCiclo2Repository $indirizzi, Request $request)
    {
        $aggrega = (bool)$request->get('aggrega', false);
        return response()->json($indirizzi->lista($aggrega));
    }

    public function provenienze(SediCiclo2Repository $sediCiclo2Repository, Request $request)
    {
        $idAnno = $this->ottieniIdAnno($request);
        $provenienze = $sediCiclo2Repository->provenienze($idAnno);
        return response()->json($provenienze);
    }


}


