<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Http\Controllers;

use App\Models\AnnoScolastico;

class AnnoScolasticoController extends Controller
{
    //

    public function list()
    {
        $list = AnnoScolastico::query()->ordered();
        return response()->json($list);
    }


    public function get($annoScolastico)
    {

        $anno = AnnoScolastico::query()->find($annoScolastico);

        if ($anno == null) {
            abort(404, 'Not found');
        }
        return response()->json($anno);

    }

    public function corrente()
    {
        $anno = AnnoScolastico::corrente();

        if ($anno == null) {
            abort(404, 'Not found');
        }
        return response()->json($anno);
    }
}
