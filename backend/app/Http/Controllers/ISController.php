<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Http\Controllers;

use App\Repositories\ISRepository;
use Illuminate\Http\Request;


class ISController extends Controller
{

    /** @var ISRepository */
    private $ISRepository;


    public function __construct(ISRepository $ISRepository)
    {
        $this->ISRepository = $ISRepository;
    }

    public function list(Request $request)
    {
        $idAnno = $this->ottieniIdAnno($request);
        $list = $this->ISRepository->list($idAnno);

        return response()->json($list);
    }


    public function cercaByCodice(Request $request, string $codiceIS)
    {
        $idAnno = $this->ottieniIdAnno($request);
        if ($codiceIS == null) {
            abort(400, 'Codice non valido');
        }
        $is = $this->ISRepository->cercaByCodice($idAnno, $codiceIS);
        if ($is == null) {
            abort(404, 'Not found');
        }
        return response()->json($is, 200);
    }


    public function ciclo1(Request $request)
    {
        $idAnno = $this->ottieniIdAnno($request);
        $ciclo1 = $this->ISRepository->ciclo1($idAnno);
        return response()->json($ciclo1);
    }

    public function ciclo2(Request $request)
    {

        $idAnno = $this->ottieniIdAnno($request);
        $ciclo2 = $this->ISRepository->ciclo2($idAnno);
        return response()->json($ciclo2);
    }

    public function odof(Request $request, ISRepository $ISRepository)
    {
        $idAnno = $this->ottieniIdAnno($request);
        return response()->json($ISRepository->odof($idAnno));

    }

    public function sovraDimensionate(Request $request)
    {
        $idAnno = $this->ottieniIdAnno($request);
        $sovraDimensionate = $this->ISRepository->sovraDimensionate($idAnno);
        return response()->json($sovraDimensionate);
    }

    public function sottoDimensionate(Request $request)
    {
        $idAnno = $this->ottieniIdAnno($request);
        $sovraDimensionate = $this->ISRepository->sottoDimensionate($idAnno);
        return response()->json($sovraDimensionate);
    }

    public function get($idSede, IsRepository $isRepository)
    {
        if (!is_numeric($idSede)) {
            abort(400, 'idSede non valido');
        }

        $sede = $isRepository->find($idSede);
        if ($sede == null) {
            abort(404, "Sede non trovata");
        }
        return response()->json($sede, 200);

    }

}
