<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Http\Controllers;

use App\Repositories\TrendRepository;
use Illuminate\Http\Request;

class TrendController extends Controller
{

    /**
     * @var TrendRepository $trendRepository
     */
    private $trendRepository;

    /**
     * TrendController constructor.
     * @param $trendRepository
     */
    public function __construct(TrendRepository $trendRepository)
    {
        $this->trendRepository = $trendRepository;
    }

    public function trend(TrendRepository $trendRepository)
    {
        return response()->json($trendRepository->trend());
    }

    public function popolazioneScolastica(Request $request, TrendRepository $trendRepository)
    {
        $idAnno = $this->ottieniIdAnno($request);
        return response()->json($trendRepository->popolazioneScolasticaAnniPrecedenti($idAnno));
    }

    public function trendIs(Request $request, TrendRepository $trendRepository)
    {
        $idAnno = $this->ottieniIdAnno($request);
        $codiceIs = $request->get("codiceIs");
        if ($codiceIs == null) {
            abort(400, "codiceIs non valido");
        }

        return response()->json($trendRepository->trendIs($idAnno, $codiceIs));
    }

    public function trendPe1(Request $request, TrendRepository $trendRepository)
    {
        $idAnno = $this->ottieniIdAnno($request);
        $codicePe = $request->get("codicePe");
        if ($codicePe == null) {
            abort(400, "codicePe non valido");
        }

        return response()->json($trendRepository->trendPe1($idAnno, $codicePe));
    }

    public function trendPe2(Request $request, TrendRepository $trendRepository)
    {
        $idAnno = $this->ottieniIdAnno($request);
        $codicePe = $request->get("codicePe");
        if ($codicePe == null) {
            abort(400, "codicePe non valido");
        }

        return response()->json($trendRepository->trendPe2($idAnno, $codicePe));
    }

    public function list(Request $request)
    {
        $demografie = $this->trendRepository->demografieComunali();
        return response()->json($demografie);
    }

    public function get($codiceCatastale)
    {

//        $codiceCatastale = (string)$request->get("codiceCatastale");
//        if ($codiceCatastale == null) {
//            abort(400, "codiceCatastale non valido");
//        }

        $demografie = $this->trendRepository->demografiaComune($codiceCatastale);
        if ($demografie == null) {
            abort(404, "$codiceCatastale not found");
        }
        return response()->json($demografie);
    }
}
