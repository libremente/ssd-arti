<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Http\Controllers;


use App\Repositories\CPIARepository;
use Illuminate\Http\Request;


class CPIAController extends Controller
{


    /** @var CPIARepository */
    private $CPIARepository;


    public function __construct(CPIARepository $CPIARepository)
    {
        $this->CPIARepository = $CPIARepository;
    }


    public function list()
    {

        return response()->json($this->CPIARepository->list());
    }

    public function livello1()
    {

        return response()->json($this->CPIARepository->getLivello1());
    }

    public function livello2()
    {
        return response()->json($this->CPIARepository->getLivello2());
    }

    public function dashboard(Request $request)
    {
        $idAnno = $this->ottieniIdAnno($request);
        $dashboard = [
            'cpia' => $this->CPIARepository->getCPIAResume($idAnno),
            'livello1' => $this->CPIARepository->getLivello1Resume($idAnno),
            'livello2' => $this->CPIARepository->getLivello2Resume($idAnno),
        ];

        return response()->json($dashboard);
    }

}
