<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Http\Controllers;


use App\Repositories\ComuniRepository;
use Illuminate\Http\Request;

class ComuniController extends Controller
{

    /** @var ComuniRepository */
    private $comuni;

    public function __construct(ComuniRepository $comuni)
    {
        $this->comuni = $comuni;
    }


    public function ambiti(Request $request)
    {
        //TODO: questa funzione non ha senso qui
        return json_encode($this->comuni->ambiti());
    }

    function get($codiceCatastaleComune)
    {
        $comune = $this->comuni->infoComune($codiceCatastaleComune, false);
        if ($comune == null) {
            abort(404, 'Not found');

        }
        return response(json_encode($comune));
    }


    function list()
    {

        $comuni = $this->comuni->lista();
        if ($comuni == null) {
            abort(404, 'Not found');

        }
        return response(json_encode($comuni));
    }


    function distanza(Request $request)
    {
        $codiceOri = $request->get("codiceOri", null);
        $codiceDst = $request->get("codiceDst", null);
        if ($codiceDst == null || $codiceOri == null) {
            abort(400, "codiceOri e codiceDst sono obbligatori");
        }
        $distanza = $this->comuni->distanza($codiceOri, $codiceDst);
        if (!$distanza) {
            abort("400", "origine o destinazione non valida");
        }

        return response()->json($distanza);
    }

    function vicini(Request $request)
    {
        $codiceOri = $request->get("codiceOri", null);
        $distanzaMaxMinuti = $request->get("distanzaMax", 60);
        if (!is_numeric($distanzaMaxMinuti)) {
            abort(400, "distanzaMax non valida");
        }
        if ($codiceOri == null) {
            abort(400, "codiceOri e sono obbligatori");
        }

        $vicini = $this->comuni->vicini($codiceOri, $distanzaMaxMinuti);
        return response(json_encode($vicini));
    }


}
