<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Http\Controllers;

use App\Repositories\SediCiclo1Repository;
use Illuminate\Http\Request;

class SediCiclo1Controller extends Controller
{
    public function constructor()
    {

    }

    public function index(Request $request, SediCiclo1Repository $sediCiclo1)
    {
        $filtra = $request->input('filtra', null);
        $idAnno = $this->ottieniIdAnno($request);

        if ($filtra == null) {
            $data = $sediCiclo1->list(false, $idAnno);
        } else {
            switch ($filtra) {
                case 'sediAmministrative':
                    {
                        $data = $sediCiclo1->list(true, $idAnno);
                        break;
                    }
                default:
                    $data = [];
                    abort(400, "filtro non valido");
                    break;
            }
        }
        return response()->json($data, 200);

    }

    public function cercaByCodice(string $codice, SediCiclo1Repository $sediCiclo1Repository, Request $request)
    {
        $idAnno = $this->ottieniIdAnno($request);
        $sede = $sediCiclo1Repository->show($codice, $idAnno);
        if ($sede == null) {
            abort(404, "Sede non trovata");
        }
        return response()->json($sede, 200);

    }


    public function get($idSede, SediCiclo1Repository $sediCiclo1Repository)
    {
        if (!is_numeric($idSede)) {
            abort(400, 'idSede non valido');
        }

        $sede = $sediCiclo1Repository->find($idSede);
        if ($sede == null) {
            abort(404, "Sede non trovata");
        }
        return response()->json($sede, 200);

    }

    public function getSediByIstituzione($codiceIstituzione, SediCiclo1Repository $sediCiclo1Repository, Request $request)
    {
        $idAnno = $this->ottieniIdAnno($request);
        return response()->json($sediCiclo1Repository->getSediByIstituzione($codiceIstituzione, $idAnno), 200);

    }
}


