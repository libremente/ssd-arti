<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Console\Commands;

use App\Helpers\ImportHelpers;
use App\Models\AmbitoTerritoriale;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception;

class ImportaAmbiti extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importa:ambiti';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importa gli ambiti da un file excel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function handle()
    {


        //
        DB::beginTransaction();
        DB::statement('delete from ssd."ambitiTerritoriali"');
        if (true) {
            $this->info("Importing ambiti");
            $ambiti = $this->leggiAmbitiDaXls();
            foreach ($ambiti as $ambito) {
                $this->inserisciAmbito($ambito);
            }
        }
        DB::commit();
        return 1;
    }


    /**
     *
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     */
    private function leggiAmbitiDaXls()
    {

        $HEADER_ROW_IDX = 1;
        $fileName = __DIR__ . "/../../../dataset/ambiti/ambiti_USR_DEF.xlsx";
        //0. load file
        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileName);


        $worksheet = $spreadsheet->getSheet(0);

        //1. perform "format" check
        $checks = [
            'A1' => "Prov",
            'B1' => "Comune",
            'C1' => "Nome ambito USR",
            'D1' => "Numero Ambito USR"

        ];
        ImportHelpers::performFormatCheck($worksheet, $checks);
        //2. parse data
        $ambiti = [];

        foreach ($worksheet->getRowIterator($HEADER_ROW_IDX + 1) as $row) {
            $rowIndex = $row->getRowIndex();
            $provincia = $worksheet->getCell("A$rowIndex")->getValue();
            if (is_null($provincia) || $provincia == "") {
                break;
            }

            $codice = $worksheet->getCell("C$rowIndex")->getValue();
            $numero = $worksheet->getCell("D$rowIndex")->getValue();

//            print "n: $numero, c: $codice, p: $provincia \n";
//            var_dump($codice);
            $ambiti[$codice] = [
                'codice' => $codice,
                'numero' => $numero,
                'provincia' => $provincia
            ];
        }
        return $ambiti;

    }


    private function inserisciAmbito($ambito)
    {
        $a = new AmbitoTerritoriale();
        $a->codice = $ambito['codice'];
        $a->numero = $ambito['numero'];
        $a->provincia = $ambito['provincia'];
        $a->save();
    }

}
