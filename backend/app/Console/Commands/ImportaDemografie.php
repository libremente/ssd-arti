<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Console\Commands;

use App\Helpers\ImportHelpers;
use App\Models\Demografia;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ImportaDemografie extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importa:demografie';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importa le demografie';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::transaction(function () {
            DB::statement('TRUNCATE ssd.demografie CASCADE');
            $this->importaDemografie();
            $this->output->success("Done");
        });
    }

    function importaDemografie()
    {
        $fileName = __DIR__ . "/../../../dataset/demografie/demo in eta scolastica per classi di eta.xlsx";


        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileName);


        $worksheet = $spreadsheet->getSheet(0);

        //1. perform "format" check
        $checks = [
            'C2' => "Comune",
            'D4' => "Popolazione Totale ISTAT 2019",
            'B1' => "Popolazione al 1° Gennaio",
            'E2' => "3 – 13",
            'J2' => "3 – 5",
            'T2' => "11 – 13",
            'Y2' => "14 – 18",
            'E3' => "Popolazione ISTAT",
            'H3' => "Previsioni",
            'J3' => "Popolazione ISTAT",
            'M3' => "Previsioni",
            'O3' => "Popolazione ISTAT",
            'R3' => "Previsioni",
            'T3' => "Popolazione ISTAT",
            'W3' => "Previsioni",
            'Y3' => "Popolazione ISTAT",
            'AB3' => "Previsioni",
            'E4' => "2015",
            'F4' => "2019",
            'H4' => "2023",

            'J4' => "2015",
            'K4' => "2019",
            'M4' => "2023",

            'O4' => "2015",
            'P4' => "2019",
            'R4' => "2023",

            'T4' => "2015",
            'U4' => "2019",
            'W4' => "2023",

            'Y4' => "2015",
            'Z4' => "2019",
            'AB4' => "2023",
        ];

        ImportHelpers::performFormatCheck($worksheet, $checks);
        //2. parse data


        foreach ($worksheet->getRowIterator(5) as $row) {
            $rowIndex = $row->getRowIndex();
            $comune = $worksheet->getCell("C$rowIndex")->getValue();
            $popolazione = $worksheet->getCell("D$rowIndex")->getValue();

            $comuneModel = ImportHelpers::comuneByNome($comune);
            if ($comuneModel == null) {
                continue;
            }
            $demografia = new Demografia();
            $demografia->codiceCatastaleComune = $comuneModel->codiceCatastale;
            $demografia->popTot2019 = $popolazione;
            //3-13
            $demografia->pop_3_13_2015 = $worksheet->getCell("E$rowIndex")->getValue();
            $demografia->pop_3_13_2019 = $worksheet->getCell("F$rowIndex")->getValue();
            $demografia->var_3_13_2019_2015 = floatval($worksheet->getCell("G$rowIndex")->getCalculatedValue());
            $demografia->prev_3_13_2023 = $worksheet->getCell("H$rowIndex")->getValue();
            $demografia->var_3_13_2023_2019 = floatval($worksheet->getCell("I$rowIndex")->getCalculatedValue());
            //3-5
            $demografia->pop_3_5_2015 = $worksheet->getCell("J$rowIndex")->getValue();
            $demografia->pop_3_5_2019 = $worksheet->getCell("K$rowIndex")->getValue();
            $demografia->var_3_5_2019_2015 = floatval($worksheet->getCell("L$rowIndex")->getCalculatedValue());
            $demografia->prev_3_5_2023 = $worksheet->getCell("M$rowIndex")->getValue();
            $demografia->var_3_5_2023_2019 = floatval($worksheet->getCell("N$rowIndex")->getCalculatedValue());
            //6-10
            $demografia->pop_6_10_2015 = $worksheet->getCell("O$rowIndex")->getValue();
            $demografia->pop_6_10_2019 = $worksheet->getCell("P$rowIndex")->getValue();
            $demografia->var_6_10_2019_2015 = floatval($worksheet->getCell("Q$rowIndex")->getCalculatedValue());
            $demografia->prev_6_10_2023 = $worksheet->getCell("R$rowIndex")->getValue();
            $demografia->var_6_10_2023_2019 = floatval($worksheet->getCell("S$rowIndex")->getCalculatedValue());
            //11-13
            $demografia->pop_11_13_2015 = $worksheet->getCell("T$rowIndex")->getValue();
            $demografia->pop_11_13_2019 = $worksheet->getCell("U$rowIndex")->getValue();
            $demografia->var_11_13_2019_2015 = floatval($worksheet->getCell("V$rowIndex")->getCalculatedValue());
            $demografia->prev_11_13_2023 = $worksheet->getCell("W$rowIndex")->getValue();
            $demografia->var_11_13_2023_2019 = floatval($worksheet->getCell("X$rowIndex")->getCalculatedValue());
            //14-18
            $demografia->pop_14_18_2015 = $worksheet->getCell("Y$rowIndex")->getValue();
            $demografia->pop_14_18_2019 = $worksheet->getCell("Z$rowIndex")->getValue();
            $demografia->var_14_18_2019_2015 = floatval($worksheet->getCell("AA$rowIndex")->getCalculatedValue());
            $demografia->prev_14_18_2023 = $worksheet->getCell("AB$rowIndex")->getValue();
            $demografia->var_14_18_2023_2019 = floatval($worksheet->getCell("AC$rowIndex")->getCalculatedValue());
            $demografia->save();


        }
    }
}
