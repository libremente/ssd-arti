<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Console\Commands;

use App\Helpers\ImportHelpers;
use App\Helpers\ImportTraits;
use App\Helpers\ODOF1920_AGGImporter;
use App\Models\AnnoScolastico;
use App\Models\IndirizzoStudioCiclo2;
use App\Models\SedeCiclo2;
use Illuminate\Console\Command;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\App;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ImportaReteScolastica20192020 extends Command
{

    const ALUNNI_FROM_RETE = true;
    const CODICE_INDIRIZZO_NON_DISPONIBILE = "skip codice indirizzo";

    use ImportTraits;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importa:reteScolastica20192020';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    private $fileName = __DIR__ . "/../../../dataset/20192020/19_20_OD+OF.xlsx";
    /**
     * @var Worksheet
     */
    private $worksheet;

    private const HEADER_ROW_IDX = 1;

    private const HEADER = [
        'A' => "Lat",
        'B' => "Lon",
        'C' => "DA1819",
        'D' => "Ciclo",
        'E' => "CodED1819",
        'F' => "CodPE1920",
        'G' => "CodIS1920",
        'H' => "Tipo Scuola",

        'I' => "ComunePE",
        'K' => "IndirPE",
        'L' => "IndirIS",
        'M' => "ComuneIS",

        'N' => "DenPE1920",
        'O' => "DenIS1920",

        'P' => "TotIscr",
        'Q' => "TotSez",
        'R' => "TotIscrTN",
        'S' => "TotSezTN",
        'T' => "TotIscrTP",
        'U' => "TotISezTP",
        'V' => "TotIscrTR",
        'W' => "TotISezTR",
        'Y' => "Organico di Fatto",


    ];

    private $errori = [];

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \Exception
     */
    public function handle()
    {

        if (App::environment() == 'ssd-priv') {
            $odof = new ODOF1920_AGGImporter();
            $odof->handle();
            return;
        }
        $annoScolasticoStr = "2019/2020";
        $this->eliminaAnnoScolastico($annoScolasticoStr);
        $annoScolastico = AnnoScolastico::nuovoAnnoScolastico($annoScolasticoStr);
        $this->loadFile();

        $this->info("load file");
        $sedi = $this->loadSedi();
        $istituzioni = [];
        foreach ($sedi as $sediPerVerticalizzazione) {
            foreach ($sediPerVerticalizzazione as $sede) {
                $codiceIS = $sede['codiceIS'];
                //sfrutto l'indice per garantire l'unicità
                if (isset($istituzioni[$codiceIS])) {
                    $istituzione = $istituzioni[$codiceIS];

                    $organicoDiFattoSede = array_key_exists('organicoFatto', $sede) ? $sede['organicoFatto'] : null;

                    if ($istituzione['nAlunni'] != null) {
                        $istituzione['nAlunni'] += $sede['nAlunni'];
                    } else {
                        $istituzione['nAlunni'] = $sede['nAlunni'];
                    }
                    if ($istituzione['nClassi'] != null) {
                        $istituzione['nClassi'] += $sede['nClassi'];
                    } else {
                        $istituzione['nClassi'] = $sede['nClassi'];
                    }
                    if ($istituzione['nDisabili'] != null) {
                        $istituzione['nDisabili'] += $sede['nDisabili'];
                    } else {
                        $istituzione['nDisabili'] = $sede['nDisabili'];
                    }
                    if ($organicoDiFattoSede != null) {
                        if ($istituzione['organicoFatto'] != null) {
                            $istituzione['organicoFatto'] += $organicoDiFattoSede;
                        } else {
                            $istituzione['organicoFatto'] = $organicoDiFattoSede;
                        }
                    }
                    $istituzioni[$codiceIS] = $istituzione;
                } else {
                    $istituzioni[$codiceIS] = $sede;
                }
            }
        }

        $this->info("IS");
        foreach ($istituzioni as $istituzione) {
            $this->inserisciIS($istituzione, $annoScolastico);
            if (self::ALUNNI_FROM_RETE) {
                $this->inserisciAlunniIs($istituzione, $annoScolastico);
            }
        }
        $this->info("Sedi");
        $this->inserisciSedi($sedi, $annoScolastico);
        $this->info("Dettagli alunni");
        if (self::ALUNNI_FROM_RETE) {
            $this->inserisciAlunniSedi($sedi, $annoScolastico, true);
            //per quanto riguarda i dati dei singoli corsi di studio, è necessario far riferimento ad un diverso file excel
            $this->output->progressStart();
            $this->ottieniEdInserisciGrado2($annoScolastico);
        } else {
            $this->importaDettagliAlunni($annoScolastico);
        }
        foreach ($this->errori as $errore) {
            $this->error($errore);
        }
        $this->output->success("done");


    }

    /**
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function loadFile()
    {


        $reader = IOFactory::createReaderForFile($this->fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($this->fileName);


        $worksheet = $spreadsheet->getSheet(0);


        //perform "format" check
        ImportHelpers::performFormatCheck($worksheet, self::HEADER, self::HEADER_ROW_IDX);

        $this->worksheet = $worksheet;


    }


    /**
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function loadSedi()
    {
        $sedi = [];
        $worksheet = $this->worksheet;

        foreach ($worksheet->getRowIterator(self::HEADER_ROW_IDX + 1) as $row) {
            $rowIndex = $row->getRowIndex();

            $codiceIS = $worksheet->getCell("G$rowIndex")->getValue();
            if (ImportHelpers::checkIfEmptyCell($codiceIS)) {
                continue;
            }
            $codiceSede = $worksheet->getCell("F$rowIndex")->getValue();
            $lat = $worksheet->getCell("A$rowIndex")->getValue();
            $lon = $worksheet->getCell("B$rowIndex")->getValue();
            if ($lat == "ND" || $lon == "ND") {
                $this->errori[] = "Coordinate non valide ($lat,$lon) per $codiceSede";
                continue;
            }
            $denominazione = $worksheet->getCell("N$rowIndex")->getValue();

            $isDirezioneAmministrativa = (boolean)$worksheet->getCell("C$rowIndex")->getValue() != "";


            $verticalizzazione = $worksheet->getCell("D$rowIndex")->getValue();
            $tipologiaScuola = trim($worksheet->getCell("H$rowIndex")->getValue());
            $codiceEdificio = $worksheet->getCell("E$rowIndex")->getValue();
            $comuneSede = $worksheet->getCell("I$rowIndex")->getValue() ?: "N/D";
            $indirizzoPlesso = $worksheet->getCell("K$rowIndex")->getValue();
            $comuneSedeDirigenza = $worksheet->getCell("M$rowIndex")->getValue();
//            $indirizzoIs =  $worksheet->getCell("L$rowIndex")->getValue();
            $denominazioneIs = $worksheet->getCell("O$rowIndex")->getValue();
            $organicoFatto = $worksheet->getCell("Y$rowIndex")->getValue();

            $lat = ImportHelpers::isCampoNonDisponibile($lat) ? null : $lat;
            $lon = ImportHelpers::isCampoNonDisponibile($lon) ? null : $lon;
            $verticalizzazioneIdx = $verticalizzazione;
            if ($verticalizzazioneIdx == "0") $verticalizzazioneIdx = "nd";

            $nAlunni = $worksheet->getCell("P$rowIndex")->getValue();
            $nClassi = $worksheet->getCell("Q$rowIndex")->getValue();
            $nDisabili = null;
            $codiceIndirizzo = self::CODICE_INDIRIZZO_NON_DISPONIBILE;

            $dettagli = [
                'TotIscrTN' => $worksheet->getCell("R$rowIndex")->getValue(),
                'TotSezTN' => $worksheet->getCell("S$rowIndex")->getValue(),
                'TotIscrTP' => $worksheet->getCell("T$rowIndex")->getValue(),
                'TotISezTP' => $worksheet->getCell("U$rowIndex")->getValue(),
                'TotIscrTR' => $worksheet->getCell("V$rowIndex")->getValue(),
                'TotISezTR' => $worksheet->getCell("W$rowIndex")->getValue(),
                'Plur' => $worksheet->getCell("X$rowIndex")->getValue()

            ];

            $sedi[$verticalizzazioneIdx][] = compact(
                'codiceSede',
                'denominazione',
                'codiceIS',
                'isDirezioneAmministrativa',
                'verticalizzazione',
                'tipologiaScuola',
                'codiceEdificio',
                'lat',
                'lon',
                'comuneSede',
                'indirizzoPlesso',
                'comuneSedeDirigenza',
                'denominazioneIs',
                'codiceIndirizzo',
                'nAlunni',
                'nClassi',
                'nDisabili',
                'dettagli',
                'organicoFatto'
            );
        }

        return $sedi;

    }

    private function importaDettagliAlunni(AnnoScolastico $annoScolastico)
    {


        $this->output->progressStart(10);

        $this->info("Alunni IS");
        $dataIs = $this->ottieniTotaliPerIs2019();
        $this->output->progressAdvance();

        foreach ($dataIs as $is) {
            try {
                $this->inserisciAlunniIs($is, $annoScolastico);
            } catch (\Exception $exception) {
                $this->error($exception->getMessage());
            }
        }
        $this->output->progressAdvance();

        $this->info("Alunni Infanzia");
        $datiInfanzia = $this->ottieniDatiInfanzia();
        foreach ($datiInfanzia as $item) {
            try {
                $this->inserisciAlunniCiclo1($item, $annoScolastico);
            } catch (\Exception $exception) {
                $this->error($exception->getMessage());
            }
        }
        $this->output->progressAdvance();

        $this->info("Alunni Primaria");
        $datiPrimaria = $this->ottieniDatiPrimaria();
        foreach ($datiPrimaria as $item) {
            try {
                $this->inserisciAlunniCiclo1($item, $annoScolastico);
            } catch (\Exception $exception) {
                $this->error($exception->getMessage());
            }
        }
        $this->output->progressAdvance();

        //grado 1
        $this->info("Alunni Grado 1");
        $datiGrado1 = $this->ottieniDatiGrado1();
        foreach ($datiGrado1 as $item) {
            try {
                $this->inserisciAlunniCiclo1($item, $annoScolastico);
            } catch (\Exception $exception) {
                $this->error($exception->getMessage());
            }
        }
        $this->output->progressAdvance();


        $this->ottieniEdInserisciGrado2($annoScolastico);
        $this->output->progressAdvance();

        $this->output->progressFinish();
    }

    private function ottieniEdInserisciGrado2($annoScolastico)
    {
        //grado 2
        $error = 0;
        $this->info("Alunni Grado 2");
        $datiGrado2 = $this->ottieniDatiGrado2();
        foreach ($datiGrado2 as $item) {
            try {
                $this->inserisciAlunniCiclo2($item, $annoScolastico);
            } catch (\Exception $exception) {
                $this->error($exception->getMessage());
                $error++;
            }
        }
        $this->output->progressAdvance();

        $this->info("Indirizzi Studio Grado 2");
        foreach ($datiGrado2 as $item) {

            $codice = $item['codiceIndirizzo'];
            if ($codice == self::CODICE_INDIRIZZO_NON_DISPONIBILE) {
                continue;
            }
            $indirizzoDiStudioModel = IndirizzoStudioCiclo2::query()->whereKey($codice)->first();
            if ($indirizzoDiStudioModel == null) {
                $indirizziDiStudioNonTrovati[] = $codice;
            } else {
                $sedeData = SedeCiclo2::trovaCodiceAnno($item['codiceSede'], $annoScolastico->id);
                if ($sedeData == null) {
                    $this->error("Sede ${item['codiceSede']} non trovata ");
                    $error++;
                    continue;
                }
                try {
                    $sedeData->indirizziDiStudio()->attach($indirizzoDiStudioModel);
                } catch (QueryException $exception) {
                    $this->error($exception->getMessage());
                    $error++;
                }

            }
            $this->output->progressAdvance();
        }

        if ($error != 0) {
            $this->error("Si sono verificati $error errori");
        }
    }

    public function ottieniTotaliPerIs2019()
    {

        $fileName = __DIR__ . "/../../../dataset/scuole_19_20_OD_1.xlsx";


        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileName);


        $sedi = [];
        $worksheet = $spreadsheet->getSheet(0);

        $HEADER_ROW_IDX = 1;

        $checks = [
            'S' => "CodIS1920",
            'Y' => "TotIscr",
            'Z' => "TotSez"
        ];


        //1. perform "format" check
        ImportHelpers::performFormatCheck($worksheet, $checks, $HEADER_ROW_IDX);

        //2. parse data
        foreach ($worksheet->getRowIterator($HEADER_ROW_IDX + 1) as $row) {
            $rowIndex = $row->getRowIndex();
            $codiceIS = $worksheet->getCell("S$rowIndex")->getValue();
            if (ImportHelpers::checkIfEmptyCell($codiceIS)) {
                break;
            }

            $nAlunni = $worksheet->getCell("Y$rowIndex")->getValue();
            $nClassi = $worksheet->getCell("Z$rowIndex")->getValue();
            $nDisabili = null;

            $sedi[$codiceIS][] = compact('nAlunni', 'nClassi', 'nDisabili');
        }
        $dataIs = [];
        foreach ($sedi as $codiceIs => $items) {
            $accumulatore = ['codiceIS' => $codiceIs, 'nClassi' => 0, 'nAlunni' => 0, 'nDisabili' => null];
            foreach ($items as $item) {
                $accumulatore['nClassi'] += $item['nClassi'];
                $accumulatore['nAlunni'] += $item['nAlunni'];

            }
            $dataIs[] = $accumulatore;
        }

        return $dataIs;
    }


    /**
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function ottieniDatiInfanzia()
    {
        $fileName = __DIR__ . "/../../../dataset/andamenti/20192020/Infanzia OD 2019-20.xlsx";


        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileName);

        $dataInfanzia = [];
        $worksheet = $spreadsheet->getSheet(0);
        {
            $HEADER_ROW_IDX = 2;

            $checks = [
                'K1' => "Totale",
                'M1' => "Orario normale",
                'O1' => "Orario ridotto",

                'B2' => "Codice",
                'F2' => "Cod. ist. Rif.",
                'K2' => "Bambini",
                'L2' => 'Sezioni',
                'M2' => "Bambini",
                "N2" => "Sezioni",
                'O2' => "Bambini",
                "P2" => "Sezioni",
            ];

            //1. perform "format" check
            ImportHelpers::performFormatCheck($worksheet, $checks);

            //2. parse data
            foreach ($worksheet->getRowIterator($HEADER_ROW_IDX + 1) as $row) {
                $rowIndex = $row->getRowIndex();
                $codiceSede = $worksheet->getCell("B$rowIndex")->getValue();
                if (ImportHelpers::checkIfEmptyCell($codiceSede)) {
                    continue;
                }

                $nAlunni = $worksheet->getCell("K$rowIndex")->getValue();
                $nDisabili = null;//$worksheet->getCell("K$rowIndex")->getValue();
                $nClassi = $worksheet->getCell("L$rowIndex")->getValue();

                $dettagli = [];
                $dettagli['orarioNormale']['nAlunni'] = $worksheet->getCell("M$rowIndex")->getValue();
                $dettagli['orarioNormale']['sezioni'] = $worksheet->getCell("N$rowIndex")->getValue();
                $dettagli['orarioRidotto']['nAlunni'] = $worksheet->getCell("O$rowIndex")->getValue();
                $dettagli['orarioRidotto']['sezioni'] = $worksheet->getCell("P$rowIndex")->getValue();


                $dataInfanzia[] = compact('codiceSede', 'nAlunni', 'nClassi', 'nDisabili', 'dettagli');
            }
        }
        return $dataInfanzia;

    }


    /**
     * @param Spreadsheet $spreadsheet
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function ottieniDatiPrimaria()
    {
        $fileName = __DIR__ . "/../../../dataset/andamenti/20192020/Primaria OD 2019-20.xlsx";


        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileName);

        $dataInfanzia = [];
        $worksheet = $spreadsheet->getSheet(0);
        {
            $HEADER_ROW_IDX = 3;

            $checks = [
                'B3' => "Codice",
                'K2' => "Totale",
                'K3' => "Alunni",

                'L3' => "Classi",
                'M2' => "I anno di corso",
                'M3' => "Alunni",
                'N3' => "classi",

                'O2' => "II anno di corso",

                'O3' => "Alunni",
                'P3' => "classi",

                'Q3' => "Alunni",
                'S3' => "Alunni",
                'U3' => "Alunni",
                'W3' => "Plur."
            ];

            //1. perform "format" check
            ImportHelpers::performFormatCheck($worksheet, $checks);

            //2. parse data
            foreach ($worksheet->getRowIterator($HEADER_ROW_IDX + 1) as $row) {
                $rowIndex = $row->getRowIndex();
                $codiceSede = $worksheet->getCell("B$rowIndex")->getValue();
                if (ImportHelpers::checkIfEmptyCell($codiceSede)) {
                    continue;
                }

                $nAlunni = $worksheet->getCell("K$rowIndex")->getValue();
                $nDisabili = null;
                $nClassi = $worksheet->getCell("L$rowIndex")->getValue();

                $dettagli = [];
                //1 anno
                $dettagli['1anno']['nAlunni'] = $worksheet->getCell("M$rowIndex")->getValue();
                $dettagli['1anno']['nDisabili'] = null;
                $dettagli['1anno']['classi'] = $worksheet->getCell("N$rowIndex")->getValue();
                //2 anno
                $dettagli['2anno']['nAlunni'] = $worksheet->getCell("O$rowIndex")->getValue();
                $dettagli['2anno']['nDisabili'] = null;
                $dettagli['2anno']['classi'] = $worksheet->getCell("P$rowIndex")->getValue();
                //3 anno
                $dettagli['3anno']['nAlunni'] = $worksheet->getCell("Q$rowIndex")->getValue();
                $dettagli['3anno']['nDisabili'] = null;
                $dettagli['3anno']['classi'] = $worksheet->getCell("R$rowIndex")->getValue();
                //4 anno
                $dettagli['4anno']['nAlunni'] = $worksheet->getCell("S$rowIndex")->getValue();
                $dettagli['4anno']['nDisabili'] = null;
                $dettagli['4anno']['classi'] = $worksheet->getCell("T$rowIndex")->getValue();
//                $dettagli['4anno']['rapporto'] = $worksheet->getCell("Y$rowIndex")->getValue();
                //5 anno
                $dettagli['5anno']['nAlunni'] = $worksheet->getCell("U$rowIndex")->getValue();
                $dettagli['5anno']['nDisabili'] = null;
                $dettagli['5anno']['classi'] = $worksheet->getCell("V$rowIndex")->getValue();
                $dettagli['5anno']['plur'] = $worksheet->getCell("W$rowIndex")->getValue();


                $dataInfanzia[] = compact('codiceSede', 'nAlunni', 'nClassi', 'nDisabili', 'dettagli');
            }
        }
        return $dataInfanzia;

    }

    private function ottieniDatiGrado1()
    {
        $fileName = __DIR__ . "/../../../dataset/andamenti/20192020/Primo Grado OD 2019-20.xlsx";


        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileName);
        $dataGrado1 = [];
        $worksheet = $spreadsheet->getSheet(0);
        {
            $HEADER_ROW_IDX = 3;

            $checks = [
                'B2' => "Codice",
                'K2' => "Totale",

                'K3' => "Alunni",
                'L3' => "Classi",


                'M3' => "Alunni",
                'N3' => "Classi",


                'O3' => "Alunni",
                'P3' => "Classi",

                'Q3' => "Alunni",
                'R3' => "Classi",


            ];

            //1. perform "format" check
            ImportHelpers::performFormatCheck($worksheet, $checks);

            //2. parse data
            foreach ($worksheet->getRowIterator($HEADER_ROW_IDX + 1) as $row) {
                $rowIndex = $row->getRowIndex();
                $codiceSede = $worksheet->getCell("B$rowIndex")->getValue();
                if (ImportHelpers::checkIfEmptyCell($codiceSede)) {
                    continue;
                }

                $nAlunni = $worksheet->getCell("K$rowIndex")->getValue();
                $nDisabili = null;//$worksheet->getCell("L$rowIndex")->getValue();
                $nClassi = $worksheet->getCell("L$rowIndex")->getValue();

                $dettagli = [];
                //1 anno
                $dettagli['1anno']['nAlunni'] = $worksheet->getCell("M$rowIndex")->getValue();
                $dettagli['1anno']['nDisabili'] = null;
                $dettagli['1anno']['classi'] = $worksheet->getCell("N$rowIndex")->getValue();
                //2 anno
                $dettagli['2anno']['nAlunni'] = $worksheet->getCell("O$rowIndex")->getValue();
                $dettagli['2anno']['nDisabili'] = null;
                $dettagli['2anno']['classi'] = $worksheet->getCell("P$rowIndex")->getValue();
                //3 anno
                $dettagli['3anno']['nAlunni'] = $worksheet->getCell("Q$rowIndex")->getValue();
                $dettagli['3anno']['nDisabili'] = null;
                $dettagli['3anno']['classi'] = $worksheet->getCell("R$rowIndex")->getValue();


                $dataGrado1[] = compact('codiceSede', 'nAlunni', 'nClassi', 'nDisabili', 'dettagli');
            }
        }
        return $dataGrado1;

    }


    /**
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function ottieniDatiGrado2()
    {
        $fileName = __DIR__ . "/../../../dataset/andamenti/20192020/Secondo Grado OD 2019-20_indirizzi.xlsx";


        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);

        $spreadsheet = $reader->load($fileName);
        $dataGrado2 = [];
        $worksheetGrado2 = $spreadsheet->getSheet(0);

        $HEADER_ROW_IDX = 2;

        $checksGrado2 = [
            'B2' => "Codice",
            'K2' => 'Cod. ind.',
            'L2' => "Denominazione indirizzo",
            'M1' => "Totale",
            'M2' => "Alunni",
            'N2' => "Classi",
            'O2' => "Alunni",
            'P2' => "Classi",
            'Q2' => "Alunni",
            'R2' => "Classi",
            'S2' => "Alunni",
            'T2' => "Classi",
            'U2' => "Alunni",
            'V2' => "Classi",
            'W2' => "Alunni",
            'X2' => "Classi",
            'Y2' => "Alunni",
            'Z2' => "Classi",
        ];

        ImportHelpers::performFormatCheck($worksheetGrado2, $checksGrado2);

        //2. parse data
        foreach ($worksheetGrado2->getRowIterator($HEADER_ROW_IDX + 1) as $row) {
            $rowIndex = $row->getRowIndex();
            $codiceSede = $worksheetGrado2->getCell("B$rowIndex")->getValue();
            if (ImportHelpers::checkIfEmptyCell($codiceSede)) {
                continue;
            }
            $codiceIndirizzo = $worksheetGrado2->getCell("K$rowIndex")->getValue();

            $nAlunni = $worksheetGrado2->getCell("M$rowIndex")->getValue();
            $nClassi = $worksheetGrado2->getCell("N$rowIndex")->getValue();

            $dettagli = [];
            //1 anno
            $dettagli['1anno']['nAlunni'] = $worksheetGrado2->getCell("O$rowIndex")->getValue();
            $dettagli['1anno']['nClassi'] = $worksheetGrado2->getCell("P$rowIndex")->getValue();
            //2 anno
            $dettagli['2anno']['nAlunni'] = $worksheetGrado2->getCell("Q$rowIndex")->getValue();
            $dettagli['2anno']['nClassi'] = $worksheetGrado2->getCell("R$rowIndex")->getValue();
            //3 anno
            $dettagli['3anno']['nAlunni'] = $worksheetGrado2->getCell("S$rowIndex")->getValue();
            $dettagli['3anno']['nClassi'] = $worksheetGrado2->getCell("T$rowIndex")->getValue();
            //4 anno
            $dettagli['4anno']['nAlunni'] = $worksheetGrado2->getCell("U$rowIndex")->getValue();
            $dettagli['4anno']['nClassi'] = $worksheetGrado2->getCell("V$rowIndex")->getValue();
            //5 anno
            $dettagli['5anno']['nAlunni'] = $worksheetGrado2->getCell("W$rowIndex")->getValue();
            $dettagli['5anno']['nClassi'] = $worksheetGrado2->getCell("X$rowIndex")->getValue();
            //6 anno
            $dettagli['6anno']['nAlunni'] = $worksheetGrado2->getCell("Y$rowIndex")->getValue();
            $dettagli['6anno']['nClassi'] = $worksheetGrado2->getCell("Z$rowIndex")->getValue();
            //$dettagli['totale']['nAlunni'] = $worksheetGrado2->getCell("X$rowIndex")->getCalculatedValue();


            $nDisabili = null;


            $dataGrado2[] = compact('codiceSede', 'codiceIndirizzo', 'nAlunni', 'nClassi', 'nDisabili', 'dettagli');
        }

        return $dataGrado2;

    }


}
