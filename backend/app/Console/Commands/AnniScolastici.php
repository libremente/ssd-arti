<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Console\Commands;

use App\Models\AnnoScolastico;
use Illuminate\Console\Command;

class AnniScolastici extends Command
{


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'anniScolastici:nuovo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->hasArgument('annoScolastico')) {
            $annoScolastico = $this->argument('annoScolastico');
        } else {
            $annoScolastico = $this->ask("Inserire anno scolastico (yyyy/yyyy)");
        }
        $model = AnnoScolastico::nuovoAnnoScolastico($annoScolastico);
        if ($model == null) {
            $this->error("Formato non valido");
        } else {
            $this->output->success("Anno scolastico $annoScolastico inserito con successo (id = $model->id)");
        }

    }
}
