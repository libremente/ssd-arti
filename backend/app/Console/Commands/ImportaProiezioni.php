<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Console\Commands;

use App\Helpers\ImportHelpers;
use App\Models\AlunniIsProiezione;
use App\Models\AlunniSedeCiclo1Proiezione;
use App\Models\AlunniSedeCiclo2Proiezione;
use App\Models\AnnoScolasticoProiezione;
use App\Models\TipologiaScuola;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ImportaProiezioni extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importa:proiezioni';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        AlunniIsProiezione::query()->delete();
        AlunniSedeCiclo1Proiezione::query()->delete();
        AlunniSedeCiclo2Proiezione::query()->delete();


        $annoScolastico2021 = AnnoScolasticoProiezione::nuovoAnnoScolastico("2020/2021");
        $annoScolastico2122 = AnnoScolasticoProiezione::nuovoAnnoScolastico("2021/2022");
        $annoScolastico2223 = AnnoScolasticoProiezione::nuovoAnnoScolastico("2022/2023");

        $fileName = __DIR__ . "/../../../dataset/20192020/proiezioni_alunni_triennale.xlsx";

        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileName);


        $data = $this->load($spreadsheet);

        $this->info("Alunni IS");

        $errors = 0;
        foreach ($data as $datum) {

            $annoScolastico = null;
            switch ($datum['anno']) {
                case "2020/2021":
                    $annoScolastico = $annoScolastico2021;
                    break;
                case "2021/2022":
                    $annoScolastico = $annoScolastico2122;
                    break;
                case "2022/2023":
                    $annoScolastico = $annoScolastico2223;
                    break;
            }
            try {

                $ciclo = TipologiaScuola::getCiclo($datum['tipo']);
                if ($ciclo == 1) {
                    $alunniSede = AlunniSedeCiclo1Proiezione::nuovo($datum['codicePE'], $datum['codiceIS'], $annoScolastico);
                    $alunniSede->alunni = $datum['nAlunni'];
                    $alunniSede->save();
                } else if ($ciclo == 2) {
                    $alunniSede = AlunniSedeCiclo2Proiezione::nuovo($datum['codicePE'], $datum['codiceIS'], $annoScolastico);
                    $alunniSede->alunni = $datum['nAlunni'];
                    $alunniSede->save();
                }

//                $alunniIs = AlunniIsProiezione::nuovo($datum['codice'], $annoScolastico);

            } catch (\Exception $exception) {
//                $this->error("Anno scolastico = " . $datum['anno']);
                $this->error($exception->getMessage());
                $errors++;
            }


        }

        //usando il db, calcolo il totale per is
        $queryAlunniIsFromProiezioni1 = <<<SQL
insert into "alunniIsProiezioni"("codiceIs", "idAnnoProiezione", "alunni")
select "codiceIs", "idAnnoProiezione", sum(alunni) as "alunni"
from "alunniSedeCiclo1Proiezioni" group by "codiceIs", "idAnnoProiezione";
SQL;
        $queryAlunniIsFromProiezioni2 = <<<SQL
insert into "alunniIsProiezioni"("codiceIs", "idAnnoProiezione", "alunni")
select "codiceIs", "idAnnoProiezione", sum(alunni) as "alunni"
from "alunniSedeCiclo2Proiezioni" group by "codiceIs", "idAnnoProiezione";
SQL;

        $ok = DB::statement($queryAlunniIsFromProiezioni1);
        if (!$ok) {
            $errors++;
        }
        $ok = DB::statement($queryAlunniIsFromProiezioni2);
        if (!$ok) {
            $errors++;
        }


        if ($errors != 0) {
            $this->error("$errors Errori");
            $errors = 0;
        } else {
            $this->output->success("OK");
        }


    }


    /**
     * @param Spreadsheet $spreadsheet
     * @return array
     * @throws Exception
     */
    public function load(Spreadsheet $spreadsheet)
    {


        $data = [];
        $worksheet = $spreadsheet->getSheet(0);
        {
            $HEADER_ROW_IDX = 1;

            $checks = [
                'B1' => "CodicePE",
                'F1' => "CodIS",
                'K1' => "Bambini",
                'L1' => "Sezioni",
                'M1' => "Tipo",
                'Q1' => "PROIEZIONI OD 20/21",
                'R1' => "PROIEZIONI OD21/22",
                'S1' => 'PROIEZIONI OD22/23'
            ];


            ImportHelpers::performFormatCheck($worksheet, $checks);

            foreach ($worksheet->getRowIterator($HEADER_ROW_IDX + 1) as $row) {
                $rowIndex = $row->getRowIndex();
                $codicePE = $worksheet->getCell("B$rowIndex")->getValue();
                if (ImportHelpers::checkIfEmptyCell($codicePE)) {
                    continue;
                }
                $codiceIS = $worksheet->getCell("F$rowIndex")->getValue();

                $anno = "2020/2021";
                $tipo = $worksheet->getCell("M$rowIndex")->getValue();
                $nAlunni = round($worksheet->getCell("Q$rowIndex")->getValue());
                $data[] = compact('codicePE', 'codiceIS', 'tipo', 'anno', 'nAlunni');

                $anno = "2021/2022";
                $nAlunni = round($worksheet->getCell("R$rowIndex")->getValue());
                $data[] = compact('codicePE', 'codiceIS', 'tipo', 'anno', 'nAlunni');

                $anno = "2022/2023";
                $nAlunni = round($worksheet->getCell("S$rowIndex")->getValue());
                $data[] = compact('codicePE', 'codiceIS', 'tipo', 'anno', 'nAlunni');
            }
        }
        return $data;
    }


}
