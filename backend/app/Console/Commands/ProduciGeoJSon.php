<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Console\Commands;


use App\Models\AmbitoTerritoriale;
use App\Models\Distretto;
use App\Models\UnioneComuni;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ProduciGeoJSon extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'produci:geoJSON';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->assegnaConfineAmbiti();
        $this->produciGeoJSONAmbiti();

        $this->assegnaConfineDistretti();
        $this->produciGeoJSONDistretti();

        $this->assegnaConfineUnioneComuni();
        $this->produciGeoJSONUnioneComuni();

        $this->info("Done!");
    }


    private function assegnaConfineAmbiti()
    {

        $query = 'select ST_Multi(ST_Union(ST_MakeValid(confine))) as confine, "ambitoTerritoriale" from comuni where confine is not null group by "ambitoTerritoriale"';
        $results = DB::select(DB::raw($query));
//        var_dump($results);
        foreach ($results as $result) {
            /** @var AmbitoTerritoriale $ambito */
            $ambito = AmbitoTerritoriale::query()->find($result->ambitoTerritoriale);
            if ($ambito != null) {
                $ambito->confine = $result->confine;
                $ambito->save();
            }
        }
    }

    private function assegnaConfineUnioneComuni()
    {
        $unioni = UnioneComuni::all();


        foreach ($unioni as $unione) {


            $in = '\'' . implode('\',\'', $unione->codiciComuni) . '\'';

            $query = 'select ST_Multi(ST_Union(ST_MakeValid(confine))) as confine from comuni where confine is not null and "codiceCatastale" in (' . $in . ')';
            $results = DB::select(DB::raw($query));
            $unione->confine = $results[0]->confine;
            $unione->save();
        }
    }


    private function produciGeoJSONAmbiti()
    {
        $query = <<<QUERY
    SELECT 'FeatureCollection' As type, array_to_json(array_agg(f))
As features FROM
(SELECT
'Feature' As type,
concat('ambito_',codice) as id,
ST_AsGeoJSON((confine),15,4)::json As geometry,

json_build_object('codice', codice, 'numero', numero, 'provincia', provincia) As properties
FROM "ambitiTerritoriali" where confine is not null
)
As f ;

QUERY;
        $results = DB::selectOne(DB::raw($query));

        $outFile = fopen("dataset/out/ambiti.json", "w");
        if (!$outFile) {
            $this->error("Can't open out file");
            return;
        }

        $json = (array)$results;
        $json['features'] = (array)json_decode($json['features']);
        fwrite($outFile, json_encode($json));
        fclose($outFile);
    }

    private function produciGeoJSONUnioneComuni()
    {
        $query = <<<QUERY
    SELECT 'FeatureCollection' As type, array_to_json(array_agg(f))
As features FROM
(SELECT
'Feature' As type,
concat('unione_',id) as id,
ST_AsGeoJSON((confine),15,4)::json As geometry,
json_build_object('id', id, 'nome', nome) As properties
FROM "unioniComuni" where confine is not null
)
As f ;

QUERY;
        $results = DB::selectOne(DB::raw($query));

        $outFile = fopen("dataset/out/unioniComuni.json", "w");
        if (!$outFile) {
            $this->error("Can't open out file");
            return;
        }

        $json = (array)$results;
        $json['features'] = (array)json_decode($json['features']);
        fwrite($outFile, json_encode($json));
        fclose($outFile);
    }

    private function assegnaConfineDistretti()
    {

        $query = 'select ST_Multi(ST_Union(ST_MakeValid(confine))) as confine, "numero_distretto" from comuni where confine is not null group by "numero_distretto"';
        $results = DB::select(DB::raw($query));
//        var_dump($results);
        foreach ($results as $result) {
            /** @var Distretto $distretto */
            $distretto = Distretto::query()->find($result->numero_distretto);
            if ($distretto == null) {
                $distretto = new Distretto();
                $distretto->numero = $result->numero_distretto;
            }
            $distretto->confine = $result->confine;
            $distretto->save();
        }
    }


    private function produciGeoJSONDistretti()
    {
        $query = <<<QUERY
    SELECT 'FeatureCollection' As type, array_to_json(array_agg(f))
As features FROM
(SELECT
'Feature' As type,
concat('distretto_',numero) as id,
ST_AsGeoJSON((confine),15,4)::json As geometry,

json_build_object('numero', numero) As properties
FROM "distretti" where confine is not null
)
As f ;

QUERY;
        $results = DB::selectOne(DB::raw($query));

        $outFile = fopen("dataset/out/distretti.json", "w");
        if (!$outFile) {
            $this->error("Can't open out file");
            return;
        }

        $json = (array)$results;
        $json['features'] = (array)json_decode($json['features']);
        fwrite($outFile, json_encode($json));
        fclose($outFile);
    }
}
