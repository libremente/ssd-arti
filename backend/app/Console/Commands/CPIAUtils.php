<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Console\Commands;

use App\Models\CPIAPuntoErogazioneLivello1;
use App\Models\CPIAPuntoErogazioneLivello2;
use App\Models\SedeCiclo2;
use Exception;
use Geocoder\Collection;
use Geocoder\Model\Address;
use Geocoder\Model\Coordinates;
use Geocoder\Query\GeocodeQuery;
use Illuminate\Console\Command;
use Phaza\LaravelPostgis\Geometries\Point;

class CPIAUtils extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cpia:ottieniCoordinate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->recuperaCoordinateLivello1();
        $this->recuperaCoordinateLivello2();

    }

    /**
     * @param $indirizzo
     * @param $comune
     * @return Coordinates | null
     */
    private function getCoordinate($indirizzo, $comune)
    {
        /** @var $punto CPIAPuntoErogazioneLivello1 */
        $address = GeocodeQuery::create("$indirizzo, $comune, Italy");
        try {
            /** @var Collection $geocode */
            $geocode = app('geocoder')->geocode($address)->get();
            $coordinates = $geocode->get(0)->getCoordinates();
            /** @var Address $geocode ->items[0]->coordinates->latitude */
        } catch (Exception $exception) {
            $coordinates = null;
        }
        return $coordinates;

    }

    private function recuperaCoordinateLivello1()
    {
        $puntiSenzaCoordinateLivello1 = CPIAPuntoErogazioneLivello1::query()
            ->where(['coordinate' => null])
            ->get();

        /** @var CPIAPuntoErogazioneLivello1 $punto */
        foreach ($puntiSenzaCoordinateLivello1 as $punto) {
            $coordinates = $this->getCoordinate($punto->indirizzo, $punto->comune);
            if ($coordinates != null) {
                $punto->coordinate = new Point($coordinates->getLatitude(), $coordinates->getLongitude());
                $punto->save();
            } else {
                $this->error("Can't retrieve coordinate for $punto->codiceMeccanograficoPuntoErogazione");
            }
        }

    }

    private function recuperaCoordinateLivello2()
    {
        $puntiSenzaCoordinateLivello2 = CPIAPuntoErogazioneLivello2::query()
            ->where(['coordinate' => null]);

        /** @var CPIAPuntoErogazioneLivello2 $punto */
        foreach ($puntiSenzaCoordinateLivello2 as $punto) {

            $sedeCicloCollegato = SedeCiclo2::query()->whereKey($punto->codiceMeccanograficoIS)->first();
            if ($sedeCicloCollegato != null) {
                $punto->coordinate = $sedeCicloCollegato->coordinate;
                $punto->save();
            } else {

                $coordinates = $this->getCoordinate($punto->indirizzo, $punto->comune);
                if ($coordinates != null) {
                    $punto->coordinate = new Point($coordinates->getLatitude(), $coordinates->getLongitude());
                    $punto->save();
                } else {
                    $this->error("Can't retrieve coordinate for $punto->codiceMeccanograficoPuntoErogazione");
                }
            }
        }

    }
}
