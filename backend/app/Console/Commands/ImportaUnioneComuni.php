<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Console\Commands;

use App\Helpers\ImportHelpers;
use App\Models\UnioneComuni;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ImportaUnioneComuni extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importa:unioniComuni';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('unioniComuni')->truncate();
        $unioni = $this->leggiUnioni();
        foreach ($unioni as $nome => $codiciComuni) {
            $unione = new UnioneComuni();
            $unione->nome = $nome;
            $unione->codiciComuni = $codiciComuni;
            $unione->save();
        }
    }

    private function leggiUnioni()
    {
        $fileName = __DIR__ . "/../../../dataset/UNIONE DI COMUNI.xlsx";
        //0. load file
        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileName);

        $checks = [

            'E1' => "Codice Catastale",
            'F1' => "Unione dei Comuni"
        ];

        $worksheet = $spreadsheet->getSheet(0);
        ImportHelpers::performFormatCheck($worksheet, $checks);


        $unioni = [];
        foreach ($worksheet->getRowIterator(2) as $row) {
            $rowIndex = $row->getRowIndex();
            $nomeUnione = $worksheet->getCell("F$rowIndex")->getValue();
            if ($nomeUnione == "") {
                continue;
            }
            $codiceCatastaleComune = $worksheet->getCell("E$rowIndex")->getValue();
            if (!isset($unioni[$nomeUnione])) {
                $unioni[$nomeUnione] = [];
            }
            $unioni[$nomeUnione][] = $codiceCatastaleComune;
        }
        return $unioni;


    }
}
