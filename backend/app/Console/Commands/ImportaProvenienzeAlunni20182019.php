<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Console\Commands;


use App\Helpers\ImportHelpers;
use App\Models\AnnoScolastico;
use App\Models\DistribuzioneResidenzaAlunniSedeCiclo2;
use App\Models\SedeCiclo2;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ImportaProvenienzeAlunni20182019 extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importa:provenienzeAlunni20182019';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'importa provenienze alunni 2018/2019';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $annoScolastico = AnnoScolastico::nuovoAnnoScolastico("2018/2019");

        $sqlDelete = <<<SQL
delete from "distribuzioneResidenzaAlunniSedeCiclo2" where "idSedeCiclo2" in
        (
            Select "sediCiclo2".id from "sediCiclo2"
            join "istituzioniScolastiche" on "sediCiclo2"."idIstituzioneScolastica" = "istituzioniScolastiche".id
            where "idAnnoScolastico" = ?
        );
SQL;

        $deleted = DB::delete($sqlDelete, [$annoScolastico->id]);
        $this->warn("Deleted $deleted");
        $this->info("Importing");

        $this->importa20182019($annoScolastico);


    }


    private function importa20182019(AnnoScolastico $annoScolastico)
    {


        $fileName = __DIR__ . "/../../../dataset/provenienze/Provenienze 18_19.xlsx";
        //
        //0. load file
        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileName);

        $error = 0;
        $success = 0;
        $this->info("Provenienze alunni Grado 2");

        $provenienze = $this->ottieniProvenienze20182019($spreadsheet);
        $this->info("provenienze");


        foreach ($provenienze as $provenienza) {
            $sede = SedeCiclo2::trovaCodiceAnno($provenienza['codicePe'], $annoScolastico->id);
            if ($sede != null && $sede->id != null) {
                $provenienzaModel = new DistribuzioneResidenzaAlunniSedeCiclo2();
                $provenienzaModel->codiceCatastaleComune = $provenienza['codiceComune'];
                $provenienzaModel->idSedeCiclo2 = $sede->id;
                $nAlunni = preg_replace('/\s+/', '', $provenienza['nAlunni']);
                if ($nAlunni == "<3") {
                    $provenienzaModel->numeroAlunni = null;
                    $provenienzaModel->numeroAlunniInferiore3 = true;
                } else {
                    $provenienzaModel->numeroAlunni = $nAlunni;
                    $provenienzaModel->numeroAlunniInferiore3 = false;
                }
                try {
                    $provenienzaModel->save();
                    $success++;
                } catch (\Exception $exception) {
                    $error++;
                    $this->error($exception->getMessage());
                }
            } else {
                $this->error("Sede non trovata per ${provenienza['codicePe']}, $annoScolastico->id");
                $error++;
            }
        }


        //grado 2


        if ($error != 0) {
            $this->error("Si sono verificati $error errori");
        }
        if ($success != 0) {
            $this->info("$success inseriti con successo");
        }
    }


    /**
     * @param Spreadsheet $spreadsheet
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function ottieniProvenienze20182019(Spreadsheet $spreadsheet)
    {

        $provenienze = [];
        $worksheet = $spreadsheet->getSheet(0);

        $HEADER_ROW_IDX = 1;

        $checks = [
            'A1' => "Prov",
            'D1' => "CodIS",
            'G1' => "codPE",
            'H1' => "cod_com",
            'J1' => "N° alunni per Comune di provenienza"
        ];

        //1. perform "format" check
        ImportHelpers::performFormatCheck($worksheet, $checks);

        //2. parse data
        foreach ($worksheet->getRowIterator($HEADER_ROW_IDX + 1) as $row) {
            $rowIndex = $row->getRowIndex();
            $codicePe = $worksheet->getCell("G$rowIndex")->getValue();
            if (ImportHelpers::checkIfEmptyCell($codicePe)) {
                continue;
            }

            $codiceComune = $worksheet->getCell("H$rowIndex")->getValue();
            $nAlunni = $worksheet->getCell("J$rowIndex")->getValue();


            $provenienze[] = compact('codicePe', 'codiceComune', 'nAlunni');
        }

        return $provenienze;
    }


}
