<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Console\Commands;

use App\Helpers\ImportHelpers;
use App\Models\AlunniIs;
use App\Models\AlunniSedeCiclo1;
use App\Models\AlunniSedeCiclo2;
use App\Models\AnnoScolastico;
use Illuminate\Console\Command;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ImportaAndamenti extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importa:andamenti';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*
         * select "codiceSedeCiclo2", "label", SUM("totaleAlunni") as totaleAlunni from ssd."alunniSedeCiclo2" as a2
         * join ssd."anniScolastici" as anni on a2."idAnnoScolastico"= anni.id
         * group by "codiceSedeCiclo2", "label" order by a2."codiceSedeCiclo2";
         */
        $annoScolastico1617 = AnnoScolastico::nuovoAnnoScolastico("2016/2017");
        $annoScolastico1718 = AnnoScolastico::nuovoAnnoScolastico("2017/2018");
        $annoScolastico1819 = AnnoScolastico::nuovoAnnoScolastico("2018/2019");

        $fileName = __DIR__ . "/../../../dataset/andamenti/Andamento triennale Punti di erogazione.xlsx";

        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileName);


        $dataIs = $this->ottieniTotaliPerIs($spreadsheet);

        $this->info("Alunni IS");

        $errors = 0;
        foreach ($dataIs as $is) {

            $annoScolastico = null;
            switch ($is['anno']) {
                case "2016/2017":
                    $annoScolastico = $annoScolastico1617;
                    break;
                case "2017/2018":
                    $annoScolastico = $annoScolastico1718;
                    break;
                case "2018/2019":
                    $annoScolastico = $annoScolastico1819;
                    break;
            }
            try {
                $alunniIs = AlunniIs::nuovo($is['codice'], $annoScolastico);
                $nAlunni = $is['nAlunni'];
                $nClassi = $is['nClassi'];
                $nDisabili = $is['nDisabili'];

                if ($nAlunni != null) {
                    $alunniIs->totaleAlunni = $nAlunni;
                }
                if ($nClassi != null) {
                    $alunniIs->totaleClassi = $nClassi;
                }
                if ($nDisabili != null) {
                    $alunniIs->totaleAlunniDisabili = $nDisabili;
                }
                $alunniIs->save();
            } catch (\Exception $exception) {
//                $this->error("Anno scolastico = " . $is['anno']);
                $this->error($exception->getMessage());
                $errors++;
            }

        }
        if ($errors != 0) {
            $this->error("$errors Errori");
            $errors = 0;
        }

        $errors = 0;
        $this->info("Primo ciclo");
        $primoCiclo = $this->ottieniPrimoCiclo($spreadsheet);
        foreach ($primoCiclo as $sede) {

            $annoScolastico = -1;
            switch ($sede['anno']) {
                case "2016/2017":
                    $annoScolastico = $annoScolastico1617;
                    break;
                case "2017/2018":
                    $annoScolastico = $annoScolastico1718;
                    break;
                case "2018/2019":
                    $annoScolastico = $annoScolastico1819;
                    break;
            }
            try {
                $alunniSede = AlunniSedeCiclo1::nuovo($sede['codice'], $annoScolastico);
                $nAlunni = $sede['nAlunni'];
                $nClassi = $sede['nClassi'];
                $nDisabili = $sede['nDisabili'];

                if ($nAlunni != null) {
                    $alunniSede->totaleAlunni = $nAlunni;
                }
                if ($nClassi != null) {
                    $alunniSede->totaleClassi = $nClassi;
                }
                if ($nDisabili != null) {
                    $alunniSede->totaleAlunniDisabili = $nDisabili;
                }
                $alunniSede->save();
            } catch (\Exception $exception) {
//                $this->error("Anno scolastico = " . $sede['anno']);
//                $this->error($exception->getMessage());
                $errors++;
            }
        }
        if ($errors != 0) {
            $this->error("$errors Errori");
            $errors = 0;
        }

        $this->info("Secondo ciclo");
        $secondoCiclo = $this->ottieniSecondoCiclo($spreadsheet);
        foreach ($secondoCiclo as $sede) {

            $annoScolastico = -1;
            switch ($sede['anno']) {
                case "2016/2017":
                    $annoScolastico = $annoScolastico1617;
                    break;
                case "2017/2018":
                    $annoScolastico = $annoScolastico1718;
                    break;
                case "2018/2019":
                    $annoScolastico = $annoScolastico1819;
                    break;
            }
            try {
                $alunniSede = AlunniSedeCiclo2::nuovo($sede['codice'], $sede['codiceIndirizzo'], $annoScolastico);
                $nAlunni = $sede['nAlunni'];
                $nClassi = $sede['nClassi'];
                $nDisabili = $sede['nDisabili'];

                if ($nAlunni != null) {
                    $alunniSede->totaleAlunni = $nAlunni;
                }
                if ($nClassi != null) {
                    $alunniSede->totaleClassi = $nClassi;
                }
                if ($nDisabili != null) {
                    $alunniSede->totaleAlunniDisabili = $nDisabili;
                }
                $alunniSede->save();
            } catch (\Exception $exception) {
//                $this->error("Anno scolastico = " . $sede['anno']);
//                $this->error($exception->getMessage());
                $errors++;
            }
        }
        if ($errors != 0) {
            $this->error("$errors Errori");
            $errors = 0;
        }

    }


    /**
     * @param Spreadsheet $spreadsheet
     * @return array
     * @throws Exception
     */
    public function ottieniTotaliPerIs(Spreadsheet $spreadsheet)
    {


        $dataIs = [];
        $worksheet = $spreadsheet->getSheet(0);
        {
            $HEADER_ROW_IDX = 1;

            $checks = [
                'B1' => "CODICE",
                'D1' => "TOTALE 2018/209",

                'E1' => "CODICE",
                'G1' => "TOTALE 2017/2018",

                'H1' => "CODICE",
                'J1' => "TOTALE 2016/2017",
            ];


            ImportHelpers::performFormatCheck($worksheet, $checks);

            foreach ($worksheet->getRowIterator($HEADER_ROW_IDX + 1) as $row) {
                $rowIndex = $row->getRowIndex();
                $codice = $worksheet->getCell("B$rowIndex")->getValue();
                if (ImportHelpers::checkIfEmptyCell($codice)) {
                    continue;
                }
                $anno = "2018/2019";

                $nAlunni = $worksheet->getCell("D$rowIndex")->getValue();
                $nDisabili = null;
                $nClassi = null;

                $dataIs[] = compact('codice', 'anno', 'nAlunni', 'nClassi', 'nDisabili');
            }

            foreach ($worksheet->getRowIterator($HEADER_ROW_IDX + 1) as $row) {
                $rowIndex = $row->getRowIndex();
                $codice = $worksheet->getCell("E$rowIndex")->getValue();
                if (ImportHelpers::checkIfEmptyCell($codice)) {
                    continue;
                }
                $anno = "2017/2018";

                $nAlunni = $worksheet->getCell("G$rowIndex")->getValue();
                $nDisabili = null;
                $nClassi = null;

                $dataIs[] = compact('codice', 'anno', 'nAlunni', 'nClassi', 'nDisabili');
            }

            foreach ($worksheet->getRowIterator($HEADER_ROW_IDX + 1) as $row) {
                $rowIndex = $row->getRowIndex();
                $codice = $worksheet->getCell("H$rowIndex")->getValue();
                if (ImportHelpers::checkIfEmptyCell($codice)) {
                    continue;
                }
                $anno = "2016/2017";

                $nAlunni = $worksheet->getCell("J$rowIndex")->getValue();
                $nDisabili = null;
                $nClassi = null;

                $dataIs[] = compact('codice', 'anno', 'nAlunni', 'nClassi', 'nDisabili');
            }
        }
        return $dataIs;
    }

    private function ottieniPrimoCiclo(Spreadsheet $spreadsheet)
    {
        $dataPrimoCiclo = [];

        foreach (["2016/2017", "2017/2018", "2018/2019"] as $index => $anno) {


            $worksheet = $spreadsheet->getSheet(1 + $index);

            $HEADER_ROW_IDX = 1;

            $checks = [
                'C1' => "Codice Plesso",
                'F1' => "Alunni",
            ];


            ImportHelpers::performFormatCheck($worksheet, $checks);

            foreach ($worksheet->getRowIterator($HEADER_ROW_IDX + 1) as $row) {
                $rowIndex = $row->getRowIndex();
                $codice = $worksheet->getCell("C$rowIndex")->getValue();
                if (ImportHelpers::checkIfEmptyCell($codice)) {
                    continue;
                }

                $nAlunni = $worksheet->getCell("F$rowIndex")->getValue();
                $nDisabili = null;
                $nClassi = null;

                $dataPrimoCiclo[] = compact('codice', 'anno', 'nAlunni', 'nClassi', 'nDisabili');
            }
        }
        return $dataPrimoCiclo;
    }

    private function ottieniSecondoCiclo(Spreadsheet $spreadsheet)
    {
        $dataSecondoCiclo = [];

        foreach (["2016/2017", "2017/2018", "2018/2019"] as $index => $anno) {


            $worksheet = $spreadsheet->getSheet(4 + $index);

            $HEADER_ROW_IDX = 1;

            $checks = [
                'C1' => "Cod. Plesso",
                'E1' => "Cod. ind.",
                'G1' => "Alunni",
            ];


            ImportHelpers::performFormatCheck($worksheet, $checks);

            foreach ($worksheet->getRowIterator($HEADER_ROW_IDX + 1) as $row) {
                $rowIndex = $row->getRowIndex();
                $codice = $worksheet->getCell("C$rowIndex")->getValue();
                if (ImportHelpers::checkIfEmptyCell($codice)) {
                    continue;
                }
                $codiceIndirizzo = $worksheet->getCell("E$rowIndex")->getValue();
                $nAlunni = $worksheet->getCell("G$rowIndex")->getValue();
                $nDisabili = null;
                $nClassi = null;

                $dataSecondoCiclo[] = compact('codice', 'codiceIndirizzo', 'anno', 'nAlunni', 'nClassi', 'nDisabili');
            }
        }
        return $dataSecondoCiclo;
    }
}
