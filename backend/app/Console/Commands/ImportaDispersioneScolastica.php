<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Console\Commands;

use App\Helpers\ImportHelpers;
use App\Models\AnnoScolastico;
use App\Models\DispersioneScolastica;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ImportaDispersioneScolastica extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importa:dispersioneScolastica';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->importa20152016();
    }

    private function importa20152016()
    {
        $annoScolastico = AnnoScolastico::nuovoAnnoScolastico("2015/2016");
        DB::delete('delete FROM "dispersioneScolastica" where "idAnnoScolastico" = ?', [$annoScolastico->id]);
        //rimuovo dalla tabella tutti quelli con stesso idAnnoScolastico
        $dispersione = $this->read20152016Ciclo1();
        $this->save($annoScolastico->id, DispersioneScolastica::$ciclo1, $dispersione);
        $dispersione = $this->read20152016Ciclo2();
        $this->save($annoScolastico->id, DispersioneScolastica::$ciclo2, $dispersione);

    }

    private function read20152016Ciclo1()
    {


        $fileName = __DIR__ . "/../../../dataset/dispersioneScolastica/Dati Dispersione Puglia Primo Ciclo 15.16.xlsx";
        //0. load file
        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileName);

        $checks = [
            "A1" => "Comuni",
            "I1" => "Iscritti",
            "P1" => "Abbandoni",
            "AC1" => "Evasioni",
            "AJ1" => "Ripetenze",
            "AQ1" => "Irregolari"
        ];

        $worksheet = $spreadsheet->getSheet(0);
        ImportHelpers::performFormatCheck($worksheet, $checks);


        $dispersioni = [];
        foreach ($worksheet->getRowIterator(3) as $row) {
            $rowIndex = $row->getRowIndex();
            $comune = $worksheet->getCell("A$rowIndex")->getValue();
            if ($comune == "") {
                continue;
            }
            $comune = str_replace(" Totale", "", $comune);
            $iscritti = $worksheet->getCell("I$rowIndex")->getCalculatedValue();
            $abbandoni = $worksheet->getCell("P$rowIndex")->getCalculatedValue();
            $evasioni = $worksheet->getCell("AC$rowIndex")->getCalculatedValue();
            $ripentenze = $worksheet->getCell("AJ$rowIndex")->getCalculatedValue();
            $irregolari = $worksheet->getCell("AQ$rowIndex")->getCalculatedValue();
            $dispersioni[] = compact('comune', 'iscritti', 'abbandoni', 'evasioni', 'ripentenze', 'irregolari');
        }
        return $dispersioni;
    }

    private function read20152016Ciclo2()
    {


        $fileName = __DIR__ . "/../../../dataset/dispersioneScolastica/Dati Dispersione Puglia Secondo Ciclo 15.16.xlsx";
        //0. load file
        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileName);

        $checks = [
            "B1" => "Comuni",
            "J1" => "Iscritti",
            "Q1" => "Abbandoni",
            "X1" => "Evasioni",
            "AE1" => "Ripetenze",
            "AL1" => "Frequenze Irregolari"
        ];

        $worksheet = $spreadsheet->getSheet(0);
        ImportHelpers::performFormatCheck($worksheet, $checks);


        $dispersioni = [];
        foreach ($worksheet->getRowIterator(3) as $row) {
            $rowIndex = $row->getRowIndex();
            $comune = $worksheet->getCell("B$rowIndex")->getValue();
            if ($comune == "") {
                continue;
            }
            $comune = str_replace(" Totale", "", $comune);
            $iscritti = $worksheet->getCell("J$rowIndex")->getCalculatedValue();
            $abbandoni = $worksheet->getCell("Q$rowIndex")->getCalculatedValue();
            $evasioni = $worksheet->getCell("XC$rowIndex")->getCalculatedValue();
            $ripentenze = $worksheet->getCell("AE$rowIndex")->getCalculatedValue();
            $irregolari = $worksheet->getCell("AL$rowIndex")->getCalculatedValue();
            $dispersioni[] = compact('comune', 'iscritti', 'abbandoni', 'evasioni', 'ripentenze', 'irregolari');
        }
        return $dispersioni;
    }

    /** @noinspection PhpUndefinedVariableInspection */
    private function save(int $idAnnoScolastico, string $ciclo, array $dispersione)
    {

        foreach ($dispersione as $item) {
            extract($item);

            $comuneModel = ImportHelpers::comuneByNome($comune);
            if ($comuneModel == null) {
                $this->error("$comune non trovato");
                continue;
            } else {
                $dispersione = new DispersioneScolastica();
                $dispersione->codiceCatastaleComune = $comuneModel->codiceCatastale;
                $dispersione->idAnnoScolastico = $idAnnoScolastico;
                $dispersione->ciclo = $ciclo;
                $dispersione->iscritti = $iscritti;
                $dispersione->abbandoni = $abbandoni;
                $dispersione->evasioni = $evasioni;
                $dispersione->ripetenze = $ripentenze;
                $dispersione->frequenzeIrregolari = $irregolari;

                $dispersione->save();
            }

        }


    }
}
