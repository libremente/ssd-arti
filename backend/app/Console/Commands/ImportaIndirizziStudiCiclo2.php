<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Console\Commands;

use App\Helpers\ImportHelpers;
use App\Models\IndirizzoStudioCiclo2;
use App\Models\TipologiaIndirizzo2Ciclo;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ImportaIndirizziStudiCiclo2 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importa:indirizziStudioCiclo2';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importa gli indirizzi di studio per le scuole di secondo ciclo';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return int
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     */
    public function handle()
    {
        DB::statement('TRUNCATE ssd."indirizziStudioCiclo2" CASCADE');

        DB::beginTransaction();
        $fileName = __DIR__ . "/../../../dataset/confluenze indirizzi.xlsx";
        $inserted = $this->parse($fileName);
        DB::commit();

        $this->output->success("Success: $inserted inserted!");

        return 1;

    }


    public function parse($fileName)
    {

        //0. load file
        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileName);

        $checks = [
            'A1' => "LA",
            'B1' => "LICEO ARTISTICO",
        ];
        ImportHelpers::performFormatCheck($spreadsheet->getSheet(1), $checks);

        $fogli = [
            "Liceo" => $spreadsheet->getSheet(1),
            "Istituto Professionale" => $spreadsheet->getSheet(2),
            "Istituto Tecnico" => $spreadsheet->getSheet(3),
        ];

        $inserted = 0;
        foreach ($fogli as $tipologia => $foglio) {

            $data = $this->parseConfluenze($foglio);

            foreach ($data as $datum) {
                $indirizziDiStudio = new IndirizzoStudioCiclo2();
                $tipoIndirizzo = $this->tipoIndirizzoByNome($tipologia);
                $indirizziDiStudio->codice = $datum['codiceIndirizzo'];
                $indirizziDiStudio->idTipologia = $tipoIndirizzo->id;
                $indirizziDiStudio->denominazione = $datum['denominazioneIndirizzo'];
                $indirizziDiStudio->codicePadre = $datum['codicePadre'];
                $indirizziDiStudio->save();
                $inserted++;
            }
        }

        return $inserted;


    }


    private function tipoIndirizzoByNome($nome)
    {
        $model = TipologiaIndirizzo2Ciclo::query()->where(['nome' => $nome])->first();
        if ($model == null) {
            $model = new TipologiaIndirizzo2Ciclo();
            $model->nome = $nome;
            $model->save();
        }
        return $model;
    }

    private function tipoIndirizzoIstitutoTecnico($settore)
    {
        $nome = "Istituto tecnico";
        $model = TipologiaIndirizzo2Ciclo::query()->where(['nome' => $nome, 'settore' => $settore])->first();
        if ($model == null) {
            $model = new TipologiaIndirizzo2Ciclo();
            $model->nome = $nome;
            $model->settore = $settore;
            $model->save();
        }
        return $model;
    }

    private function parseConfluenze(Worksheet $foglio)
    {

        $data = [];

        //2. parse data
        $indirizzoPadrePrecedente = null;
        foreach ($foglio->getRowIterator(1) as $row) {
            $rowIndex = $row->getRowIndex();
            $codiceIndirizzo = $foglio->getCell("A$rowIndex")->getValue();
            if (ImportHelpers::checkIfEmptyCell($codiceIndirizzo)) {
                continue;
            }
            $denominazioneIndirizzo = $foglio->getCell("B$rowIndex")->getValue();
            $codicePadre = $foglio->getCell("C$rowIndex")->getValue();;
            $data[] = compact('codiceIndirizzo', 'codicePadre', 'denominazioneIndirizzo');
        }
        return $data;
    }

}
