<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Console\Commands;


use App\Helpers\ImportHelpers;
use App\Models\AnnoScolastico;
use App\Models\DistribuzioneResidenzaAlunniSedeCiclo2;
use App\Models\SedeCiclo2;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ImportaProvenienzeAlunni20172018 extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importa:provenienzeAlunni20172018';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'importa provenienze alunni 2017/2018';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('distribuzioneResidenzaAlunniSedeCiclo2')->truncate();

        $this->importa20172018();


    }


    private function importa20172018()
    {


        $annoScolastico = AnnoScolastico::nuovoAnnoScolastico("2017/2018");
        $fileName = __DIR__ . "/../../../dataset/provenienze/31_Alunni_Puglia_1718.xlsx";
        //
        //0. load file
        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileName);

        $error = 0;
        $this->info("Provenienze alunni Grado 2");

        $provenienze = $this->ottieniProvenienze20172018($spreadsheet);
        $this->info("provenienze");


        foreach ($provenienze["SedeCiclo2"] as $provenienza) {
            $sede = SedeCiclo2::trovaCodiceAnno($provenienza['codicePlesso'], $annoScolastico->id);
            if ($sede != null && $sede->id != null) {
                $provenienzaModel = new DistribuzioneResidenzaAlunniSedeCiclo2();
                $provenienzaModel->codiceCatastaleComune = $provenienza['codiceComune'];
                $provenienzaModel->idSedeCiclo2 = $sede->id;
                $nAlunni = $provenienza['nAlunni'];
                if ($nAlunni == "<3") {
                    $provenienzaModel->numeroAlunniInferiore3 = true;
                } else {
                    $provenienzaModel->numeroAlunni = $nAlunni;
                }
                try {
                    $provenienzaModel->save();
                } catch (\Exception $exception) {
                    $error++;
                    $this->error($exception->getMessage());
                }
            } else {
                $error++;
            }
        }


        //grado 2


        if ($error != 0) {
            $this->error("Si sono verificati $error errori");
        }
    }


    /**
     * @param Spreadsheet $spreadsheet
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function ottieniProvenienze20172018(Spreadsheet $spreadsheet)
    {

        $mapping = [
//            0 => "SedeCiclo1",
//            1 => "SedeCiclo1",
//            2 => "SedeCiclo1",
            3 => "SedeCiclo2",
        ];
        $provenienze = [];


        foreach ($mapping as $key => $map) {


            $worksheet = $spreadsheet->getSheet($key);

            $HEADER_ROW_IDX = 1;

            $checks = [
                'A1' => "Provincia_scuola",
                'B1' => "codice_plesso",
                'C1' => "codice_comune_residenza_alunno",
                'D1' => "numero_alunni"
            ];

            //1. perform "format" check
            ImportHelpers::performFormatCheck($worksheet, $checks);

            //2. parse data
            foreach ($worksheet->getRowIterator($HEADER_ROW_IDX + 1) as $row) {
                $rowIndex = $row->getRowIndex();
                $codicePlesso = $worksheet->getCell("B$rowIndex")->getValue();
                if (ImportHelpers::checkIfEmptyCell($codicePlesso)) {
                    continue;
                }

                $codiceComune = $worksheet->getCell("C$rowIndex")->getValue();
                $nAlunni = $worksheet->getCell("D$rowIndex")->getValue();


                $provenienze[$map][] = compact('codicePlesso', 'codiceComune', 'nAlunni');
            }
        }
        return $provenienze;
    }


}
