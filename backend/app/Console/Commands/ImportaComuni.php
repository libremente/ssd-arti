<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Console\Commands;

use App\Helpers\ImportHelpers;
use App\Models\Comune;
use App\Models\Distretto;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Phaza\LaravelPostgis\Geometries\Polygon;
use PhpOffice\PhpSpreadsheet\IOFactory;
use ShapeFile\ShapeFile;
use ShapeFile\ShapeFileException;

class ImportaComuni extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importa:comuni';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importa i dati Istat e i confini dei comuni pugliesi';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::transaction(function () {
            DB::statement('TRUNCATE ssd.comuni CASCADE');
            $this->info("Importing comuni");
            $this->importaDatiIstat();
            $this->info("Importing confini");
            $this->assegnaConfineComuni();
            $this->assegnaAmbitoEDistretto();
            $this->output->success("Done");
        });
    }

    private function importaDatiIstat()
    {
        $fileName = __DIR__ . "/../../../dataset/comuni/Elenco-comuni-italiani.xls";

        $codicePuglia = 16;

        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileName);


        $worksheet = $spreadsheet->getSheet(0);

        //1. perform "format" check
        $checks = [
            'A1' => "Codice Regione",
            'G1' => "Denominazione in italiano",
            'L1' => "Denominazione dell'Unità territoriale sovracomunale
(valida a fini statistici)",
            'K1' => "Denominazione regione",
            'O1' => "Codice Comune formato numerico",
            'S1' => "Codice Catastale del comune"
        ];

        ImportHelpers::performFormatCheck($worksheet, $checks);
        //2. parse data


        foreach ($worksheet->getRowIterator(2) as $row) {
            $rowIndex = $row->getRowIndex();
            $codiceRegione = $worksheet->getCell("A$rowIndex")->getValue();
//            if ($codiceRegione != $codicePuglia) {
//                continue;
//            }
            $nomeComune = $worksheet->getCell("G$rowIndex")->getValue();
            $provincia = $worksheet->getCell("L$rowIndex")->getValue();
            $regione = $worksheet->getCell("K$rowIndex")->getValue();
            $codiceIstat = (int)$worksheet->getCell("O$rowIndex")->getValue();
            $codiceCatastale = $worksheet->getCell("S$rowIndex")->getValue();

            $comune = new Comune();
            $comune->nome = $nomeComune;
            $comune->provincia = $provincia;
            $comune->codiceCatastale = $codiceCatastale;
            $comune->codiceIstat = $codiceIstat;
            $comune->regione = $regione;
            $comune->save();

        }
    }

    private function assegnaConfineComuni()
    {
        $fileName = __DIR__ . "/../../../dataset/ambiti/comuni/comuni.shp";
        $confiniNotFound = [];


        DB::statement('update comuni set confine = null');
        try {
            // Open shapefile
            $shapeFile = new ShapeFile($fileName);


            // Read all the records
            while ($record = $shapeFile->getRecord(ShapeFile::GEOMETRY_WKT)) {
                if ($record['dbf']['_deleted']) continue;
                $codiceIstat = $record['dbf']['ISTAT'];
                $comune = Comune::query()->where('codiceIstat', '=', $codiceIstat)->first();
                if ($comune == null) {
                    $this->warn("Comune '$codiceIstat' non presente in DB");
                }

                try {
                    $confine = Polygon::fromWKT($record['shp']);

                    if ($comune == null) {
                        $confiniNotFound[$codiceIstat] = $confine;
                        continue;
                    }

                    $this->aggiornaConfineComune($comune, $confine);
                } catch (\Exception $exception) {
                    $this->error($exception->getMessage());
                }
            }

        } catch (ShapeFileException $e) {
            // Print detailed error information
            exit('Error ' . $e->getCode() . ' (' . $e->getErrorType() . '): ' . $e->getMessage());
        }
        //fusione  Comune di Acquarica del Capo - Presicce
        $codiceCatastale = "M428";
        $acquaricaPresicce = Comune::query()->find($codiceCatastale);
        $acquaricaPresicceConfini = array_values(array_filter($confiniNotFound, function ($codiceIStat) {
            return $codiceIStat == "75062" || $codiceIStat == "75001";
        }, ARRAY_FILTER_USE_KEY));
        $this->aggiornaConfineMultiploComune($acquaricaPresicce, $acquaricaPresicceConfini);

    }

    private function assegnaAmbitoEDistretto()
    {
        $fileName = __DIR__ . "/../../../dataset/comuni/distretti_def.xlsx";

        $codicePuglia = 16;

        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileName);


        $worksheet = $spreadsheet->getSheet(0);

        //1. perform "format" check
        $checks = [
            'A1' => "COMUNE",
            'B1' => "Ambito_USR",
            'C1' => "Numero_Amb",
            'D1' => "Distretto",
        ];

        ImportHelpers::performFormatCheck($worksheet, $checks);
        //2. parse data


        foreach ($worksheet->getRowIterator(2) as $row) {
            $rowIndex = $row->getRowIndex();
            $comune = $worksheet->getCell("A$rowIndex")->getValue();

            $ambito = $worksheet->getCell("B$rowIndex")->getValue();
            $distretto = $worksheet->getCell("D$rowIndex")->getValue();


            $comuneModel = ImportHelpers::comuneByNome($comune);
            if ($comuneModel == null) {
                $this->error("Comune '$comune' non presente in DB");
                continue;
            }
            $this->assicuraDistretto($distretto);
            $comuneModel->numero_distretto = $distretto;
            $comuneModel->ambitoTerritoriale = $ambito;
            $comuneModel->save();

        }
    }

    private function aggiornaConfineComune(Comune $comune, $confine)
    {
        if (true) {
            $query = <<<QUERY
update "comuni" set "confine" = ST_GeomFromText('POLYGON ($confine)',27700) where "codiceCatastale" = '$comune->codiceCatastale';
QUERY;
            DB::statement($query);
        } else {
            //Da testare, ora dovrebbe funzionare
            $comune->confine = $confine;
            $comune->save();
        }
    }

    private function aggiornaConfineMultiploComune(Comune $comune, array $confine)
    {

        $query = <<<QUERY
update "comuni" set "confine" = ST_Multi(ST_Union(
    ST_GeomFromText('POLYGON ($confine[0])',27700),
    ST_GeomFromText('POLYGON ($confine[1])',27700)))
    where "codiceCatastale" = '$comune->codiceCatastale';
QUERY;
        DB::statement($query);

    }

    private function assicuraDistretto($numero_distretto): Distretto
    {
        $distretto = Distretto::query()->find($numero_distretto);
        if ($distretto == null) {
            $distretto = new Distretto();
            $distretto->numero = $numero_distretto;
            $distretto->save();
        }
        return $distretto;
    }
}
