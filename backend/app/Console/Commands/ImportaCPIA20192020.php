<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Console\Commands;

use App\Helpers\ImportHelpers;
use App\Models\AnnoScolastico;
use App\Models\CPIA;
use App\Models\CPIAPuntoErogazioneLivello1;
use App\Models\CPIAPuntoErogazioneLivello2;
use App\Models\CPIATipoPercorsoLivello1;
use App\Models\CPIATipoSede;
use App\Models\IndirizzoStudioCiclo2;
use Illuminate\Console\Command;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ImportaCPIA20192020 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importa:cpia';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'importa i CPIA ';

    /**
     * @var AnnoScolastico
     */
    private $annoScolastico;


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $annoScolasticoStr = "2019/2020";
        $this->annoScolastico = AnnoScolastico::nuovoAnnoScolastico($annoScolasticoStr);
        CPIA::query()->where(['idAnnoScolastico' => $this->annoScolastico->id])->truncate();
//        DB::transaction(function () {
        $fileName = __DIR__ . "/../../../dataset/CPIA/CPIA _RICOGNIZIONE.xls";
        $this->importa($fileName, true, true, true);
//        });


    }

    /**
     *
     * @param string $fileName
     * @param $cpia
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     */
    private function importa(string $fileName, bool $cpia, bool $livello1, bool $livello2)
    {
        //0. load file
        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileName);


        $percorsiLivello1 = $this->parsePercorsiILivello($spreadsheet);
        $this->inserisciPercorsi($percorsiLivello1);

        $tipoSede = $this->parseTipoSede($spreadsheet);
        $this->inserisciTipiSede($tipoSede);


        if ($cpia) {
            $this->info("======================= Sedi Amministrative\n\n");
            DB::statement('TRUNCATE ssd."CPIA" CASCADE');
            $sediAmministrative = $this->parseSediAmministrative($spreadsheet);

            $success = 0;

            foreach ($sediAmministrative as $sedeData) {
                $sede = new CPIA();
                $sede->idAnnoScolastico = $this->annoScolastico->id;
                $sede->codice = $sedeData['codice'];
                $sede->provincia = $sedeData['provincia'];
                $sede->denominazione = $sedeData['denominazione'];
                $sede->indirizzo = $sedeData['indirizzo'];
                $sede->attoIstitutivo = $sedeData['attoIstitutivo'];
                $sede->comune = $sedeData['comune'];
                $sede->indirizzo = $sedeData['indirizzo'];
                $sede->email = $sedeData['email'];
                $sede->pec = $sedeData['pec'];
                $sede->telefono = $sedeData['telefono'];
//                $sede->coordinate = ImportHelpers::makePoint($sedeData['lat'], $sedeData['lon']);


                try {
                    $sede->save();
                    $success++;
                } catch (QueryException $e) {
                    $this->error($e->getMessage());
                }
            }
            $this->info("Inserite $success sedi amministrative");
        }
        if ($livello1) {
            $this->info("======================= Punti erogazioni livello 1\n\n");
            DB::table("CPIAPuntiErogazioneLivello1")->truncate();
            $puntiErogazioneLivello1 = $this->parsePuntiErogazioneLivello1($spreadsheet);
            $success = $this->inserisciPuntiErogazioneLivello1($puntiErogazioneLivello1);
            $this->info("Inserite $success punti erogazioni livello 1");
        }
        if ($livello2) {
            $this->info("======================= Punti erogazioni livello 2\n\n");
            DB::table("CPIAPuntiErogazioneLivello2")->truncate();
            $puntiErogazioneLivello2 = $this->parsePuntiErogazioneLivello2($spreadsheet);
            $success = $this->inserisciPuntiErogazioneLivello2($puntiErogazioneLivello2);
            $this->info("Inserite $success punti erogazioni livello 2");
        }
    }

    /**
     * @param Spreadsheet $spreadsheet
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function parseSediAmministrative(Spreadsheet $spreadsheet)
    {
        $worksheet = $spreadsheet->getSheet(0);

        $checks = [
            'A1' => "Provincia",
            'B1' => "Codice meccanografico",
            'C1' => "Denominazione",
            'D1' => "Atto istitutivo",
            'E1' => "Comune",
            'F1' => "Indirizzo",
            'G1' => "Indirizzo email PEO",
            'H1' => "Indirizzo email PEC",
            'I1' => "Telefono",
            'J2' => "Percorsi di alfabetizzazione livello A2 + Percorsi di primo livello",
            'K2' => "Percorsi di alfabetizzazione livello pre A2",
            "L2" => "Percorsi di secondo livello",
            "M2" => "TOTALE Organico"
        ];
        ImportHelpers::performFormatCheck($worksheet, $checks);

        $data = [];

        foreach ($worksheet->getRowIterator(3) as $row) {
            $rowIndex = $row->getRowIndex();


            $provincia = $worksheet->getCell("A$rowIndex")->getCalculatedValue();
            if (ImportHelpers::checkIfEmptyCell($provincia)) {
                break;
            }
            $codice = $worksheet->getCell("B$rowIndex")->getCalculatedValue();
            $denominazione = $worksheet->getCell("C$rowIndex")->getCalculatedValue();
            $attoIstitutivo = $worksheet->getCell("D$rowIndex")->getCalculatedValue();
            $comune = $worksheet->getCell("E$rowIndex")->getCalculatedValue();
            $indirizzo = $worksheet->getCell("F$rowIndex")->getCalculatedValue();
            $email = $worksheet->getCell("G$rowIndex")->getCalculatedValue();
            $pec = $worksheet->getCell("H$rowIndex")->getCalculatedValue();
            $telefono = $worksheet->getCell("I$rowIndex")->getCalculatedValue();


            $data[] = compact('provincia', 'codice', 'denominazione', 'attoIstitutivo', 'comune', 'indirizzo', 'email',
                'pec', 'telefono');
        }
        return $data;


    }


    /**
     * @param Spreadsheet $spreadsheet
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function parsePuntiErogazioneLivello1(Spreadsheet $spreadsheet)
    {
        $worksheet = $spreadsheet->getSheet(1);
        $checks = [
            'A1' => "Provincia",
            'B1' => "Denominazione",
            'C1' => "Cod. meccanografico punto di erogazione",
            'D1' => "Denominazione punto erogazione",
            'E1' => "Comune",
            'F1' => "Indirizzo",
            'J1' => "Tipologia sede associata",
            'L1' => "CORREZIONE - Tipologia percorso erogato",
            'M1' => "Numero patti formativi",
        ];
        ImportHelpers::performFormatCheck($worksheet, $checks);

        $data = [];

        foreach ($worksheet->getRowIterator(2) as $row) {
            $rowIndex = $row->getRowIndex();

            $provincia = $worksheet->getCell("A$rowIndex")->getCalculatedValue();
            $denominazioneCPIA = $worksheet->getCell("B$rowIndex")->getCalculatedValue();
            if (ImportHelpers::checkIfEmptyCell($denominazioneCPIA)) {
                break;
            }
            $codicePuntoErogazione = trim($worksheet->getCell("C$rowIndex")->getCalculatedValue());

            $denominazionePuntoErogazione = $worksheet->getCell("D$rowIndex")->getCalculatedValue();
            $comune = $worksheet->getCell("E$rowIndex")->getCalculatedValue();
            $indirizzo = $worksheet->getCell("F$rowIndex")->getCalculatedValue();
            $tipologiaSedeAssociata = $worksheet->getCell("J$rowIndex")->getCalculatedValue();
            $percorsoErogato = $worksheet->getCell("L$rowIndex")->getCalculatedValue();
            $numeroPattiFormativi = $worksheet->getCell("M$rowIndex")->getCalculatedValue();


            $data[] = compact('denominazioneCPIA', 'codicePuntoErogazione', 'denominazionePuntoErogazione',
                'provincia', 'comune', 'indirizzo', 'tipologiaSedeAssociata', 'percorsoErogato', 'numeroPattiFormativi');
        }
        return $data;


    }

    /**
     * @param array $puntiErogazioneLivello1
     * @return int success
     * @throws \Exception
     */
    private function inserisciPuntiErogazioneLivello1(array $puntiErogazioneLivello1): int
    {
        $success = 0;
        foreach ($puntiErogazioneLivello1 as $puntoData) {
            /**
             *
             * 'indirizzo', 'tipologiaSedeAssociata', '', 'numeroPattiFormativi', 'numeroDiversamenteAbili'
             */
            $punto = $this->getPuntoErogazioneLivello1($puntoData);
            $percorsoErogato = $this->getPercorsoLivello1ByName($puntoData['percorsoErogato']);
            try {
                $punto->percorsiErogati()->attach($percorsoErogato, [
                    'nPattiFormativi' => ImportHelpers::checkNd($puntoData['numeroPattiFormativi']),
                    'nDiversamenteAbili' => null
                ]);
                $success++;
            } catch (QueryException $e) {
//                $this->error($e->getMessage());
                $this->error("Impossibile effettuare associazioni {$punto->codiceMeccanograficoPuntoErogazione} -> percorso \"$percorsoErogato->percorso\"");
            }
        }
        return $success;
    }

    /**
     * @param $puntoData
     * @return CPIAPuntoErogazioneLivello1
     * @throws \Exception
     */
    private function getPuntoErogazioneLivello1($puntoData): CPIAPuntoErogazioneLivello1
    {
        $puntoErogazione = CPIAPuntoErogazioneLivello1::query()->where([
                'codiceMeccanograficoPuntoErogazione' => $puntoData['codicePuntoErogazione'],
                'comune' => $puntoData['comune']
            ]
        )->first();
        if ($puntoErogazione == null) {
            $puntoErogazione = new CPIAPuntoErogazioneLivello1();
            $CPIA = CPIA::query()->where(['denominazione' => strtoupper($puntoData['denominazioneCPIA'])])->first();
            if ($CPIA == null) {
                throw new \Exception("Cpia \"" . $puntoData['denominazioneCPIA'] . "\" not found!");
            }

            $puntoErogazione->codiceMeccanograficoPuntoErogazione = $puntoData['codicePuntoErogazione'];
            $puntoErogazione->idCPIA = $CPIA->id;
            $puntoErogazione->denominazione = $puntoData['denominazionePuntoErogazione'];
            $puntoErogazione->idTipoSede = $this->getIdTipoSede($puntoData['tipologiaSedeAssociata']);
            $puntoErogazione->comune = $puntoData['comune'];
            $puntoErogazione->provincia = $puntoData['provincia'];
            $puntoErogazione->indirizzo = $puntoData['indirizzo'];
            $puntoErogazione->save();
        }
        return $puntoErogazione;

    }

    private function getPercorsoLivello1ByName($nomePercorsoErogato): CPIATipoPercorsoLivello1
    {
        $percorso = CPIATipoPercorsoLivello1::query()->where(['percorso' => $nomePercorsoErogato])->first();
        if ($percorso == null) {
            $percorso = new CPIATipoPercorsoLivello1();
            $percorso->percorso = $nomePercorsoErogato;
            $percorso->save();
        }
        return $percorso;

    }

    /**
     * @param Spreadsheet $spreadsheet
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function parsePuntiErogazioneLivello2(Spreadsheet $spreadsheet)
    {
        $worksheet = $spreadsheet->getSheet(2);
        $HEADER_ROW_IDX = 1;
        $checks = [
            'A1' => "Provincia",
            'B1' => "Denominazione",
            'C1' => "Cod. meccanografico  IS II grado",
            'D1' => "Denominazione  IS II grado",
            'E1' => "Comune punto di erogazione",
            'F1' => "Indirizzo punto di erogazione",
            'G1' => "Indirizzo email PEO",
            'H1' => "Indirizzo email PEC",
            'I1' => "Telefono",
            'J1' => "Codice indirizzo del Percorso di II livello",
            'K1' => "Denominazione indirizzo del Percorso di II livello",
            'L1' => "Numero patti formativi",
        ];
        ImportHelpers::performFormatCheck($worksheet, $checks);

        $data = [];

        foreach ($worksheet->getRowIterator($HEADER_ROW_IDX + 1) as $row) {
            $rowIndex = $row->getRowIndex();

            $provincia = $worksheet->getCell("A$rowIndex")->getCalculatedValue();
            $denominazioneCPIA = $worksheet->getCell("B$rowIndex")->getCalculatedValue();
            if (ImportHelpers::checkIfEmptyCell($denominazioneCPIA)) {
                break;
            }
            $codiceIs = trim($worksheet->getCell("C$rowIndex")->getCalculatedValue());
            $denominazioneIs = trim($worksheet->getCell("D$rowIndex")->getCalculatedValue());
            $comune = $worksheet->getCell("E$rowIndex")->getCalculatedValue();
            $indirizzo = $worksheet->getCell("F$rowIndex")->getCalculatedValue();

            $email = $worksheet->getCell("G$rowIndex")->getCalculatedValue();
            $pec = $worksheet->getCell("H$rowIndex")->getCalculatedValue();
            $telefono = $worksheet->getCell("I$rowIndex")->getCalculatedValue();

            $codicePercorso = trim($worksheet->getCell("J$rowIndex")->getCalculatedValue());
            $denominazionePercorso = $worksheet->getCell("K$rowIndex")->getCalculatedValue();

            $numeroPattiFormativi = $worksheet->getCell("L$rowIndex")->getCalculatedValue();

            $data[] = compact('denominazioneCPIA', 'codiceIs', 'denominazioneIs', 'denominazionePuntoErogazione',
                'provincia', 'comune', 'indirizzo', 'codicePercorso', 'denominazionePercorso',
                'numeroPattiFormativi', 'pec', 'email', 'telefono');
        }
        return $data;


    }

    /**
     * @param array $puntiErogazioneLivello1
     * @return int
     * @throws \Exception
     */
    private function inserisciPuntiErogazioneLivello2(array $puntiErogazioneLivello1): int
    {
        $success = 0;
        foreach ($puntiErogazioneLivello1 as $puntoData) {
            /**
             *
             * 'indirizzo', 'tipologiaSedeAssociata', '', 'numeroPattiFormativi', 'numeroDiversamenteAbili'
             */
            /** @var CPIAPuntoErogazioneLivello2 */
            try {
                $punto = $this->getPuntoErogazioneLivello2($puntoData);
            } catch (QueryException $e) {
                $this->error($e->getMessage());
                continue;
            }
            $codiceIndirizzoStudio = $puntoData['codicePercorso'];
            $denominazionePercorso = $puntoData['denominazionePercorso'];
            if (ImportHelpers::checkNd($codiceIndirizzoStudio) != null) {
                $indirizzoStudio = $this->getPercorsoIndirizzoStudioCiclo2($codiceIndirizzoStudio, $denominazionePercorso);
                if ($indirizzoStudio == null) {
                    $this->error("Indirizzo di studio '$codiceIndirizzoStudio' non trovato");
                }
                try {
                    $punto->indirizziDiStudio()->attach($indirizzoStudio, [
                        'nPattiFormativi' => ImportHelpers::checkNd($puntoData['numeroPattiFormativi']),
                        'nDiversamenteAbili' => null
                    ]);
                    $success++;
                } catch (QueryException $e) {
                    $this->error($e->getMessage());
                }
            } else {
                $this->alert("Il codice indirizzo di studio '$codiceIndirizzoStudio' non è valido ");
            }
//            if ($punto->coordinate == null) {
//                $this->alert("Le coordinate del punto $punto->codiceMeccanograficoIS, $punto->comune sono mancanti");
//            }
        }
        return $success;
    }


    private function getPuntoErogazioneLivello2($puntoData): CPIAPuntoErogazioneLivello2
    {

        $puntoErogazione = CPIAPuntoErogazioneLivello2::query()->where([
            'codiceMeccanograficoIS' => $puntoData['codiceIs'],
            'comune' => $puntoData['comune']
        ])->first();
        if ($puntoErogazione == null) {
            $puntoErogazione = new CPIAPuntoErogazioneLivello2();
            $CPIA = CPIA::query()->where(['denominazione' => strtoupper($puntoData['denominazioneCPIA'])])->first();
            if ($CPIA == null) {
                throw new \Exception("Cpia \"" . $puntoData['denominazioneCPIA'] . "\" not found!");
            }

            $puntoErogazione->codiceMeccanograficoIS = $puntoData['codiceIs'];
            $puntoErogazione->idCPIA = $CPIA->id;
//            $puntoErogazione->coordinate = ImportHelpers::makePoint($puntoData['lat'], $puntoData['lon']);
            $puntoErogazione->comune = $puntoData['comune'];
            $puntoErogazione->provincia = $puntoData['provincia'];
            $puntoErogazione->indirizzo = $puntoData['indirizzo'];
            $puntoErogazione->denominazioneIS = $puntoData['denominazioneIs'];
            $puntoErogazione->email = $puntoData['email'];
            $puntoErogazione->pec = $puntoData['pec'];
            $puntoErogazione->telefono = $puntoData['telefono'];
            $puntoErogazione->save();
        }
        return $puntoErogazione;

    }


    private function getPercorsoIndirizzoStudioCiclo2($codicePercorso, $denominazionePercorso): ?IndirizzoStudioCiclo2
    {
        $percorso = IndirizzoStudioCiclo2::query()->whereKey($codicePercorso)->first();
        if ($percorso == null) {
            $percorso = new IndirizzoStudioCiclo2();
            $percorso->codice = $codicePercorso;
            $percorso->denominazione = $denominazionePercorso;
            $percorso->save();
        }
        return $percorso;

    }

    private function parsePercorsiILivello(Spreadsheet $spreadsheet)
    {
        $worksheet = $spreadsheet->getSheet(3);

        $data = [];

        foreach ($worksheet->getRowIterator(0) as $row) {
            $rowIndex = $row->getRowIndex();

            $percorso = $worksheet->getCell("D$rowIndex")->getCalculatedValue();
            if (ImportHelpers::checkIfEmptyCell($percorso)) {
                break;
            }

            $data[] = $percorso;
        }
        return $data;

    }

    private function inserisciPercorsi(array $percorsi)
    {
        foreach ($percorsi as $percorso) {
            $model = CPIATipoPercorsoLivello1::findByPercorso($percorso);
            if ($model == null) {
                $model = new CPIATipoPercorsoLivello1();
                $model->percorso = $percorso;
                $model->save();
            }
        }
    }


    private function parseTipoSede(Spreadsheet $spreadsheet)
    {
        $worksheet = $spreadsheet->getSheet(3);

        $data = [];

        foreach ($worksheet->getRowIterator(0) as $row) {
            $rowIndex = $row->getRowIndex();

            $tipoSede = $worksheet->getCell("C$rowIndex")->getCalculatedValue();
            if (ImportHelpers::checkIfEmptyCell($tipoSede)) {
                break;
            }

            $data[] = $tipoSede;
        }
        return $data;

    }


    private function inserisciTipiSede(array $tipiSede)
    {
        foreach ($tipiSede as $tipoSede) {
            $model = CPIATipoSede::findByTipo($tipoSede);
            if ($model == null) {
                $model = new CPIATipoSede();
                $model->tipo = $tipoSede;
                $model->save();
            }
        }
    }

    private function getIdTipoSede($tipo)
    {
        $model = CPIATipoSede::findByTipo($tipo);
        if ($model != null) {
            return $model->id;
        } else {
            $model = new CPIATipoSede();
            $model->tipo = $tipo;
            $model->save();
            return $model->id;
        }
    }


}
