<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Console\Commands;

use App\Helpers\ImportHelpers;
use App\Helpers\ImportTraits;
use App\Models\AnnoScolastico;
use Illuminate\Console\Command;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception;

//TODO: mancano i dettagli sulle Sedi

class ImportaReteScolastica20172018 extends Command
{

    use ImportTraits;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importa:reteScolastica20172018';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return int
     */
    public function handle()
    {

        $this->eliminaAnnoScolastico("2017/2018");
        $annoScolastico = AnnoScolastico::nuovoAnnoScolastico("2017/2018");

        $this->info("IS");
        $this->handleIs($annoScolastico);
        $this->info("Sedi");
        $this->output->error("Dati non disponibili");
        $this->info("Dettagli alunni");
        $this->output->error("Dati non disponibili");
        $this->output->success("Done");

        return 1;

    }


    private function handleIS(AnnoScolastico $annoScolastico)
    {

        //nelle prossime versioni potrebbe essere preso da stdin
        $fileName = __DIR__ . "/../../../dataset/81_Decreto Ambiti Territoriali a.s. 2017-18 - Allegato1-convertito.xlsx";
        //
        $istituzioniScolastiche = $this->parse2017($fileName);

        $this->importaTipiIstituti($istituzioniScolastiche);

        foreach ($istituzioniScolastiche as $is) {
            $this->inserisciIS($is, $annoScolastico);
        }


    }

    /**
     * @param $fileName
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     */
    public function parse2017($fileName)
    {
        //0. load file
        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileName);

        $istuzioniScolastiche = [];
        $HEADER_ROW_IDX = 1;
        foreach ($spreadsheet->getAllSheets() as $worksheet) {

            $checks = [
                'A1' => "Codice Istituzione
Scolastica",
                'B1' => "Tipo Istituto",
                'C1' => "Denominazione Istituzione Scolastica",
                'E1' => "Note"

            ];
            //1. perform "format" check
            ImportHelpers::performFormatCheck($worksheet, $checks);

            //2. parse data
            foreach ($worksheet->getRowIterator($HEADER_ROW_IDX + 1) as $row) {
                $rowIndex = $row->getRowIndex();
                $codiceIS = $worksheet->getCellByColumnAndRow(1, $rowIndex)->getValue();
                if (is_null($codiceIS) || $codiceIS == "") {
                    break;
                }
                $tipoIS = $worksheet->getCellByColumnAndRow(2, $rowIndex)->getValue();
                $denominazione = $worksheet->getCellByColumnAndRow(3, $rowIndex)->getValue();
                $comuneSedeDirigenza = $worksheet->getCellByColumnAndRow(4, $rowIndex)->getValue();
                $multiAmbito = (boolean)$worksheet->getCellByColumnAndRow(5, $rowIndex)->getValue() != "";
                $codiceAmbito = $worksheet->getCellByColumnAndRow(7, $rowIndex)->getValue();

                //print "c: $codiceIS, tIS: $tipoIS, d: $denominazione, multia: $multiAmbito, ca: $codiceAmbito \n";
                $istuzioniScolastiche[] = [
                    'codiceIS' => $codiceIS,
                    'tipoIS' => $tipoIS,
                    'comuneSedeDirigenza' => $comuneSedeDirigenza,
                    'denominazioneIs' => $denominazione,
                    'multiAmbito' => $multiAmbito,
                    'codiceAmbito' => $codiceAmbito
                ];
            }
        }
        return $istuzioniScolastiche;
    }


}

