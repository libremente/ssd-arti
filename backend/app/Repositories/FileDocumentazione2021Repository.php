<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Repositories;


use App\Models\FileDocumentazione2021;
use Illuminate\Support\Facades\Storage;

class FileDocumentazione2021Repository
{
    public function getHeader()
    {
        $header = FileDocumentazione2021::query()->get('fileName')->map(function ($item) {
            return $item['fileName'];
        })->unique();
        $header->prepend("Comune/i punti di erogazione");
        $header->prepend("Denominazione istituzione scolastica");
        $header->prepend("Codice istituzione scolastica");
        $header->prepend("Provincia");
        return $header;//array_merge(['Comune'], $header);
    }

    public function getFilesByComune()
    {
        $files = FileDocumentazione2021::query()
            ->orderBy('provincia')->orderBy('comune')->orderBy('codice')
            ->get(['provincia', 'comune', 'codice', 'denominazione', 'fileName', 'filePath']);
        $province = $files->groupBy(['provincia', 'comune', 'codice']);
        $filesByComune = [];
        foreach ($province as $nomeProvincia => $provincia) {

            foreach ($provincia as $nomeComune => $comuni) {
                foreach ($comuni as $files) {

                    $fileComune = [];
                    $fileComune['Provincia'] = $nomeProvincia;
                    $fileComune['Comune/i punti di erogazione'] = $nomeComune;

                    foreach ($files as $file) {
                        if (!isset($fileComune['codice'])) {
                            $fileComune['Codice istituzione scolastica'] = $file['codice'];
                            $fileComune['Denominazione istituzione scolastica'] = $file['denominazione'];
                        }

                        $filePath = $file['filePath'];
                        if ($filePath != null) {
                            $url = Storage::disk('public')->url($filePath);
                            $fileComune[$file['fileName']] = $url;
                        } else {
                            $fileComune[$file['fileName']] = null;
                        }

                    }
                    $filesByComune[] = $fileComune;

                }
            }
        }

        return $filesByComune;
    }
}
