<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Repositories;


use App\Models\CPIA;
use App\Models\CPIAPuntoErogazioneLivello1;
use App\Models\CPIAPuntoErogazioneLivello2;
use App\Models\CPIATipoPercorsoLivello1;
use App\Models\IndirizzoStudioCiclo2;
use Illuminate\Support\Facades\DB;

class CPIARepository
{

    public function list()
    {
        return CPIA::query()->get();
    }

    public function getLivello1()
    {
        $livello1 = CPIA::has("puntiErogazione1Livello")
            ->with([
                "puntiErogazione1Livello",
                "puntiErogazione1Livello.percorsiErogati"])
            ->get();

        /** @var CPIA $item */
        foreach ($livello1 as $item) {
            /** @var CPIAPuntoErogazioneLivello1 $punto */
            foreach ($item->puntiErogazione1Livello as $punto) {
                /** @var CPIATipoPercorsoLivello1 $percorso */
                foreach ($punto->percorsiErogati as $key => $percorso) {
                    $percorsoFormattato = $percorso->toArray();
                    $percorsoFormattato['nDiversamenteAbili'] = $percorso->pivot->nDiversamenteAbili;
                    $percorsoFormattato['nPattiFormativi'] = $percorso->pivot->nPattiFormativi;
                    unset($percorsoFormattato['pivot']);
                    unset($percorsoFormattato['id']);
                    $punto->percorsiErogati[$key] = $percorsoFormattato;

                }
            }
        }
        return $livello1;
    }

    public function getLivello2()
    {
        $livello2 = CPIA::has("puntiErogazione2Livello")
            ->with([
                "puntiErogazione2Livello",
                "puntiErogazione2Livello.indirizziDiStudio"])
            ->get();


        /** @var CPIA $item */
        foreach ($livello2 as $item) {
            /** @var CPIAPuntoErogazioneLivello2 $punto */
            foreach ($item->puntiErogazione2Livello as $punto) {
                /** @var IndirizzoStudioCiclo2 $indirizzo */
                foreach ($punto->indirizziDiStudio as $key => $indirizzo) {
                    $percorsoFormattato = $indirizzo->toArray();
                    $percorsoFormattato['nDiversamenteAbili'] = $indirizzo->pivot->nDiversamenteAbili;
                    $percorsoFormattato['nPattiFormativi'] = $indirizzo->pivot->nPattiFormativi;
                    unset($percorsoFormattato['pivot']);
                    unset($percorsoFormattato['id']);
                    $punto->indirizziDiStudio[$key] = $percorsoFormattato;

                }
            }
        }
        return $livello2;
    }

    public function getCPIAResume($idAnno)
    {
        $query = <<<SQL
select
      provincia,
       codice,
       denominazione,
       comune,
       indirizzo,
       email,
       pec,
       telefono,
       "attoIstitutivo",
       null as "percorsiA2Livello1",
       null as "percorsiPreA2",
       null as "percorsiLivello2",
       null as "totaleOrganico"
from "CPIA"
where "idAnnoScolastico"=:idAnno;
SQL;


        $data = DB::select(DB::raw($query), [':idAnno' => $idAnno]);

        $header = [
            ['name' => "provincia", 'label' => "Provincia"],
            ['name' => "codice", 'label' => "Codice meccanografico"],
            ['name' => "denominazione", 'label' => "Denominazione"],
            ['name' => "attoIstitutivo", 'label' => "Atto Istitutivo"],
            ['name' => "comune", 'label' => "Comune"],
            ['name' => "indirizzo", 'label' => "Indirizzo"],
            ['name' => "email", 'label' => "Indirizzo email PEO"],
            ['name' => "pec", 'label' => "Indirizzo email PEC"],
            ['name' => "telefono", 'label' => "Telefono"],
            ['name' => "percorsiA2Livello1", 'label' => "Percorsi di alfabetizzazione livello A2 + Percorsi di primo livello"],
            ['name' => "percorsiPreA2", 'label' => "Percorsi di alfabetizzazione livello pre A2"],
            ['name' => "percorsiLivello2", 'label' => "Percorsi di secondo livello"],
            ['name' => "totaleOrganico", 'label' => "TOTALE Organico"],
        ];

        return [
            'header' => $header,
            'data' => $data
        ];
    }

    public function getLivello1Resume($idAnno)
    {
        $query = <<<SQL
select C.provincia,
       C.denominazione,
       "codiceMeccanograficoPuntoErogazione",
       P.denominazione as "denominazionePE",
       P.comune,
       P.indirizzo,
       TIPI.tipo as "tipoSede",
       CTPL1.percorso,
       L."nPattiFormativi"
from "CPIAPuntiErogazioneLivello1" P
         join "CPIA" C on P."idCPIA" = C.id
         join "CPIAPuntiErogazioneLivello1HasCPITipiPercorsoLivello1" L
              on P.id = L."CPIAPuntiErogazioneLivello1_id"
        join "CPIATipiPercorsoLivello1" CTPL1 on L."CPIATipiPercorsoLivello1_id" = CTPL1.id
        join "CPIATipiSede" TIPI on P."idTipoSede" = TIPI.id
        where "idAnnoScolastico"=:idAnno;
SQL;

        $data = DB::select(DB::raw($query), [':idAnno' => $idAnno]);

        $header = [
            ['name' => "provincia", 'label' => "Provincia"],
            ['name' => "denominazione", 'label' => "Denominazione"],
            ['name' => "codiceMeccanograficoPuntoErogazione", "label" => "Cod. meccanografico punto di erogazione"],
            ['name' => "denominazionePE", "label" => "Denominazione Punto Erogazione"],
            ['name' => "comune", 'label' => "Comune"],
            ['name' => "indirizzo", 'label' => "Indirizzo"],
            ['name' => "tipoSede", "label" => "Tipologia sede associata"],
            ['name' => "percorso", 'label' => "Tipologia percorso erogato"],
            ['name' => "nPattiFormativi", 'label' => "Numero Patti Formativi"]
        ];

        return [
            'header' => $header,
            'data' => $data
        ];

    }

    public function getLivello2Resume($idAnno)
    {
        $query = <<<SQL
select C.provincia,
       C.denominazione,
       P."codiceMeccanograficoIS" as "codiceIS",
       P."denominazioneIS",
       P.comune,
       P.indirizzo,
       P.email,
       P.pec,
       P.telefono,
       indirizzo.codice as "codiceIndirizzo",
       indirizzo.denominazione as "nomeIndirizzo",
       L."nPattiFormativi"
from "CPIAPuntiErogazioneLivello2" P
         join "CPIA" C on P."idCPIA" = C.id
         join "CPIAPuntiErogazioneLivello2HasIndirizziStudioCiclo2" L on P.id = L."CPIAPuntiErogazioneLivello2_id"
         join "indirizziStudioCiclo2" as indirizzo on L."indirizziStudioCiclo2_codice" = indirizzo.codice
         where "idAnnoScolastico"=:idAnno;
SQL;

        $data = DB::select(DB::raw($query), [':idAnno' => $idAnno]);

        $header = [
            ['name' => "provincia", 'label' => "Provincia"],
            ['name' => "denominazione", 'label' => "Denominazione"],
            ['name' => "codiceIS", "label" => "Codice meccanografico IS"],
            ['name' => "denominazioneIS", "label" => "Denominazione IS"],
            ['name' => "comune", 'label' => "Comune Punto Erogazione"],
            ['name' => "indirizzo", 'label' => "Indirizzo Punto Erogazione"],
            ['name' => "email", 'label' => "Indirizzo email PEO"],
            ['name' => "pec", 'label' => "Indirizzo email PEC"],
            ['name' => "telefono", 'label' => "Telefono"],

            ['name' => "codiceIndirizzo", 'label' => "Codice indirizzo del Percorso di II livello"],
            ['name' => "nomeIndirizzo", 'label' => "Denominazione indirizzo del Percorso di II livello"],
            ['name' => "nPattiFormativi", 'label' => "Numero Patti Formativi"]
        ];

        return [
            'header' => $header,
            'data' => $data
        ];
    }
}
