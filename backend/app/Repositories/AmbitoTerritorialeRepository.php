<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Repositories;


use App\Models\AmbitoTerritoriale;
use Illuminate\Support\Facades\DB;


class AmbitoTerritorialeRepository
{

    public function constructor()
    {

    }

    public function ambitiTerritorialiData($ambiti)
    {
        $is = new ISRepository();
        foreach ($ambiti as $ambito) {
            $this->ambitoTerritorialeData($ambito, $is);

        }
        return $ambiti;
    }


    public function ambitoTerritorialeData(AmbitoTerritoriale $ambito, ISRepository $is)
    {
        $ambito->istituzioniScolastiche;
        $ambito->codice;
        $ambito->provincia;
        $ambito->comuni()->select("codiceCatastale", "nome", "provincia", "regione", "cap", "codiceIstat");
        $is->istituzioniScolasticheData($ambito->istituzioniScolastiche);
        return $ambito;
    }

    public function statistiche(int $idAnnoScolastico)
    {

        $totaliIsAmbitoQuery = <<<QUERY
       Select ist."codiceAmbito"  as "codice",
       count("codiceAmbito") as "totaleIs",
       sum(al."totaleAlunni") as "totaleAlunniAmbito",
       sum(al."totaleAlunniDisabili") as "totaleAlunniDisabiliAmbito",
       sum(al."totaleClassi") as "totaleClassiAmbito"
       from "istituzioniScolastiche" as ist join "alunniIs" as al on ist.id = al."idIs"
       where ist."idAnnoScolastico" = ?
       group by ist."codiceAmbito"
       order by ist."codiceAmbito"
QUERY;


        $totaliIsAmbito = DB::select(DB::raw($totaliIsAmbitoQuery), [$idAnnoScolastico]);

        return $totaliIsAmbito;
    }


    public function index(int $idAnnoScolastico)
    {
        //todo: inserire la divisione per anni scolastici
        $ambiti = AmbitoTerritoriale::all(['codice', 'provincia']);
        return $this->ambitiTerritorialiData($ambiti);

    }
    public function codici(int $idAnnoScolastico)
    {
        //todo: inserire la divisione per anni scolastici
       return AmbitoTerritoriale::all(['codice']);


    }
}
