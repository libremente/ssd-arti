<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Repositories;


use App\Models\IndirizzoStudioCiclo2;
use App\Models\SedeCiclo2;
use App\Models\SedeCiclo2HasIndirizziStudio;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;


class SediCiclo2Repository
{


    public function sediData($sedi)
    {
        if ($sedi == null) {
            return null;
        }
        if (is_array($sedi) || $sedi instanceof Collection) {

            foreach ($sedi as $sede) {
                self::assicuraCampi($sede);
            }
            return $sedi;
        } else {
            $sedi_array = [];
            self::assicuraCampi($sedi);
            array_push($sedi_array, $sedi);
            return $sedi_array;
        }

    }

    private static function assicuraCampi(SedeCiclo2 $sede)
    {

        $sede->codice;
        $sede->coordinate;
        $sede->codiceCatastaleComune;
        $sede->comune;
        $sede->indirizzo;
        $sede->tipo;
        $sede->sedeDirezioneAmministrativa;
        $sede->tipologiaScuola;
        $sede->codiceEdificio;
        $sede->istituzioneScolastica;
        $sede->istituzioneScolastica->alunni;
        $sede->tipologiaScuola;
        $sede->alunni;
        $sede->indirizziDiStudio;
        self::assicuraCampiIndirizziDiStudio($sede->indirizziDiStudio);

    }

    private static function assicuraCampiIndirizziDiStudio($indirizziDiStudio)
    {

        /** @var IndirizzoStudioCiclo2 $indirizzi */
        foreach ($indirizziDiStudio as $indirizzi) {
            $indirizzi->tipologiaIndirizzo2Ciclo;
        }

    }


    public function list(bool $showOnlySedeAmministrative, int $idAnnoScolastico)
    {

        $builder = SedeCiclo2::query()
            ->whereHas(
                "istituzioneScolastica",
                function ($query) use ($idAnnoScolastico) {
                    $query->where('idAnnoScolastico', '=', $idAnnoScolastico);
                }
            )
            ->with([
                "istituzioneScolastica" => function ($query) use ($idAnnoScolastico) {
                    $query->where('idAnnoScolastico', '=', $idAnnoScolastico);
                },
                "alunni",
                "indirizziDiStudio",
                "indirizziDiStudio.tipologiaIndirizzo2Ciclo",
            ])
            ->where('coordinate', '<>', null);

        if ($showOnlySedeAmministrative) {
            $builder->where('sedeDirezioneAmministrativa', "=", 'true');
        }

        return $builder->get();

    }

    public function get(string $codice, int $idAnnoScolastico): ?array
    {
        $sede = SedeCiclo2::trovaCodiceAnno($codice, $idAnnoScolastico);
        return $this->sediData($sede);
    }

    public function getSediByIstituzione($codiceIstituzione)
    {
        return $this->sediData(SedeCiclo2::where('codiceIstituzione', $codiceIstituzione)->get());
    }

    public function listByIndirizzo(string $indirizzo, int $idAnnoScolastico)
    {

        $idSedi = SedeCiclo2HasIndirizziStudio::query()
            ->where("indirizzoStudioCiclo2_codice", "=", $indirizzo)
            ->get('idSedeCiclo2');


        return $this->sediConIndirizzo($idSedi, $idAnnoScolastico);
    }

    public function listByIndirizzi(array $indirizzi, int $idAnnoScolastico)
    {
        $idSedi = SedeCiclo2HasIndirizziStudio::query()
            ->whereIn("indirizzoStudioCiclo2_codice", $indirizzi)
            ->get('idSedeCiclo2');

        return $this->sediConIndirizzo($idSedi, $idAnnoScolastico);

    }

    private function sediConIndirizzo(\Illuminate\Database\Eloquent\Collection $idSedi, int $idAnnoScolastico)
    {
        $sediConIndirizzo = SedeCiclo2::query()
            ->whereHas(
                "istituzioneScolastica",
                function ($query) use ($idAnnoScolastico) {
                    $query->where('idAnnoScolastico', '=', $idAnnoScolastico);
                }
            )
            ->with([
                "istituzioneScolastica" => function ($query) use ($idAnnoScolastico) {
                    $query->where('idAnnoScolastico', '=', $idAnnoScolastico);
                },
                "alunni",
                "indirizziDiStudio",
                "indirizziDiStudio.tipologiaIndirizzo2Ciclo",
            ])
            ->whereIn('id', $idSedi)->get();


        return $sediConIndirizzo;
    }

    public function find(int $idSede): ?SedeCiclo2
    {
        return SedeCiclo2::query()
            ->with([
                "istituzioneScolastica",
                "alunni",
                "indirizziDiStudio",
                "indirizziDiStudio.tipologiaIndirizzo2Ciclo",
            ])
            ->find($idSede);
    }

    public function provenienze(int $idAnno)
    {
        $query = <<<QUERY
select cSede.provincia    as "ProvinciaPE",
       cSede.nome         as "ComunePE",
       ist.denominazione  as "DenominazioneIS",
       sede.codice        as "CodicePE",
       sede.denominazione as "DenominazionePE",
       cProvenienza.nome  as "ComuneProvenienza",
       distr."numeroAlunni",
       distr."numeroAlunniInferiore3"
from "istituzioniScolastiche" as ist
         join "sediCiclo2" as sede on ist.id = sede."idIstituzioneScolastica"
         join "distribuzioneResidenzaAlunniSedeCiclo2" as distr on sede.id = distr."idSedeCiclo2"
         join comuni cProvenienza on distr."codiceCatastaleComune" = cProvenienza."codiceCatastale"
         join comuni cSede on sede."codiceCatastaleComune" = cSede."codiceCatastale"
where ist."idAnnoScolastico" = ?
order by cSede.provincia, cSede.nome, ist.denominazione;
QUERY;
        return DB::select($query, [$idAnno]);
    }

}
