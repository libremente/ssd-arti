<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Repositories;


use App\Models\DispersioneScolastica;
use Illuminate\Support\Facades\DB;

class DispersioneScolasticaRepository
{

    public function list(int $idAnno)
    {
        return DispersioneScolastica::query()->where('idAnnoScolastico', "=", $idAnno)->get();
    }

    public function indicatori(int $idAnno)
    {
        $query = <<<QUERY
select nome                                   as "comune",
       c."ambitoTerritoriale",
       null                                   as "tassoDispersione",
       abbandoni * 1.0 / iscritti             as "tassoAbbandoni",
       evasioni * 1.0 / iscritti              as "tassoEvasioni",
       ripetenze * 1.0 / iscritti             as "tassoRipetenza",
       "frequenzeIrregolari" * 1.0 / iscritti as "tassoFrequenzaIrregolare"
from "dispersioneScolastica"
         join comuni c on "dispersioneScolastica"."codiceCatastaleComune" = c."codiceCatastale"
where "idAnnoScolastico" = ?;
QUERY;
        return DB::select($query, [$idAnno]);
    }
}
