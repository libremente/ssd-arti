<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Repositories;


use App\Models\IstituzioneScolastica;
use App\Models\SedeCiclo1;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;


class SediCiclo1Repository
{


    private function getSedeBuilder(int $idAnnoScolastico): Builder
    {

        return SedeCiclo1::query()
            ->whereHas(
                "istituzioneScolastica",
                function ($query) use ($idAnnoScolastico) {
                    $query->where('idAnnoScolastico', '=', $idAnnoScolastico);
                }
            )
            ->with([
                "istituzioneScolastica" => function ($query) use ($idAnnoScolastico) {
                    $query->where('idAnnoScolastico', '=', $idAnnoScolastico);
                },
                "alunni",
                "tipologiaScuola",
                "istituzioneScolastica.alunni"
            ]);
    }

    public function sediData($sedi)
    {
        if ($sedi == null) {
            return null;
        }
        if (($sedi instanceof Collection)) {

            foreach ($sedi as $sede) {
                self::assicuraCampi($sede);
            }
            return $sedi;
        } else {
            $sedi_array = [];
            self::assicuraCampi($sedi);
            array_push($sedi_array, $sedi);
            return $sedi_array;
        }

    }

    private static function assicuraCampi(SedeCiclo1 $sede)
    {
        $sede->codice;
        $sede->coordinate;
        $sede->codiceCatastaleComune;
        $sede->comune;
        $sede->indirizzo;
        $sede->tipo;
        $sede->sedeDirezioneAmministrativa;
        $sede->tipologiaScuola;
        $sede->codiceEdificio;
        $sede->istituzioneScolastica;
        $sede->istituzioneScolastica->alunni;
        $sede->tipologiaScuola;
        $sede->alunni;
    }

    public function  list(bool $showOnlySedeAmministrative, int $idAnnoScolastico)
    {

        $builder = $this->getSedeBuilder($idAnnoScolastico);
        $builder->where('coordinate', '<>', null);

        if ($showOnlySedeAmministrative) {
            $builder->where('sedeDirezioneAmministrativa', "=", 'true');
        }

        return $builder->get();

    }

    public function show($codice, $idAnnoScolastico): ?SedeCiclo1
    {
        $builder = $this->getSedeBuilder($idAnnoScolastico);
        $builder->where('codice', '=', $codice);
        return $builder->first();

    }

    public function getSediByIstituzione($codiceIstituzione, $idAnno)
    {
        $is = IstituzioneScolastica::trovaCodiceAnno($codiceIstituzione, $idAnno);
        $builder = $this->getSedeBuilder($idAnno);
        $builder->without(["istituzioneScolastica"]);
        $builder->where('idIstituzioneScolastica', '=', $is->id);
        return $builder->get();

    }

    public function find(int $idSede): ?SedeCiclo1
    {
        return SedeCiclo1::query()
            ->with([
                "istituzioneScolastica",
                "alunni",
                "tipologiaScuola",
                "istituzioneScolastica.alunni"
            ])->find($idSede);
    }

}
