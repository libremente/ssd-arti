<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace App\Repositories;


use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class TrendRepository
{

    public function trend()
    {
        return [
            'popolazione' =>
                [
                    "2016/2017" => null,
                    "2017/2018" => null,
                    "2018/2019" => null
                ],
            'popolazioneEtaScolare' =>
                [
                    "2016/2017" => null,
                    "2017/2018" => null,
                    "2018/2019" => null
                ]


        ];
    }

    public function popolazioneScolasticaAnniPrecedenti(int $idAnnoPartenza)
    {

        $datiAnniPrecedenti = $this->anniPrecedenti($idAnnoPartenza, 3);

        if (!$datiAnniPrecedenti) {
            abort(404, "Anno scolastico non trovato");
        }

        $headerTemplate = "";
        $idAnni = "";
        $lastIndex = count($datiAnniPrecedenti) - 1;
        foreach ($datiAnniPrecedenti as $key => $anno) {
            $headerTemplate .= $this->makeHeader($anno->id, $anno->label);
            $idAnni .= $anno->id;
            if ($key != $lastIndex) {
                $headerTemplate .= ", ";
                $idAnni .= ", ";
            }
        }


        $querySede1 = <<<SQL
select cSede.provincia    as "Provincia PE",
       cSede.nome         as "Comune PE",
       ist.codice         as "Codice IS",
       ist.denominazione  as "Denominazione IS",
       sede.codice        as "Codice PE",
       sede.denominazione as "Denominazione PE",
       pivot.*
from "sediCiclo1" as sede
    JOIN (
            SELECT
                   codice as "Codice PE",
                   $headerTemplate
            from (
                    select sede.codice               as codice,
                               "idAnnoScolastico",
                               "alunniIs"."totaleAlunni" as "TotaleAlunniIs",
                               alunniSede."totaleAlunni" as "TotaleAlunniPE"
                        from "istituzioniScolastiche" as ist
                                 join "alunniIs" on ist.id = "alunniIs"."idIs"
                                 join "sediCiclo1" as sede on ist.id = sede."idIstituzioneScolastica"
                                 left join "alunniSedeCiclo1" as alunniSede on sede.id = alunniSede."idSedeCiclo1"
                        where "idAnnoScolastico" in ( $idAnni )
                 ) as popolazione
            group by codice
        ) as pivot on sede.codice = pivot."Codice PE"
    join comuni cSede on sede."codiceCatastaleComune" = cSede."codiceCatastale"
    join "istituzioniScolastiche" ist on ist.id = sede."idIstituzioneScolastica"
where "idAnnoScolastico" = ?

SQL;


        $querySede2 = <<<SQL
select cSede.provincia    as "Provincia PE",
       cSede.nome         as "Comune PE",
       ist.codice         as "Codice IS",
       ist.denominazione  as "Denominazione IS",
       sede.codice        as "Codice PE",
       sede.denominazione as "Denominazione PE",
       pivot.*
from "sediCiclo2" as sede
    JOIN (
            SELECT
                   codice as "Codice PE",
                   $headerTemplate
            from (
                    select sede.codice               as codice,
                           "idAnnoScolastico",
                           "alunniIs"."totaleAlunni" as "TotaleAlunniIs",
                           alunniSede."totaleAlunni" as "TotaleAlunniPE"
                    from "istituzioniScolastiche" as ist
                             join "alunniIs" on ist.id = "alunniIs"."idIs"
                             join "sediCiclo2" as sede on ist.id = sede."idIstituzioneScolastica"
                             left join "alunniSedeCiclo2" as alunniSede on sede.id = alunniSede."idSedeCiclo2"
                    where "idAnnoScolastico" in ( $idAnni )
                 ) as popolazione
            group by codice
        ) as pivot on sede.codice = pivot."Codice PE"
    join comuni cSede on sede."codiceCatastaleComune" = cSede."codiceCatastale"
    join "istituzioniScolastiche" ist on ist.id = sede."idIstituzioneScolastica"
where "idAnnoScolastico" = ?

SQL;

        $query = <<<SQL
$querySede1
UNION ALL
$querySede2
order by "Provincia PE", "Codice IS"
SQL;

//        print($query);
//        die();
        $dataOut = DB::select($query, [$idAnnoPartenza, $idAnnoPartenza]);
        $header = isset($dataOut[0]) ? array_keys((array)$dataOut[0]) : '';
        return [
            'header' => $header,
            'data' => $dataOut
        ];

    }

    function makeHeader($id, $label)
    {
        $ret = <<<SQL
        MAX("TotaleAlunniIs") FILTER (WHERE "idAnnoScolastico" = $id) AS "tot. alunni Is $label",
        sum("TotaleAlunniPE") FILTER (WHERE "idAnnoScolastico" = $id) AS "tot. alunni PE $label"
SQL;
        return $ret;
    }

    private function anniPrecedenti(int $idAnnoPartenza, int $nAnni)
    {
        $queryNanniPrecedenti = <<<SQL
        select id, label
        from "anniScolastici"
        where inizio <= (select inizio from "anniScolastici" where id = ?)
        order by inizio desc
        limit ?

SQL;
        return DB::select($queryNanniPrecedenti, [$idAnnoPartenza, $nAnni]);
    }


    private function anniProiezioni(int $idAnnoPartenza, int $nAnni)
    {
        $queryNanniProiezioni = <<<SQL
        select id, label
        from "anniScolasticiProiezioni"
        where inizio > (select inizio from "anniScolastici" where id = ?)
        order by inizio desc
        limit ?

SQL;
        return DB::select($queryNanniProiezioni, [$idAnnoPartenza, $nAnni]);
    }

    /**
     * Elabora un trend per l'istituzione scolastica data in input, considerando come riferimento l'anno scolastico dato in input
     * @param int $idAnno
     * @param string $codiceIs
     * @return array con campi precedenti e proiezione
     */
    public function trendIs(int $idAnno, string $codiceIs)
    {
        $datiAnniPrecedenti = $this->anniPrecedenti($idAnno, 3);

        if (!$datiAnniPrecedenti) {
            abort(404, "Anno scolastico non trovato");
        }

        return [
            'precedenti' => $this->storicoIs($idAnno, $codiceIs),
            'proiezione' => $this->proiezioneIs($idAnno, $codiceIs)
        ];

    }


    private function proiezioneIs(int $idAnnoScolastico, $codiceIs)
    {
        $anniProiezioni = $this->anniProiezioni($idAnnoScolastico, 3);

        $in = implode(',', Arr::pluck($anniProiezioni, 'id'));

        $query = <<<SQL
select "label",
       alunni
from "alunniIsProiezioni"
    join "anniScolasticiProiezioni" on "alunniIsProiezioni"."idAnnoProiezione" = "anniScolasticiProiezioni".id
where
      "codiceIs" = ? AND "alunniIsProiezioni"."idAnnoProiezione" in ($in)
;
SQL;


        return DB::select($query, [$codiceIs]);

    }

    private function storicoIs(int $idAnnoScolastico, $codiceIs)
    {
        $anniProiezioni = $this->anniPrecedenti($idAnnoScolastico, 3);

        $in = implode(',', Arr::pluck($anniProiezioni, 'id'));

        $query = <<<SQL
select label,
       "totaleAlunni" as alunni
from "alunniIs"
         join "istituzioniScolastiche" i on "alunniIs"."idIs" = i.id
         join "anniScolastici" on i."idAnnoScolastico" = "anniScolastici".id
where codice = ? AND "idAnnoScolastico" in ($in)
;
SQL;


        return DB::select($query, [$codiceIs]);

    }

    public function trendPe1(int $idAnno, string $codiceIs)
    {
        $datiAnniPrecedenti = $this->anniPrecedenti($idAnno, 3);

        if (!$datiAnniPrecedenti) {
            abort(404, "Anno scolastico non trovato");
        }

        return [
            'precedenti' => $this->storicoPe1($idAnno, $codiceIs),
            'proiezione' => $this->proiezionePe1($idAnno, $codiceIs)
        ];

    }

    private function proiezionePe1(int $idAnnoScolastico, $codiceIs)
    {
        $anniProiezioni = $this->anniProiezioni($idAnnoScolastico, 3);

        $in = implode(',', Arr::pluck($anniProiezioni, 'id'));

        $query = <<<SQL
select "label",
       alunni
from "alunniSedeCiclo1Proiezioni"
    join "anniScolasticiProiezioni" on "alunniSedeCiclo1Proiezioni"."idAnnoProiezione" = "anniScolasticiProiezioni".id
where
      "codicePe" = ? AND "alunniSedeCiclo1Proiezioni"."idAnnoProiezione" in ($in)
;
SQL;


        return DB::select($query, [$codiceIs]);

    }

    private function storicoPe1(int $idAnnoScolastico, $codiceIs)
    {
        $anniProiezioni = $this->anniPrecedenti($idAnnoScolastico, 3);

        $in = implode(',', Arr::pluck($anniProiezioni, 'id'));

        $query = <<<SQL
select label,
       "totaleAlunni" as alunni
from "alunniSedeCiclo1"
         join "sediCiclo1" sede on "alunniSedeCiclo1"."idSedeCiclo1" = sede.id
         join "istituzioniScolastiche" ist on sede."idIstituzioneScolastica" = ist.id
         join "anniScolastici" on ist."idAnnoScolastico" = "anniScolastici".id
where sede.codice = ? AND "idAnnoScolastico" in ($in)
;
SQL;


        return DB::select($query, [$codiceIs]);

    }

    public function trendPe2(int $idAnno, string $codiceIs)
    {
        $datiAnniPrecedenti = $this->anniPrecedenti($idAnno, 3);

        if (!$datiAnniPrecedenti) {
            abort(404, "Anno scolastico non trovato");
        }

        return [
            'precedenti' => $this->storicoPe2($idAnno, $codiceIs),
            'proiezione' => $this->proiezionePe2($idAnno, $codiceIs)
        ];

    }

    private function proiezionePe2(int $idAnnoScolastico, $codiceIs)
    {
        $anniProiezioni = $this->anniProiezioni($idAnnoScolastico, 3);

        $in = implode(',', Arr::pluck($anniProiezioni, 'id'));

        $query = <<<SQL
select "label",
       alunni
from "alunniSedeCiclo2Proiezioni"
    join "anniScolasticiProiezioni" on "alunniSedeCiclo2Proiezioni"."idAnnoProiezione" = "anniScolasticiProiezioni".id
where
      "codicePe" = ? AND "alunniSedeCiclo2Proiezioni"."idAnnoProiezione" in ($in)
;
SQL;


        return DB::select($query, [$codiceIs]);

    }

    private function storicoPe2(int $idAnnoScolastico, $codiceIs)
    {
        $anniProiezioni = $this->anniPrecedenti($idAnnoScolastico, 3);

        $in = implode(',', Arr::pluck($anniProiezioni, 'id'));

        $query = <<<SQL
select label,
       sum("totaleAlunni") as alunni
from "alunniSedeCiclo2"
         join "sediCiclo2" sede on "alunniSedeCiclo2"."idSedeCiclo2" = sede.id
         join "istituzioniScolastiche" ist on sede."idIstituzioneScolastica" = ist.id
         join "anniScolastici" on ist."idAnnoScolastico" = "anniScolastici".id
where sede.codice = ? AND "idAnnoScolastico" in ($in) group by "idSedeCiclo2", label
;
SQL;


        return DB::select($query, [$codiceIs]);

    }

    public function demografieComunali()
    {
        $query = <<<SQL
select c.provincia,
       c.nome as comune,
       "popTot2019",
       "pop_3_13_2015",
       "pop_3_13_2019",
       "var_3_13_2019_2015",
       "prev_3_13_2023",
       "var_3_13_2023_2019",
       "pop_3_5_2015",
       "pop_3_5_2019",
       "var_3_5_2019_2015",
       "prev_3_5_2023",
       "var_3_5_2023_2019",
       "pop_6_10_2015",
       "pop_6_10_2019",
       "var_6_10_2019_2015",
       "prev_6_10_2023",
       "var_6_10_2023_2019",
       "pop_11_13_2015",
       "pop_11_13_2019",
       "var_11_13_2019_2015",
       "prev_11_13_2023",
       "var_11_13_2023_2019",
       "pop_14_18_2015",
       "pop_14_18_2019",
       "var_14_18_2019_2015",
       "prev_14_18_2023",
       "var_14_18_2023_2019"
from demografie
         join comuni c on demografie."codiceCatastaleComune" = c."codiceCatastale"
ORDER BY provincia, comune
;
SQL;

        return DB::select($query);
    }


    public function demografiaComune($codiceCatastale)
    {
        $query = <<<SQL
select c.provincia,
       c.nome,
       demografie.*
from demografie
         join comuni c on demografie."codiceCatastaleComune" = c."codiceCatastale"
WHERE
   "codiceCatastaleComune" = ?
SQL;

        return DB::select($query, [$codiceCatastale]);
    }
}
