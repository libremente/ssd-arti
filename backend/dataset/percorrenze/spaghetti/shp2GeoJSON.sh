#!/bin/bash

for f in *.shp; do
    echo "$f"
    secondString=".json"
    pattern="Pol_spaghetti.shp"
    out=${f//$pattern/$secondString}
    echo out

    ogr2ogr -simplify 100 -f "GeoJSON" -lco COORDINATE_PRECISION=4 -t_srs EPSG:4326 "geojson/$out" "$f"
done
