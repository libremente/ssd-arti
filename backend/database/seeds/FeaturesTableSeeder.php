<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

use Flynsarmy\CsvSeeder\CsvSeeder;

class FeaturesTableSeeder extends CsvSeeder
{
    /**
	 * Initializes the CSV Seed file
	 */
	public function __construct()
    {
        $this->table = 'features';
        $this->csv_delimiter = ';';
		$this->filename = base_path() . '/database/seeds/csv_seeds/features_seed.csv';
		$this->mapping = [
			0 => 'name',
			1 => 'type',
			2 => 'shape',
			3 => 'description'
		];
	}

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::disableQueryLog();
        parent::run();
    }
}
