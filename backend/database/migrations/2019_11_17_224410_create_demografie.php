<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDemografie extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demografie', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codiceCatastaleComune', 4);
            $table->integer('popTot2019')->nullable();
            //3-13
            $table->integer('pop_3_13_2015')->nullable();
            $table->integer('pop_3_13_2019')->nullable();
            $table->float('var_3_13_2019_2015')->nullable();
            $table->integer('prev_3_13_2023')->nullable();
            $table->float('var_3_13_2023_2019')->nullable();

            //3-5
            $table->integer('pop_3_5_2015')->nullable();
            $table->integer('pop_3_5_2019')->nullable();
            $table->float('var_3_5_2019_2015')->nullable();
            $table->integer('prev_3_5_2023')->nullable();
            $table->float('var_3_5_2023_2019')->nullable();

            //6-10
            $table->integer('pop_6_10_2015')->nullable();
            $table->integer('pop_6_10_2019')->nullable();
            $table->float('var_6_10_2019_2015')->nullable();
            $table->integer('prev_6_10_2023')->nullable();
            $table->float('var_6_10_2023_2019')->nullable();

            //11-13
            $table->integer('pop_11_13_2015')->nullable();
            $table->integer('pop_11_13_2019')->nullable();
            $table->float('var_11_13_2019_2015')->nullable();
            $table->integer('prev_11_13_2023')->nullable();
            $table->float('var_11_13_2023_2019')->nullable();

            //14-18
            $table->integer('pop_14_18_2015')->nullable();
            $table->integer('pop_14_18_2019')->nullable();
            $table->double('var_14_18_2019_2015')->nullable();
            $table->integer('prev_14_18_2023')->nullable();
            $table->float('var_14_18_2023_2019')->nullable();


            $table->foreign('codiceCatastaleComune', 'fk_demografie_comune')
                ->references('codiceCatastale')->on('comuni')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('demografie');
    }
}
