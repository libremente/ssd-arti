<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComuniTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comuni', function (Blueprint $table) {
            $table->string('codiceCatastale', 4)->primary('comuni_pkey');
            $table->string('nome', 100);
            $table->string('provincia', 100);
            $table->string('regione', 45);
            $table->string('cap', 45)->nullable();
            $table->string('codiceIstat', 10)->index('ssd_comuni_codiceistat_index');
            $table->string('ambitoTerritoriale', 5)->nullable();
            $table->geometry('confine', 27700)->nullable();
            $table->foreign('ambitoTerritoriale')->
            references('codice')->on('ambitiTerritoriali')
                ->onUpdate('cascade')->onDelete('set null');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comuni');
    }

}
