<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeCpia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('CPIA', function (Blueprint $table) {
            $table->string('provincia', 50)->default("");
        });
        Schema::table('CPIAPuntiErogazioneLivello1', function (Blueprint $table) {
            $table->integer("idTipoSede")->nullable();
            $table->foreign('idTipoSede', 'fk_tipoSede')
                ->references('id')->on('CPIATipiSede')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });


        Schema::table('CPIAPuntiErogazioneLivello2', function (Blueprint $table) {
            $table->string('denominazioneIS', 100)->nullable();
            $table->string('email', 250)->nullable();
            $table->string('pec', 250)->nullable();
            $table->string('telefono', 45)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('CPIA', function (Blueprint $table) {
            $table->dropColumn('provincia');
        });
        Schema::table('CPIAPuntiErogazioneLivello1', function (Blueprint $table) {
            $table->dropColumn("idTipoSede");
        });
        Schema::table('CPIAPuntiErogazioneLivello2', function (Blueprint $table) {
            $table->dropColumn('denominazioneIS');
            $table->dropColumn('email');
            $table->dropColumn('pec');
            $table->dropColumn('telefono');

        });
    }
}
