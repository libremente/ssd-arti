<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlunniSedeCiclo1Table extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alunniSedeCiclo1', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('idSedeCiclo1', false, true);
            $table->integer('totaleAlunni')->nullable()->default(0);
            $table->integer('totaleAlunniDisabili')->nullable()->default(0);
            $table->integer('totaleClassi')->nullable()->default(0);
            $table->text('dettagli')->nullable();

            $table->foreign('idSedeCiclo1', 'fk_alunniSedeCiclo1_idSedeCiclo1')
                ->references('id')->on('sediCiclo1')
                ->onUpdate('CASCADE'
                )->onDelete('CASCADE');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('alunniSedeCiclo1');
    }

}
