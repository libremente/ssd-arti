<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCPIATable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CPIA', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codice', 10);
            $table->bigInteger('idAnnoScolastico', false, true);
            $table->string('denominazione', 50);
            $table->string('comune', 100);
            $table->string('codiceCatastaleComune', 4)->nullable();
            $table->string('indirizzo', 250)->nullable();
            $table->string('email', 250)->nullable();
            $table->string('pec', 250)->nullable();
            $table->string('telefono', 45)->nullable();
            $table->text('attoIstitutivo')->nullable();
            $table->geometry('coordinate')->nullable();
            $table->unique(['idAnnoScolastico', 'codice'], 'idx_CPIA_codice_idAnnoScolastico_unique');
            $table->foreign('idAnnoScolastico', 'fk_CPIA_idAnnoScolastico')
                ->references('id')->on('anniScolastici')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->foreign('codiceCatastaleComune', 'fk_CPIA_codiceCatastaleComune')
                ->references('codiceCatastale')->on('comuni')
                ->onUpdate('CASCADE')
                ->onDelete('SET NULL');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('CPIA');
    }

}
