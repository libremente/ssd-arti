<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace Tests\Feature;

use Tests\TestCase;


class IsControllerTest extends TestCase
{
    static $idAnnoScolastico;
    /*
    Route::prefix('/is')->group(function () {
    Route::get('/list', 'ISController@list');

    Route::get('/ciclo1', 'ISController@ciclo1');
    Route::get('/ciclo2', 'ISController@ciclo2');
    Route::get('/sovradimensionate', 'ISController@sovradimensionate');
    Route::get('/sottodimensionate', 'ISController@sottodimensionate');
    Route::get('/{codiceIS}', 'ISController@get');
    });
    */


    /**
     * @beforeClass
     */
    public function testAnnoScolasticoCorrente()
    {
        $response = $this->get('api/annoScolastico/corrente');
        $response->assertStatus(200);
        $json = $response->json();
        $response->assertJsonStructure(['id', 'label', 'inizio'], $json);
        self::$idAnnoScolastico = $json['id'];
    }


    public function testListNessunAnnoScolastico()
    {
        $response = $this->get('api/is/list');

        $response->assertStatus(400);

    }

    public function testList()
    {
        $uri = 'api/is/list?idAnno=' . self::$idAnnoScolastico;
        $response = $this->get($uri);

        $response->assertStatus(200);

        $json = $response->json();


        $struttura = [[
            'id',
            'idAnnoScolastico',
            'alunni' => ['id', 'idIs', 'totaleAlunni', 'totaleAlunniDisabili', 'totaleClassi'],
            'codice',
            'codiceAmbito',
            'codiceCatastaleComuneDirigenza',
            'comuneSedeDirigenza',
            'denominazione',
            'tipoIstituto']];


        $response->assertJsonStructure($struttura, $json);
    }

    public function testCiclo1()
    {
        $uri = 'api/is/ciclo1?idAnno=' . self::$idAnnoScolastico;
        $response = $this->get($uri);

        $response->assertStatus(200);

        $json = $response->json();


        $struttura = [[
            'id',
            'idAnnoScolastico',
            'alunni' => ['id', 'idIs', 'totaleAlunni', 'totaleAlunniDisabili', 'totaleClassi'],
            'codice',
            'codiceAmbito',
            'codiceCatastaleComuneDirigenza',
            'comuneSedeDirigenza',
            'denominazione',
            'tipoIstituto',
            'sediCiclo1'
        ]];


        $response->assertJsonStructure($struttura, $json);
    }

    public function testCiclo1tNessunAnnoScolastico()
    {
        $response = $this->get('api/is/ciclo1');

        $response->assertStatus(400);

    }

    public function testCiclo2()
    {
        $uri = 'api/is/ciclo2?idAnno=' . self::$idAnnoScolastico;
        $response = $this->get($uri);

        $response->assertStatus(200);

        $json = $response->json();


        $struttura = [[
            'id',
            'idAnnoScolastico',
            'alunni' => ['id', 'idIs', 'totaleAlunni', 'totaleAlunniDisabili', 'totaleClassi'],
            'codice',
            'codiceAmbito',
            'codiceCatastaleComuneDirigenza',
            'comuneSedeDirigenza',
            'denominazione',
            'tipoIstituto',
            'sediCiclo2'
        ]];


        $response->assertJsonStructure($struttura, $json);
    }

    public function testCiclo2NessunAnnoScolastico()
    {
        $response = $this->get('api/is/ciclo2');

        $response->assertStatus(400);

    }


    public function testSovradimensionate()
    {
        $uri = 'api/is/sovradimensionate?idAnno=' . self::$idAnnoScolastico;
        $response = $this->get($uri);

        $response->assertStatus(200);

        $json = $response->json();


        $struttura = [[
            "id", "codice", "totaleAlunni", "codiceCatastaleComuneDirigenza", "comuneSedeDirigenza"
        ]];


        $response->assertJsonStructure($struttura, $json);
    }

    public function testSovradimensionateNessunAnnoScolastico()
    {
        $response = $this->get('api/is/sovradimensionate');

        $response->assertStatus(400);

    }

    public function testSottodimensionate()
    {
        $uri = 'api/is/sottodimensionate?idAnno=' . self::$idAnnoScolastico;
        $response = $this->get($uri);

        $response->assertStatus(200);

        $json = $response->json();


        $struttura = [[
            "id", "codice", "totaleAlunni", "codiceCatastaleComuneDirigenza", "comuneSedeDirigenza"
        ]];


        $response->assertJsonStructure($struttura, $json);
    }

    public function testSottodimensionateNessunAnnoScolastico()
    {
        $response = $this->get('api/is/sottodimensionate');

        $response->assertStatus(400);

    }

    public function testCercaCodiceIs()
    {


        $uri = 'api/is/cerca/FGIC864003?idAnno=' . self::$idAnnoScolastico;

        $response = $this->get($uri);

        $response->assertStatus(200);

        $json = $response->json();

        $struttura = [[
            'id',
            'idAnnoScolastico',
            'alunni' => ['id', 'idIs', 'totaleAlunni', 'totaleAlunniDisabili', 'totaleClassi'],
            'codice',
            'codiceAmbito',
            'codiceCatastaleComuneDirigenza',
            'comuneSedeDirigenza',
            'denominazione',
            'tipoIstituto',
            'sediCiclo1',
            'sediCiclo2'
        ]];


        $response->assertJsonStructure($struttura, $json);
    }


}
