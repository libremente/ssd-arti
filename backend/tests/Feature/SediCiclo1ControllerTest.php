<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace Tests\Feature;

use Tests\TestCase;

class sediciclo1ControllerTest extends TestCase
{
    /*
Route::prefix('sediciclo1')->group(function () {
    Route::get('/', 'sediciclo1Controller@index');
    Route::get('/indirizzi', 'sediciclo1Controller@indirizzi');
    Route::get('/istituzione/{codice}', 'sediciclo1Controller@getSediByIstituzione');
    Route::get('/{codice}', 'sediciclo1Controller@show');

});

});
     */

    /** @var int */
    protected static $idAnnoScolastico;

    static $struttura = [[
        'id',
        'idIstituzioneScolastica',
        'alunni' => [
            'dettagli',
            'totaleAlunni',
            'totaleAlunniDisabili',
        ],
        'alunni',
        'codice',
        'codiceCatastaleComune',
        'codiceEdificio',
        'comune',
        'coordinate',
        'denominazione',
        'indirizzo',
        'istituzioneScolastica',
        'sedeDirezioneAmministrativa',
        'tipo',
        'tipologiaScuola',
        'verticalizzazione'

    ]];

    /**
     * @beforeClass
     */
    public function testAnnoScolasticoCorrente()
    {
        $response = $this->get('api/annoScolastico/corrente');
        $response->assertStatus(200);
        $json = $response->json();
        $response->assertJsonStructure(['id', 'label', 'inizio'], $json);
        static::$idAnnoScolastico = $json['id'];
    }


    public function testListNessunAnnoScolastico()
    {
        $response = $this->get('api/sediciclo1/');

        $response->assertStatus(400);

    }


    public function testList()
    {

        $uri = 'api/sediciclo1/?idAnno=' . self::$idAnnoScolastico;
        $response = $this->get($uri);

        $response->assertStatus(200);

        $json = $response->json();


        $response->assertJsonStructure(self::$struttura, $json);
    }


    public function testListFiltroIndirizzoDiStudio()
    {
        //
        $uri = 'api/sediciclo1/?filtra=indirizzoDi&indirizzo=LI00&idAnno=' . self::$idAnnoScolastico;
        $response = $this->get($uri);
        $response->assertStatus(400);

        $uri = 'api/sediciclo1/?filtra=indirizzoDi&indirizzo=&idAnno=' . self::$idAnnoScolastico;
        $response = $this->get($uri);
        $response->assertStatus(400);


        $uri = 'api/sediciclo1/?filtra=sediAmministrative&idAnno=' . self::$idAnnoScolastico;
        $response = $this->get($uri);
        $response->assertStatus(200);

        $json = $response->json();


        $response->assertJsonStructure(self::$struttura, $json);

    }


    public function testCercaCodice()
    {
        $response = $this->get('api/sediciclo1/cerca/fdfsds');
        $response->assertStatus(400);
        $response = $this->get('api/sediciclo1/cerca/fdfsds?idAnno=' . self::$idAnnoScolastico);
        $response->assertStatus(404);
        $response = $this->get('api/sediciclo1/cerca/FGEE872014?idAnno=' . self::$idAnnoScolastico);
        $response->assertStatus(200);

    }


}
