<?php
/**
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

namespace Tests\Feature;

use Tests\TestCase;

class AnnoScolasticoControllerTest extends TestCase
{

    private static $idAnnoScolastico;


    /**
     * @beforeClass
     */
    public function testAnnoScolasticoCorrente()
    {
        $response = $this->get('api/annoScolastico/corrente');
        $response->assertStatus(200);
        $json = $response->json();
        $response->assertJsonStructure(['id', 'label', 'inizio'], $json);
        static::$idAnnoScolastico = $json['id'];
    }

    public function testAnnoScolasticoList()
    {
        $response = $this->get('api/annoScolastico/list');
        $response->assertStatus(200);
        $json = $response->json();
        $response->assertJsonStructure([['id', 'label', 'inizio']], $json);
    }

    public function testAnnoScolasticoCorrenteDettagli()
    {
        $response = $this->get('api/annoScolastico/' . static::$idAnnoScolastico);
        $response->assertStatus(200);
        $json = $response->json();
        $response->assertJsonStructure(['id', 'label', 'inizio'], $json);

    }


}
