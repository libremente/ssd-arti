#!/usr/bin/env bash
git checkout production &&
  git pull origin production &&
  git merge dev -m "dev merged" &&
  git push origin production &&
  git checkout dev &&
echo "DONE"
