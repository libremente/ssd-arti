#!/usr/bin/env bash
branchName=$(git name-rev --name-only HEAD)
echo "${branchName}"
if [[ ${branchName} != "dev" ]]; then
  (echo >&2 "Must be on dev branch")
  exit 1
fi

ng build --configuration=dev &&
  git add dist/ &&
  git commit -m "update build at $(date)" &&
  git push origin dev &&
  echo "DONE"
