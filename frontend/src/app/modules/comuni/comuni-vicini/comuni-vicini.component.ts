/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, OnInit, ViewChild} from '@angular/core';
import {ComuniService} from '../comuni.service';
import {Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import {Comune} from '../../../shared/models/Comune';
import {NgbTypeaheadConfig} from '@ng-bootstrap/ng-bootstrap';
import {ComuneVicino} from '../ComuniVicini';

@Component({
    selector: 'app-comuni-vicini',
    templateUrl: './comuni-vicini.component.html',
    styleUrls: ['./comuni-vicini.component.css'],
    providers: [NgbTypeaheadConfig] // add NgbTypeaheadConfig to the component providers

})

export class ComuniViciniComponent implements OnInit {


    private comuni: Array<Comune> = [];
    @ViewChild('instance') instance;
    focus$ = new Subject<string>();
    click$ = new Subject<string>();
    comune: Comune;
    comuniVicini: Array<ComuneVicino> = Array();
    minutiMax = 60;
    showRisultati = false;


    constructor(private comuniService: ComuniService,
                private config: NgbTypeaheadConfig) {
    }

    ngOnInit() {
        this.comuniService.list().subscribe((comuni) => {
                this.comuni = comuni;
            }
        );

    }


    search = (text$: Observable<string>) => {
        const debouncedText$ = text$.pipe(
            debounceTime(200),
            distinctUntilChanged()
        );


        return debouncedText$.pipe(
            map(term => (
                term === '' ? [] :
                    this.comuni.map(comune => {
                        return {
                            punteggio: comune.nome.toLowerCase().indexOf(term.toLowerCase()),
                            comune: comune
                        };
                    })
                        .sort((a, b) => a.punteggio - b.punteggio)
                        .filter(o => o.punteggio > -1)
                        .map(v => v.comune)
                        .slice(0, 10))
            )
        );
        // tslint:disable-next-line:semicolon
    };

    formatter = (x: Comune) => x.nome;


    selectedItem(comune: Comune) {
        this.comuniService
            .comuniVicini(comune.codiceCatastale, this.minutiMax)
            .subscribe(
                (list) => {
                    this.showRisultati = true;
                    this.comuniVicini = list;
                },
                (e) => console.error(e));
    }
}
