/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {LOCALE_ID, NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {PagesComponent} from './pages.component';
import {CpiaComponent} from '../../maps/cpia/cpia.component';
import {AssettoISComponent} from '../../maps/assetto-is/assetto-is.component';
import {AnnoScolasticoModule} from '../anno-scolastico/anno-scolastico.module';
import {IstituzioniAnalisiComponent} from '../../maps/istituzioni-analisi/istituzioni-analisi.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {SedeDataAnalisiComponent} from './sede-data-analisi/sede-data-analisi.component';
import {OrganicoPrimoCicloOdComponent} from './tables/organico-primo-ciclo-od/organico-primo-ciclo-od.component';
import {SharedModule} from '../shared.module';
import {OffertaFormativaCiclo2Component} from '../../maps/offerta-formativa-ciclo2/offerta-formativa-ciclo2.component';
import {MappaPugliaComponent} from '../../maps/mappa-puglia/mappa-puglia.component';
import {AggregateDataComponent} from './tables/demografia/trend-demografici/aggregate-data/aggregate-data.component';
import {TrendDemograficiComponent} from './tables/demografia/trend-demografici/trend-demografici.component';
import {ChartsModule} from 'ng2-charts';
import {NumeroIsAlunniAmbitoOdComponent} from './tables/numero-is-alunni-ambito-od/numero-is-alunni-ambito-od.component';
import {PopolazioneScolasticaComponent} from './tables/popolazione-scolastica/popolazione-scolastica.component';
import {PopolazioneAnniPrecedentiComponent} from './tables/popolazione-anni-precedenti/popolazione-anni-precedenti.component';
import {DispersioneScolasticaComponent} from './tables/dispersione-scolastica/dispersione-scolastica.component';
import {ProvenienzeComponent} from './tables/provenienze/provenienze.component';
import {ModificazioniComponent} from './tables/modificazioni/modificazioni.component';
import {PreElaborateStringTablePipe} from '../../shared/pipe/pre-elaborate-string-table.pipe';
import {FilterTableComponent} from '../../filters/filter-table/filter-table.component';
import {PercentOrNdPipe} from '../../shared/pipe/percentageOrNd.pipe';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {HttpInterceptorCustom} from '../../shared/services/http-interceptor-custom.service';
import {OrganicoDiDirittoComponent} from './tables/organico-di-diritto/organico-di-diritto.component';
import {WipComponent} from './tables/wip/wip.component';
import {ExportToolsService} from '../../shared/utils/export-tools.service';
import {TrendChartLineComponent} from '../../shared/components/charts/trend-chart-line/trend-chart-line.component';
import {MapConfiniComponent} from '../../maps/mappa-puglia/map-confini/map-confini.component';
import {ConfiniService} from '../../shared/services/confini/confini.service';
import {MapTipoIsOrganicoComponent} from '../../maps/mappa-puglia/map-tipo-is-organico/map-tipo-is-organico.component';
import {MapFiltroSediComponent} from '../../maps/mappa-puglia/map-filtro-sedi/map-filtro-sedi.component';
import {MapLegendaComponent} from '../../maps/mappa-puglia/map-legenda/map-legenda.component';
import {MapEtichetteComponent} from '../../maps/mappa-puglia/map-etichette/map-etichette.component';
import {MapOpzioniComponent} from '../../maps/mappa-puglia/map-opzioni/map-opzioni.component';
import {CpiaTableComponent} from './tables/cpia/cpia-table/cpia-table.component';
import {BaciniUtenzaComponent} from './tables/bacini-utenza/bacini-utenza.component';
import {CpiaLivello1TableComponent} from './tables/cpia/cpia-livello1-table/cpia-livello1-table.component';
import {CpiaLivello2TableComponent} from './tables/cpia/cpia-livello2-table/cpia-livello2-table.component';
import {CpiaGeneralDataTableComponent} from './tables/cpia/cpia-general-data-table/cpia-general-data-table.component';
import {DemografiaComunaleComponent} from './tables/demografia/demografia-comunale/demografia-comunale.component';
import {IsOrganiciComponent} from './tables/is-organici/is-organici.component';
import {ChartModule} from 'primeng/chart';
import {DemografiaComuneComponent} from './tables/demografia/demografia-comune/demografia-comune.component';
import {StoreModule} from '@ngrx/store';
import * as fromMapPuglia from '../../maps/mappa-puglia/map-puglia.reducer';
import * as fromOffertaFormativa from '../../maps/offerta-formativa-ciclo2/offerta-formativa.reducer';
import {OrganicoDiDirittoDiFattoComponent} from './tables/organico-di-diritto-di-fatto/organico-di-diritto-di-fatto.component';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import {ButtonModule, ProgressBarModule} from 'primeng/primeng';
import {CreditiComponent} from './crediti-component/crediti.component';


const routes: Routes = [
    {
        path: '',
        data: {
            title: 'Mappe',
            urls: [
                {title: 'Dashboard', url: '/dashboard'},
            ]
        },
        component: PagesComponent,
        children: [
            {path: '', redirectTo: 'assettoIstituzioniScolastiche', pathMatch: 'full'},
            {path: 'mappa', redirectTo: 'assettoIstituzioniScolastiche', pathMatch: 'full'},
            {
                path: 'analisi',
                component: IstituzioniAnalisiComponent,
                data: {
                    title: 'Analisi Istituzione Scolastica'
                }
            },
            {
                path: 'assettoIstituzioniScolastiche', component: AssettoISComponent,
                data: {
                    title: 'Assetto Istituzioni Scolastiche',
                    showSelettoreAnno: true
                }
            },
            {
                path: 'offertaFormativaIICiclo', component: OffertaFormativaCiclo2Component,
                data: {
                    title: 'Offerta Formativa II Ciclo',
                    showSelettoreAnno: true
                },
            },
            {
                path: 'dashboard/organiciIs', component: IsOrganiciComponent,
                data: {
                    title: 'Organico di diritto ',
                    showSelettoreAnno: true
                }
            },
            {
                path: 'dashboard/organicoDiDiritto', component: OrganicoDiDirittoComponent,
                data: {
                    title: 'Organico di diritto ',
                    showSelettoreAnno: true
                }
            },
            {
                path: 'dashboard/organicoDiFatto', component: OrganicoDiDirittoDiFattoComponent,
                data: {
                    title: 'Organico di fatto ',
                    showSelettoreAnno: true
                }
            },
            {
                path: 'dashboard/cpia', component: CpiaTableComponent,
                data: {
                    title: 'CPIA ',
                    showSelettoreAnno: true
                }
            },

            {
                path: 'dashboard/baciniUtenza', component: BaciniUtenzaComponent,
                data: {
                    title: 'Bacini di utenza ',
                    showSelettoreAnno: false
                }
            }, {
                path: 'dashboard/demografiaComunale', component: DemografiaComunaleComponent,
                data: {
                    title: 'Demografia Comunale ',
                    showSelettoreAnno: true
                }
            },
            {
                path: 'dashboard/dispersione', component: DispersioneScolasticaComponent,
                data: {
                    title: 'Dispersione',
                    showSelettoreAnno: true
                }
            },
            {
                path: 'dashboard/popolazione-anni', component: PopolazioneAnniPrecedentiComponent,
                data: {
                    title: 'Popolazione 3 anni precedenti',
                    showSelettoreAnno: true
                }
            },
            {
                path: 'dashboard/popolazione-scolastica', component: PopolazioneScolasticaComponent,
                data: {
                    title: 'Popolazione scolastica',
                    showSelettoreAnno: false
                }
            },
            {
                path: 'dashboard/provenienze', component: ProvenienzeComponent,
                data: {
                    title: 'Provenienze',
                    showSelettoreAnno: true
                }
            },
            {
                path: 'dashboard/modificazioni', component: ModificazioniComponent,
                data: {
                    title: 'Modificazioni',
                    showSelettoreAnno: false
                }
            },
            {
                path: 'dashboard/trend', component: TrendDemograficiComponent,
                data: {
                    title: 'Trend',
                    showSelettoreAnno: true
                }
            },
            {
                path: 'dashboard/storiaDimensionamento', component: WipComponent,
                data: {
                    title: 'Storia dimensionamento',
                    showSelettoreAnno: true
                }
            },

            {
                path: 'dashboard/andamentoIscrizioni', component: WipComponent,
                data: {
                    title: 'Andamento Iscrizioni',
                    showSelettoreAnno: true
                }
            },
            {
                path: 'dashboard/disagioEconomicoSociale', component: WipComponent,
                data: {
                    title: 'Disagio Economico Sociale',
                    showSelettoreAnno: true
                }
            },
            {
                path: 'credits', component: CreditiComponent,
                data: {
                    title: 'Credits',
                    showSelettoreAnno: false
                }
            }
        ]
    },

];


@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
        AnnoScolasticoModule,
        DragDropModule,
        ChartModule,
        ChartsModule,
        StoreModule.forFeature(fromMapPuglia.mapPugliaFeatureKey, fromMapPuglia.mapPugliaReducer),
        StoreModule.forFeature(fromOffertaFormativa.offertaFormativaFeatureKey, fromOffertaFormativa.offFormativaReducer),
        OverlayPanelModule,
        ProgressBarModule,
        ButtonModule,
    ],
    declarations: [
        PagesComponent,
        AssettoISComponent,
        IstituzioniAnalisiComponent,
        CpiaComponent,
        SedeDataAnalisiComponent,
        OrganicoPrimoCicloOdComponent,
        OffertaFormativaCiclo2Component,
        AggregateDataComponent,
        MappaPugliaComponent,
        TrendDemograficiComponent,
        NumeroIsAlunniAmbitoOdComponent,
        PopolazioneScolasticaComponent,
        PopolazioneAnniPrecedentiComponent,
        DispersioneScolasticaComponent,
        ProvenienzeComponent,
        ModificazioniComponent,
        FilterTableComponent,
        PreElaborateStringTablePipe,
        PercentOrNdPipe,
        TrendChartLineComponent,
        OrganicoDiDirittoComponent,
        WipComponent,
        MapConfiniComponent,
        MapTipoIsOrganicoComponent,
        MapFiltroSediComponent,
        MapLegendaComponent,
        MapEtichetteComponent,
        MapOpzioniComponent,
        OrganicoDiDirittoDiFattoComponent,
        CpiaTableComponent,
        BaciniUtenzaComponent,
        CpiaLivello1TableComponent,
        CpiaLivello2TableComponent,
        CpiaGeneralDataTableComponent,
        DemografiaComunaleComponent,
        IsOrganiciComponent,
        DemografiaComuneComponent,
        CreditiComponent

    ],


    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpInterceptorCustom,
            multi: true
        },
        {
            provide: LOCALE_ID,
            useValue: 'it-IT'
        },
        ConfiniService,
        ExportToolsService,
    ]
})
export class PagesModule {
}
