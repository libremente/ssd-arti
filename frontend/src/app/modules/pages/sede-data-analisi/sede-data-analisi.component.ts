/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, OnInit} from '@angular/core';
import {SedeCiclo1Service} from '../../../shared/services/sedeciclo1/sede-ciclo1.service';
import {ActivatedRoute} from '@angular/router';
import {SedeCiclo1} from '../../../shared/models/SedeCiclo1';
import {Table} from '../../../shared/settings/table.settings';
import {SedeCiclo2} from '../../../shared/models/SedeCiclo2';
import {SediCiclo2Service} from '../../../shared/services/sediciclo2/sedi-ciclo2.service';

@Component({
    selector: 'app-sede-data-analisi',
    templateUrl: './sede-data-analisi.component.html',
    styleUrls: ['./sede-data-analisi.component.css'],

})
export class SedeDataAnalisiComponent implements OnInit {


    public idSede: number;
    public rows: Array<any>;
    public columns: Array<any>;
    public sede: any;
    public sedeCiclo1: SedeCiclo1;
    public sedeCiclo2: SedeCiclo2;

    public buttonTableOrReview: string;

    constructor(private sedeCiclo1Service: SedeCiclo1Service, private sedeCiclo2Service: SediCiclo2Service, private route: ActivatedRoute) {
        this.buttonTableOrReview = 'review';
        this.columns = Table.sede;

    }

    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            this.idSede = params['id_feature'];
            this.loadDataSede();

        });

    }

    public loadDataSedeCiclo1() {

        const component = this;
        this.sedeCiclo1Service.get(this.idSede).subscribe(resp => {
            this.sede = resp[0];
            this.rows = [resp];
        }, (err) => {
            console.log(err);
            component.loadDataSedeCiclo2();
        });
    }

    public loadDataSedeCiclo2() {
        console.log('chiamo sedi ciclo2');
        this.sedeCiclo2Service.get(this.idSede).subscribe(resp => {
            this.sede = resp[0];
            this.rows = [resp];
        });
    }

    public loadDataSede() {
        this.loadDataSedeCiclo1();
    }

    public showTableOrReview(value: string) {
        return (this.buttonTableOrReview === value);
    }

    public toogleTableOrReview() {
        (this.buttonTableOrReview === 'review') ? this.buttonTableOrReview = 'table' : this.buttonTableOrReview = 'review';
    }

    public getAlternative() {
        return (this.buttonTableOrReview === 'review') ? 'Tabella' : 'Riepilogo';
    }

    public getLat() {
        return this.sede.coordinate['coordinates'][0];
    }

    public getLon() {
        return this.sede.coordinate['coordinates'][1];
    }


}
