/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, OnInit} from '@angular/core';
import {AmbitoTerritoriale} from '../../../../shared/models/AmbitoTerritoriale';
import {Comune} from '../../../../shared/models/Comune';
import {AnnoScolasticoService} from '../../../anno-scolastico/anno-scolastico.service';
import {AnnoScolastico} from '../../../../shared/models/AnnoScolastico';
import {ComuniService} from '../../../comuni/comuni.service';
import {AmbitiTerritorialiService} from '../../../../shared/services/ambiti/ambiti-territoriali.service';
import {DispersioneService} from '../../../../shared/services/dispersione/dispersione.service';
import * as _ from 'lodash';
import {PercentOrNdPipe} from '../../../../shared/pipe/percentageOrNd.pipe';

@Component({
    selector: 'app-dispersione-scolastica',
    templateUrl: './dispersione-scolastica.component.html',
    styleUrls: ['./dispersione-scolastica.component.css']
})
export class DispersioneScolasticaComponent implements OnInit {


    public ambiti: AmbitoTerritoriale[];
    public comuni: Comune[];
    public comuneSelected = 'Tutti';
    public ambitoSelected = 'Tutti';
    public annoScolastico: AnnoScolastico;

    public columns;
    public rows;
    public savedAllRows;

    constructor(private annoScolasticoService: AnnoScolasticoService,
                private comuniService: ComuniService, private ambitiService: AmbitiTerritorialiService,
                private dispersioneService: DispersioneService) {
        this.rows = [];
        this.columns = [


            {prop: 'comune', name: 'Comune'},
            {prop: 'ambitoTerritoriale', name: 'Ambito'},
            {prop: 'tassoDispersione', name: 'Tasso di dispersione', pipe: new PercentOrNdPipe('it-IT')},
            {prop: 'tassoAbbandoni', name: 'Tasso di abbandoni', pipe: new PercentOrNdPipe('it-IT')},
            {prop: 'tassoEvasioni', name: 'Tasso di evasioni', pipe: new PercentOrNdPipe('it-IT')},
            {prop: 'tassoRipetenza', name: 'Tasso di ripetenza', pipe: new PercentOrNdPipe('it-IT')},
            {prop: 'tassoFrequenzaIrregolare', name: 'Tasso di freq. irreg.', pipe: new PercentOrNdPipe('it-IT')}

        ];

    }

    ngOnInit() {
        this.comuniService.list().subscribe(resp => this.comuni = _.sortBy(resp, 'nome'));
        this.annoScolasticoService.getAnnoScolasticoObservable().subscribe(annoScolastico => {

            if (annoScolastico) {
                this.annoScolastico = annoScolastico;

                this.dispersioneService.indicatori(this.annoScolastico).subscribe(resp => {
                    console.log('risposta', resp);
                    this.rows = resp;
                    this.savedAllRows = this.rows;
                });
                this.ambitiService.codiciList(this.annoScolastico).subscribe(resp => this.ambiti = resp.sort());

            }

        });
    }


    public filter() {
        this.rows = this.savedAllRows;
        this.filterByComune();
        this.filterByAmbito();

    }

    public filterByComune() {
        if (this.comuneSelected !== 'Tutti') {
            this.rows = this.rows.filter(item => item.comune === this.comuneSelected);

        }

    }

    public filterByAmbito() {
        if (this.ambitoSelected !== 'Tutti') {
            this.rows = this.rows.filter(item => item.ambitoTerritoriale === this.ambitoSelected);

        }

    }


}
