/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, OnInit} from '@angular/core';
import {AnnoScolasticoService} from '../../../anno-scolastico/anno-scolastico.service';
import {AnnoScolastico} from '../../../../shared/models/AnnoScolastico';
import {Provincia} from '../../../../shared/models/Provincia';
import {ComuniService} from '../../../comuni/comuni.service';
import {Comune} from '../../../../shared/models/Comune';
import * as _ from 'lodash';
import {AmbitiTerritorialiService} from '../../../../shared/services/ambiti/ambiti-territoriali.service';
import {SediCiclo2Service} from '../../../../shared/services/sediciclo2/sedi-ciclo2.service';
import {FormatRowTableService} from '../../../../shared/services/format-row-table.service';

@Component({
    selector: 'app-provenienze',
    templateUrl: './provenienze.component.html',
    styleUrls: ['./provenienze.component.css']
})
export class ProvenienzeComponent implements OnInit {


    private annoScolastico: AnnoScolastico;
    public columns;
    public rows: any[] = [];


    public denIsSelected: 'Tutti';
    public denominazioni: any[];
    public unfilteredDenominazioni: any[];
    public unfilteredRows: any[];

    public comuni: Comune[];
    private unfilteredComuni: Comune[];

    public comuneSelected = 'Tutti';
    public province: Provincia[];
    public provinciaSelected = 'Tutte';
    loading: boolean;


    constructor(private sediCiclo2Service: SediCiclo2Service,
                private annoScolasticoService: AnnoScolasticoService,
                private comuniService: ComuniService,
                private ambitoService: AmbitiTerritorialiService,
                private formatRowService: FormatRowTableService) {
        this.columns = [
            {prop: 'ProvinciaPE', name: 'Provincia'},
            {prop: 'ComunePE', name: 'Comune'},
            {prop: 'DenominazioneIS', name: 'Denominazione IS'},
            {prop: 'CodicePE', name: 'Punto di erogazione'},
            {prop: 'DenominazionePE', name: 'Denominazione punto erogazione'},
            {prop: 'ComuneProvenienza', name: 'Comune di provenienza'},
            {prop: 'numeroAlunni', name: 'Numero di alunni'}
        ];
        this.province = [{id: 1, label: 'Bari'},
            {id: 2, label: 'Foggia'},
            {id: 3, label: 'Lecce'},
            {id: 4, label: 'Taranto'},
            {id: 5, label: 'Brindisi'},
            {id: 6, label: 'Barletta-Andria-Trani'}];
    }

    ngOnInit() {
        this.loading = true;
        this.annoScolasticoService.getAnnoScolasticoObservable().subscribe(anno => {

            this.annoScolastico = anno;
            if (this.annoScolastico) {
                this.sediCiclo2Service.provenienze(this.annoScolastico).subscribe(resp => {
                    console.log('provenienze', resp);


                    this.rows = resp.map(item => {
                        if (item.numeroAlunniInferiore3) {
                            return {...item, numeroAlunni: '<3'};
                        }
                        return {...item};
                    });

                    this.unfilteredRows = this.rows;
                    this.loadFilters();
                    this.loading = false;

                });

            }
        });

    }

    private loadFilters() {
        this.province = (_.uniq(this.unfilteredRows.map(item => item.ProvinciaPE))).sort();
        this.comuni = _.sortBy(_.uniqBy(this.unfilteredRows.map((item) => {
            return {'provincia': item.ProvinciaPE, 'nome': item.ComunePE};
        }), 'nome'), 'nome');
        this.unfilteredComuni = this.comuni;
        this.denominazioni = _.uniqBy(this.unfilteredRows.map(item => {
                return {
                    'provincia': item.ProvinciaPE,
                    'comune': item.ComunePE,
                    'denominazione': item.DenominazioneIS
                };
            }), item => item.denominazione
        ).sort((a, b) => (a.denominazione.localeCompare(b.denominazione)));
        this.unfilteredDenominazioni = this.denominazioni;

    }

    public filter() {
        this.loading = true;
        this.rows = this.unfilteredRows;
        this.filterByProvincia();
        this.filterByComune();
        this.filterByDenominazione();
        this.loading = false;
    }


    public filterByProvincia() {
        if (this.provinciaSelected !== 'Tutte') {
            console.log(this.provinciaSelected);
            this.comuni = this.unfilteredComuni.filter(
                item => item.provincia.toLowerCase() === this.provinciaSelected.toLowerCase()
            );
            this.rows = this.rows.filter(
                item => item.ProvinciaPE.toLowerCase() === this.provinciaSelected.toLowerCase()
            );
        } else {
            this.comuni = this.unfilteredComuni;
        }
    }


    private filterByComune() {
        if (this.comuneSelected !== 'Tutti') {
            this.denominazioni = this.unfilteredDenominazioni.filter(item =>
                item.comune.toLowerCase() === this.comuneSelected.toLowerCase()
            );
            this.rows = this.rows.filter(
                item => item.ComunePE.toLowerCase() === this.comuneSelected.toLowerCase()
            );
        } else {
            this.denominazioni = this.unfilteredDenominazioni;
        }
    }

    private filterByDenominazione() {
        if (this.denIsSelected !== 'Tutti') {
            this.rows = this.rows.filter(
                item => {
                    return item.DenominazioneIS.toLowerCase() === this.denIsSelected.toLowerCase();
                }
            );
        } else {

        }
    }

}
