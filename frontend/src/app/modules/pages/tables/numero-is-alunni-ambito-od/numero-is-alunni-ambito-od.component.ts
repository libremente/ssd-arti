/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, OnInit} from '@angular/core';
import {AnnoScolasticoService} from '../../../anno-scolastico/anno-scolastico.service';
import {AnnoScolastico} from '../../../../shared/models/AnnoScolastico';
import {AmbitiTerritorialiService} from '../../../../shared/services/ambiti/ambiti-territoriali.service';
import {StatisticaAmbito} from '../../../../shared/models/StatisticaAmbito';
import {ComuniService} from '../../../comuni/comuni.service';
import {Comune} from '../../../../shared/models/Comune';
import {FormatRowTableService} from '../../../../shared/services/format-row-table.service';
import {AmbitoTerritoriale} from '../../../../shared/models/AmbitoTerritoriale';
import {NgxSpinnerService} from 'ngx-spinner';
import {Provincia} from '../../../../shared/models/Provincia';
import * as _ from 'lodash';
import {ExportToolsService} from '../../../../shared/utils/export-tools.service';


// TODO: lo spinner a schermo intero non va bene, poiché il componente viene usato inglobato
@Component({
    selector: 'app-numero-is-alunni-ambito-od',
    templateUrl: './numero-is-alunni-ambito-od.component.html',
    styleUrls: ['./numero-is-alunni-ambito-od.component.css']
})
export class NumeroIsAlunniAmbitoOdComponent implements OnInit {


    private statisticheAmbito: StatisticaAmbito[];
    public annoScolastico: AnnoScolastico;
    public savedAllRows: Array<any>;
    public comuni: Comune[];
    private comuniTot: Comune[];
    public province: Provincia[];
    public provinciaSelected = 'Tutti';
    public denominazioneSelected = 'Tutti';
    public denominazioni: string[] = null;
    public ambiti: AmbitoTerritoriale[];
    public ambitoSelected = 'Tutti';
    public comuneSelected = 'Tutti';
    public title = 'Numero di Istituzioni Scolastiche e alunni per ciascun ambito';
    public columns;
    public rows;


    constructor(private annoScolasticoService: AnnoScolasticoService, private ambitiService: AmbitiTerritorialiService,
                private comuniService: ComuniService, private formatRowService: FormatRowTableService,
                private  exportUtilsService: ExportToolsService, private spinner: NgxSpinnerService) {
        this.rows = [];
        this.province = [{id: 1, label: 'Bari'},
            {id: 2, label: 'Foggia'},
            {id: 3, label: 'Lecce'},
            {id: 4, label: 'Taranto'},
            {id: 5, label: 'Brindisi'},
            {id: 6, label: 'Barletta-Andria-Trani'}];

        this.columns = [
            {id: 1, prop: 'provincia', name: 'Provincia', width: 50, headerClass: 'text-center border', cellClass: 'text-center border'},
            {id: 1, prop: 'comune', name: 'Comune', width: 50, headerClass: 'text-center border', cellClass: 'text-center border'},
            {id: 1, prop: 'ambito', name: 'Ambito', width: 50, headerClass: 'text-center border', cellClass: 'text-center border'},
            {
                id: 1,
                prop: 'tot_ist',
                name: 'Tot Istituzioni',
                width: 50,
                headerClass: 'text-center border',
                cellClass: 'text-center border'
            },
            {id: 1, prop: 'tot_alunni', name: 'Tot Alunni', width: 50, headerClass: 'text-center border', cellClass: 'text-center border'},
            {
                id: 1,
                prop: 'tot_alunni_disabili',
                name: 'Tot. Alunni Disabili',
                width: 50,
                headerClass: 'text-center border',
                cellClass: 'text-center border'
            },
            {id: 1, prop: 'tot_classi', name: 'Tot. Classi', width: 50, headerClass: 'text-center border', cellClass: 'text-center border'}
        ];
    }

    ngOnInit() {
        this.spinner.show();
        this.loadComuni();
        this.annoScolasticoService.getAnnoScolasticoObservable().subscribe(annoScolastico => {
            if (annoScolastico) {
                this.annoScolastico = annoScolastico;
                this.loadAmbiti();
                this.loadAmbitiStatistiche();

            }

        });
    }


    public loadAmbitiStatistiche() {
        this.ambitiService.statistiche(this.annoScolastico).subscribe(resp => {
            this.statisticheAmbito = resp;
            console.log(this.statisticheAmbito);
            this.formatRow();

        });

    }

    public loadAmbiti() {
        this.ambitiService.codiciList(this.annoScolastico).subscribe(resp =>
            this.ambiti = resp);

    }

    public loadComuni() {
        this.comuniService.list().subscribe(resp => {
            this.comuni = _.sortBy(resp, 'nome');
            this.comuniTot = this.comuni;
        });

    }


    private formatRow() {

        this.rows = [];

        if (this.statisticheAmbito) {
            this.statisticheAmbito.forEach((item: StatisticaAmbito) =>
                this.pushRow(item));
        }
        this.savedAllRows = this.rows;
        this.spinner.hide();
    }

    public pushRow(statistica: StatisticaAmbito) {

        this.rows.push(this.formatRowService.organicoDiDiritto1ab(statistica, this.ambiti, this.comuni));

    }

    public filter() {
        this.rows = this.savedAllRows;
        this.filterByProvincia();
        this.filterByAmbito();
        this.filterByComune();
        this.sortRowsByProvincia();
    }

    private filterByComune() {
        if (this.comuneSelected !== 'Tutti') {
            this.rows = this.rows.filter(
                item => item.comune_ist === this.comuneSelected.toUpperCase()
            );
        }
    }

    private filterByProvincia() {
        if (this.provinciaSelected !== 'Tutti') {
            this.rows = this.rows.filter(item => item.provincia === this.provinciaSelected);
            this.comuni = this.comuniTot.filter(item => item.provincia === this.provinciaSelected);
        }
    }

    private filterByAmbito() {
        if (this.ambitoSelected !== 'Tutti') {
            this.rows = this.rows.filter(item => item.ambito === this.ambitoSelected.toUpperCase());
        }
    }

    public exportCsv() {
        let columnsHeadersForExport = [];
        this.columns.forEach(item => columnsHeadersForExport.push(item.name));
        this.exportUtilsService.exportTableData(this.title + this.annoScolastico.label,
            this.savedAllRows, columnsHeadersForExport);

    }

    private sortRowsByProvincia() {
        this.rows = _.sortBy(this.rows, 'provincia');

    }


}
