/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, OnInit} from '@angular/core';
import {BacinoFile} from '../../../../shared/models/files/BacinoFile';

@Component({
    selector: 'app-bacini-utenza',
    templateUrl: './bacini-utenza.component.html',
    styleUrls: ['./bacini-utenza.component.css']
})
export class BaciniUtenzaComponent implements OnInit {

    public base_path_file = '/assets/files/baciniDiUtenza/';
    public allegati =
        {
            'Liceo': [
                new BacinoFile('', 'Artistico', 'LA.pdf'),
                new BacinoFile('', 'Classico', 'LC.pdf'),
                new BacinoFile('', 'Linguistico', 'LL.pdf'),
                new BacinoFile('', 'Musicale', 'LM.pdf'),
                new BacinoFile('', 'Scientifico', 'LS.pdf'),
                new BacinoFile('', 'Scienze Umane', 'LSU.pdf')],
            'Istituto_Tecnico': [
                new BacinoFile('IT01', 'Amministrazione finanza e marketing', 'IT01.pdf'),
                new BacinoFile('IT04', 'Turismo', 'IT04.pdf'),
                new BacinoFile('IT05', 'Meccanica meccatronica energia', 'IT05.pdf'),
                new BacinoFile('IT09', 'Trasporti e logistica', 'IT09.pdf'),
                new BacinoFile('IT10', 'Elettronica e elettrotecnica', 'IT10.pdf'),
                new BacinoFile('IT13', 'Informatica e telecomunicazioni', 'IT13.pdf'),
                new BacinoFile('IT15', 'Grafica e comunicazione', 'IT15.pdf'),
                new BacinoFile('IT16', 'Chimica materiali e tecnologie', 'IT16.pdf'),
                new BacinoFile('IT19', 'Sistema moda', 'IT19.pdf'),
                new BacinoFile('IT21', 'Agraria, agroalimentare e agroindustria', 'IT21.pdf'),
                new BacinoFile('IT24', 'Costruzioni, ambiente e territorio', 'IT24.pdf')],
            'Istituto_Professionale': [
                new BacinoFile('IP11', 'Agricoltura, sviluppo rurale, valorizzazione dei prodotti del territorio e gestione delle risorse forestali e montane', 'IP11_MAP.pdf'),
                new BacinoFile('IP13', 'Industria e artigianato per il made in italy', 'IP13.pdf'),
                new BacinoFile('IP14', 'Manutenzione e assistenza tecnica', 'IP14.pdf'),
                new BacinoFile('IP15', 'Gestione delle acque e risanamento ambientale', 'IP15.pdf'),
                new BacinoFile('IP16', 'Servizi commerciali', 'IP16.pdf'),
                new BacinoFile('IP17', 'Enogastronomia ed ospitalità alberghiera', 'IP17.pdf'),
                new BacinoFile('IP18', 'Servizi culturali e dello spettacolo', 'IP18.pdf'),
                new BacinoFile('IP19', 'Servizi per la sanità e l\'assistenza sociale', 'IP19.pdf'),
                new BacinoFile('IP20', 'Arti ausiliarie delle professioni sanitarie: odontotecnico', 'IP20.pdf'),
                new BacinoFile('IP21', 'Arti ausiliarie delle professioni sanitarie: ottico', 'IP21.pdf')]
        };

    constructor() {
    }

    public getLength(item) {
        if (Array.isArray(item)) {
            return item.length;
        } else {
            return -1;
        }
    }

    ngOnInit() {
        console.log(this.allegati);
    }

}
