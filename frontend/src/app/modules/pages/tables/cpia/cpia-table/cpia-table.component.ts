/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, OnInit} from '@angular/core';
import {AnnoScolastico} from '../../../../../shared/models/AnnoScolastico';
import {AnnoScolasticoService} from '../../../../anno-scolastico/anno-scolastico.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {CpiaService} from '../cpia.service';
import {PercentOrNdPipe} from '../../../../../shared/pipe/percentageOrNd.pipe';

export interface CpiaResume {
    cpia: {
        header: Array<any>,
        data: Array<any>
    },
    livello1: {
        header: Array<any>,
        data: Array<any>
    },
    livello2: {
        header: Array<any>,
        data: Array<any>
    },

}

@Component({
    selector: 'app-cpia-table',
    templateUrl: './cpia-table.component.html',
    styleUrls: ['./cpia-table.component.css']
})
export class CpiaTableComponent implements OnInit {
    // Specifica

    /*  Inserire una tabella consultabile nella quale sono riportati i dati del file “CPIA_RICOGNIZIONE.xls”
        filtrabili perProvincia,Denominazione CPIA,Livello,Comune punto di erogazione,
        Tipologia percorso erogato(nel caso di I livello)
        oCodice/Denominazione tipo di percorso(per il II livello)
     */


    public isCpiaLivelloActive = true;
    public isPrimoLivelloActive = false;
    public isSecondoLivelloActive = false;

    public annoScolastico: AnnoScolastico;
    public title = 'CPIA';
    public columnsCpia = [];
    public rowsCpia;
    public columnsCpiaLivello1 = [];
    public rowsCpiaLivello1;
    public columnsCpiaLivello2 = [];
    public rowsCpiaLivello2;


    constructor(private annoScolasticoService: AnnoScolasticoService,
                private spinner: NgxSpinnerService,
                private cpiaService: CpiaService,
    ) {
    }

    ngOnInit() {
        this.spinner.show();
        this.annoScolasticoService.getAnnoScolasticoObservable().subscribe(annoScolastico => {
            if (annoScolastico) {
                this.annoScolastico = annoScolastico;

                this.cpiaService.dashboard(annoScolastico.id).subscribe((resp: CpiaResume) => {
                    console.log(resp);
                    resp.cpia.header.map(item => this.columnsCpia.push({
                        'name': item.label,
                        'prop': item.name,
                        pipe: new PercentOrNdPipe('it-IT')
                    }));
                    this.rowsCpia = resp.cpia.data;
                    resp.livello1.header.map(item => this.columnsCpiaLivello1.push({
                        'name': item.label,
                        'prop': item.name,
                        pipe: new PercentOrNdPipe('it-IT')
                    }));
                    this.rowsCpiaLivello1 = resp.livello1.data;
                    resp.livello2.header.map(item => this.columnsCpiaLivello2.push({
                        'name': item.label,
                        'prop': item.name,
                        pipe: new PercentOrNdPipe('it-IT')
                    }));
                    this.rowsCpiaLivello2 = resp.livello2.data;

                });

                this.spinner.hide();
            }
        });
    }

    public toggleCpiaLivello() {
        this.isCpiaLivelloActive = !this.isCpiaLivelloActive;
    }

    public togglePrimoLivello() {
        this.isPrimoLivelloActive = !this.isPrimoLivelloActive;
    }

    public toggleSecondoLivello() {
        this.isSecondoLivelloActive = !this.isSecondoLivelloActive;
    }

}
