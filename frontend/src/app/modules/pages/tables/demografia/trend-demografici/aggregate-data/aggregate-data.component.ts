/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import * as _ from 'lodash';
import {StatisticaAmbito} from '../../../../../../shared/models/StatisticaAmbito';


@Component({
    selector: 'aggregate-data',
    templateUrl: './aggregate-data.component.html',
    styleUrls: ['./aggregate-data.component.css']
})
export class AggregateDataComponent implements OnInit, OnChanges {
    @Input()
    rows: any[];

    @Input()
    statisticaAmbito: StatisticaAmbito;

    public totComuni = 0;
    public totAmbiti = 0;

    constructor() {
    }

    ngOnInit() {
        this.aggregaDati();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.aggregaDati();
    }

    public aggregaDati() {

        this.totComuni = Object.keys(_.groupBy(this.rows, 'comune_is')).length;
        this.totAmbiti = Object.keys(_.groupBy(this.rows, 'ambito')).length;

    }


}
