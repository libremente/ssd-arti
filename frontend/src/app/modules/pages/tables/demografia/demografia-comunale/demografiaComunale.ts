/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

export class DemografiaComunale {
    provincia: String;
    comune: String;
    popTot2019: Number;
    pop_3_13_2015: Number;
    pop_3_13_2019: Number;
    var_3_13_2019_2015: Number;
    prev_3_13_2023: Number;
    var_3_13_2023_2019: Number;
    pop_3_5_2015: Number;
    pop_3_5_2019: Number;
    var_3_5_2019_2015: Number;
    prev_3_5_2023: Number;
    var_3_5_2023_2019: Number;
    pop_6_10_2015: Number;
    pop_6_10_2019: Number;
    var_6_10_2019_2015: Number;
    prev_6_10_2023: Number;
    var_6_10_2023_2019: Number;
    pop_11_13_2015: Number;
    pop_11_13_2019: Number;
    var_11_13_2019_2015: Number;
    prev_11_13_2023: Number;
    var_11_13_2023_2019: Number;
    pop_14_18_2015: Number;
    pop_14_18_2019: Number;
    var_14_18_2019_2015: Number;
    prev_14_18_2023: Number;
    var_14_18_2023_2019: Number;
}
