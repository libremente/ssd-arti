/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, OnInit} from '@angular/core';
import {Comune} from '../../../../../shared/models/Comune';
import {Provincia} from '../../../../../shared/models/Provincia';
import {NgxSpinnerService} from 'ngx-spinner';
import {ExportToolsService} from '../../../../../shared/utils/export-tools.service';
import * as _ from 'lodash';
import {DemografieService} from '../demografie.service';
import {DemografiaComunale} from './demografiaComunale';


@Component({
    selector: 'app-demografia-comunale',
    templateUrl: './demografia-comunale.component.html',
    styleUrls: ['./demografia-comunale.component.scss']
})
export class DemografiaComunaleComponent implements OnInit {


    public columns;


    public rows: DemografiaComunale[];

    public title = 'Demografia Comunale';

    public comuni: Comune[];
    private saveComuniTot: Comune[];

    public comuneSelected = 'Tutti';
    public province: Provincia[];
    public provinciaSelected = 'Tutti';

    public unfilteredRows: Array<any>;

    public isLoading = true;

    comuneSelezionato: DemografiaComunale;


    constructor(private spinner: NgxSpinnerService,
                private exportUtilsService: ExportToolsService,
                private trendService: DemografieService) {
        this.rows = [];
        this.columns = [

            {field: 'provincia', header: 'Comune'},
            {field: 'comune', header: 'Comune'},
            {field: 'popTot2019', header: 'Popolazione 2019'},

            {field: 'pop_3_13_2015'},
            {field: 'pop_3_13_2019'},
            {field: 'var_3_13_2019_2015'},
            {field: 'prev_3_13_2023'},
            {field: 'var_3_13_2023_2019'},
            {field: 'pop_3_5_2015'},
            {field: 'pop_3_5_2019'},
            {field: 'var_3_5_2019_2015'},
            {field: 'prev_3_5_2023'},
            {field: 'var_3_5_2023_2019'},
            {field: 'pop_6_10_2015'},
            {field: 'pop_6_10_2019'},

            {field: 'var_6_10_2019_2015'},
            {field: 'prev_6_10_2023'},
            {field: 'var_6_10_2023_2019'},
            {field: 'pop_11_13_2015'},
            {field: 'pop_11_13_2019'},
            {field: 'var_11_13_2019_2015'},

            {field: 'prev_11_13_2023'},
            {field: 'var_11_13_2023_2019'},
            {field: 'pop_14_18_2015'},
            {field: 'pop_14_18_2019'},
            {field: 'var_14_18_2019_2015'},
            {field: 'prev_14_18_2023'},
            {field: 'var_14_18_2023_2019'}


        ];

    }

    ngOnInit() {


        this.trendService.demografiaComunale().subscribe(resp => {
            // console.log('risposta', resp);
            this.rows = resp;
            this.unfilteredRows = this.rows;
            this.loadFilters();
            this.isLoading = false;
        });

    }


    private resetFiltri() {
        this.provinciaSelected = 'Tutti';
        this.comuneSelected = 'Tutti';
    }


    private loadFilters() {
        this.province = (_.uniq(this.unfilteredRows.map(item => item.provincia))).sort();
        this.comuni = _.sortBy(_.uniqBy(this.unfilteredRows.map((item) => {
            return {'provincia': item.provincia, 'nome': item.comune};
        }), 'nome'), 'nome');
        this.saveComuniTot = this.comuni;

    }


    public filter() {
        this.spinner.show();
        this.rows = this.unfilteredRows;
        this.filterByProvincia();
        this.filterByComune();
        this.spinner.hide();
    }

    public filterByProvincia() {
        if (this.provinciaSelected !== 'Tutti') {
            this.rows = this.rows.filter(
                item => item.provincia.toLowerCase() === this.provinciaSelected.toLowerCase()
            );
            this.comuni = this.saveComuniTot.filter(item => item.provincia.toLowerCase() === this.provinciaSelected.toLowerCase());
        } else {
            this.comuneSelected = 'Tutti';
            this.comuni = this.saveComuniTot;

        }
    }


    private filterByComune() {
        if (this.comuneSelected !== 'Tutti') {
            this.rows = this.rows.filter(
                item => item.comune.toLowerCase() === this.comuneSelected.toLowerCase()
            );

        } else {
        }
    }


    public exportCsv() {
        const columnsHeadersForExport = [];
        this.columns.forEach(item => columnsHeadersForExport.push(item.name ? item.name : item.prop));
        this.exportUtilsService.exportTableData(this.title, this.unfilteredRows, columnsHeadersForExport);
    }


}
