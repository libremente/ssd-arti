/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Injectable} from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AnnoScolastico} from '../../../../shared/models/AnnoScolastico';
import {environment} from '../../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class DemografieService {
    private url = environment.apiUrl + '/trendDemografici';

    constructor(private http: HttpClient) {

    }

    public trend(annoScolastico: AnnoScolastico): Observable<any> {

        return this.http.get<any>(this.url + '/trend?idAnno=' + annoScolastico.id);
    }


    public popolazioneScolastica(annoScolastico: AnnoScolastico): Observable<any> {
        return this.http.get(this.url + '/popolazioneScolastica?idAnno=' + annoScolastico.id);
    }

    public trendIs(annoScolastico: AnnoScolastico, codiceSede: String): Observable<any> {
        return this.http.get(this.url + '/trendIs?idAnno=' + annoScolastico.id + '&codiceIs=' + codiceSede);
    }

    public trendPe1(annoScolastico: AnnoScolastico, codiceSede: String): Observable<any> {
        return this.http.get(this.url + '/trendPe1?idAnno=' + annoScolastico.id + '&codicePe=' + codiceSede);
    }

    public trendPe2(annoScolastico: AnnoScolastico, codiceSede: String): Observable<any> {
        return this.http.get(this.url + '/trendPe2?idAnno=' + annoScolastico.id + '&codicePe=' + codiceSede);
    }

    public demografiaComunale(): Observable<any> {
        return this.http.get(this.url + '/');
    }


}
