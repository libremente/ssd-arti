/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AmbitiTerritorialiService} from '../../../../shared/services/ambiti/ambiti-territoriali.service';
import {ComuniService} from '../../../comuni/comuni.service';
import {Comune} from '../../../../shared/models/Comune';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {NgxSpinnerService} from 'ngx-spinner';
import * as _ from 'lodash';
import {AnnoScolasticoService} from '../../../anno-scolastico/anno-scolastico.service';
import {Subscription} from 'rxjs';
import {AnnoScolastico} from '../../../../shared/models/AnnoScolastico';
import {StatisticaAmbito} from '../../../../shared/models/StatisticaAmbito';
import {Provincia} from '../../../../shared/models/Provincia';
import {ISService} from '../../../../shared/services/istituzioniScolastiche/is.service';
import {IstituzioneScolastica} from '../../../../shared/models/IstituzioneScolastica';
import {Sede} from '../../../../shared/models/Sede';
import {SedeCiclo2} from '../../../../shared/models/SedeCiclo2';
import {SedeCiclo1} from '../../../../shared/models/SedeCiclo1';
import {FormatRowTableService} from '../../../../shared/services/format-row-table.service';
import {ExportToolsService} from '../../../../shared/utils/export-tools.service';


@Component({
    selector: 'app-organico-primo-ciclo-od',
    templateUrl: './organico-primo-ciclo-od.component.html',
    styleUrls: ['./organico-primo-ciclo-od.component.scss']
})
export class OrganicoPrimoCicloOdComponent implements OnInit, AfterViewInit, OnDestroy {


    @Input()
    public showDashboard: boolean;


    @ViewChild(DatatableComponent) table: DatatableComponent;
    private filterAmbito = false;
    private saveAllRows: Array<any>;
    annoScolastico: AnnoScolastico;
    private annoScolasticoSubscription: Subscription;
    private statisticheAmbito: StatisticaAmbito[];
    public rows;
    public columns;
    public ambiti: Array<any>;
    public gradi: Array<any> = ['AA', 'EE', 'MM', 'SS'];
    public province: Provincia[];
    public provinciaSelected = 'Tutti';
    public selectAmbito = 'Tutti';
    public gradoSelected = 'Tutti';
    public comuneSelected = 'Tutti';
    public comuni: Comune[];
    private comuniTot: Comune[];
    public searchText = '';
    public statisticaAmbitoSelected: StatisticaAmbito;
    public denominazioneSelected = 'Tutti';
    public denominazioni: string[] = null;
    public title = 'Organico Primo Ciclo ';

    constructor(
        private comuniService: ComuniService,
        private annoScolasticoService: AnnoScolasticoService,
        private isService: ISService,
        private ambitoService: AmbitiTerritorialiService,
        private spinner: NgxSpinnerService,
        private formatRowService: FormatRowTableService,
        private exportUtilsService: ExportToolsService
    ) {

        this.columns = [
            {
                id: 1,
                prop: 'grado_istr',
                name: 'GRADO ISTRUZ',
                width: 50,
                headerClass: 'text-center border',
                cellClass: 'text-center border'
            },
            {id: 2, prop: 'provincia', name: 'PROVINCIA', width: 50, headerClass: 'text-center border', cellClass: 'text-center border'},
            {id: 3, prop: 'ambito', name: 'AMBITO', width: 50, headerClass: 'text-center border', cellClass: 'text-center border'},
            {id: 4, prop: 'comune_is', name: 'COMUNE IS', width: 50, headerClass: 'text-center border', cellClass: 'text-center border'},
            {id: 5, prop: 'codice_is', name: 'CODICE IS', width: 50, headerClass: 'text-center border', cellClass: 'text-center border'},
            {
                id: 6,
                prop: 'denominazione_is',
                name: 'DENOM IS',
                width: 50,
                headerClass: 'text-center border',
                cellClass: 'text-center border'
            },
            {
                id: 7,
                prop: 'comune__punto_di_erog',
                name: 'COMUNE PUNTO EROG',
                width: 50,
                headerClass: 'text-center border',
                cellClass: 'text-center border'
            },
            {
                id: 8,
                prop: 'denominazione_punto_erog',
                name: 'DENOM PUNTO EROG',
                width: 50,
                headerClass: 'text-center border',
                cellClass: 'text-center border'
            },
            {id: 9, prop: 'alunni', name: 'ALUNNI', width: 50, headerClass: 'text-center border', cellClass: 'text-center border'},
            {
                id: 10,
                prop: 'alunni_handicap',
                name: 'ALUNNI HANDICAP',
                width: 50,
                headerClass: 'text-center border',
                cellClass: 'text-center border'
            },
            {id: 11, prop: 'num_sez', name: 'SEZIONI', width: 50, headerClass: 'text-center border', cellClass: 'text-center border'},
            //  {prop: 'alunni_orario_normale', name: 'Alunni Orario Normale'},
            //   {prop: 'sez_orario_normale', name: 'Sezione Orario Normale'},
//    {prop: 'alunni_orario_ridotto', name: 'Alunni Orario Ridotto'},
            //  {prop: 'sez_orario_ridotto', name: 'Sezione Orario Ridotto'},
            {
                id: 12,
                prop: 'cod_indirizzo_di_studio',
                name: 'COD INDIR STUDIO',
                width: 50,
                headerClass: 'text-center border',
                cellClass: 'text-center border'
            },
            {
                id: 13,
                prop: 'den_indirizzo_di_studio',
                name: 'DENOM INDIR STUDIO',
                width: 50,
                headerClass: 'text-center border',
                cellClass: 'text-center border'
            },
            {
                id: 14,
                prop: 'num_alunni_per_organico',
                name: 'NUM ALUNNI',
                width: 50,
                headerClass: 'text-center border',
                cellClass: 'text-center border'
            }
        ];
        this.province = [{id: 1, label: 'Bari'},
            {id: 2, label: 'Foggia'},
            {id: 3, label: 'Lecce'},
            {id: 4, label: 'Taranto'},
            {id: 5, label: 'Brindisi'},
            {id: 6, label: 'Barletta-Andria-Trani'}];
    }


    public getRowClass = row => {
        if (row.cod_ambito === this.selectAmbito) {
            return {
                'row-color': true
            };
        } else {

        }
    };

    public exportCsv() {
        let columnsHeadersForExport = [];
        this.columns.forEach(item => columnsHeadersForExport.push(item.name));
        this.exportUtilsService.exportTableData(this.title + this.annoScolastico.label,
            this.saveAllRows, columnsHeadersForExport);

    }


    ngOnInit() {
        this.annoScolasticoService.getAnnoScolasticoObservable().subscribe(annoScolastico => {

            if (annoScolastico) {
                this.spinner.show();
                this.resetFiltri();
                this.annoScolastico = annoScolastico;
                this.loadAmbitiStatistiche();
                this.loadDati();

            }

        });


    }

    private resetFiltri() {
        this.comuneSelected = 'Tutti';
        this.gradoSelected = 'Tutti';
        this.selectAmbito = 'Tutti';
        this.searchText = '';
    }

    private loadAmbitiStatistiche() {
        this.ambitoService.statistiche(this.annoScolastico).subscribe(resp =>
                this.statisticheAmbito = resp,
            err => {
                console.log(err);
                this.spinner.show();
            });
    }


    private loadDati() {
        this.comuniService.list().subscribe(
            resp => {
                this.comuni = _.sortBy(resp, 'nome');
                this.comuniTot = this.comuni;

                this.isService.ciclo1(this.annoScolastico).subscribe(ciclo1 => {
                    this.isService.ciclo2(this.annoScolastico).subscribe(ciclo2 => {

                        this.formatRowData(ciclo1, ciclo2);

                        this.loadAmbiti();
                        this.sortRowsByProvincia();
                    }, err => {
                        console.log(err);
                        this.spinner.hide();
                    });

                }, err => {
                    console.log(err);
                    this.spinner.hide();
                });
            }, err => {
                console.log(err);
                this.spinner.show();
            });


    }

    private loadAmbiti() {

        // gestire tutto per l'anno
        this.ambitoService.codiciList(this.annoScolastico).subscribe(
            resp => {
                console.log(resp);
                this.ambiti = resp;
                this.spinner.hide();
            },
            err => {
                this.spinner.hide();
                alert('impossibile caricare gli ambiti');
                console.log('errore', err);
            }
        );
    }

    private formatRowData(ciclo1, ciclo2) {

        this.rows = [];

        if (ciclo1) {
            ciclo1.forEach((ist: IstituzioneScolastica) =>
                ist.sediCiclo1.forEach((sede: SedeCiclo1) =>
                    this.pushRow(ist, sede)));
        }
        if (ciclo2) {
            ciclo2.forEach((ist: IstituzioneScolastica) =>
                ist.sediCiclo2.forEach((sede: SedeCiclo2) => this.pushRow(ist, sede)));
        }
        this.saveAllRows = this.rows;
    }

    public pushRow(ist: IstituzioneScolastica, sede: Sede) {

        this.rows.push(this.formatRowService.organicoDiDiritto1c(ist, sede, this.comuni));

    }


    public filter() {
        this.spinner.show();
        this.rows = this.saveAllRows;
        if (this.searchText.trim() !== '') {
            this.filterText();
        }
        this.filterTable();
        this.filterByGrado();
        this.filterByProvincia();
        this.filterByComune();
        this.sortRowsByProvincia();
        this.spinner.hide();
    }


    public updateFilterWithText(event) {
        if (event.keyCode === 13) {
            this.searchText = event.target.value.toString();
        }
        this.filter();
    }


    public filterByProvincia() {
        if (this.provinciaSelected !== 'Tutti') {
            this.rows = this.rows.filter(
                item => item.provincia === this.provinciaSelected.toUpperCase()
            );
            this.comuni = this.comuniTot.filter(item => item.provincia === this.provinciaSelected);
        }
    }


    private filterText() {
        const val = this.searchText.toLowerCase();
        const colsAmt = this.columns.length;
        const keys = Object.keys(this.saveAllRows[0]);
        this.rows = this.rows.filter(function (item) {
            // iterate through each row's column data
            for (let i = 0; i < colsAmt; i++) {
                if (
                    item[keys[i]] &&
                    item[keys[i]]
                        .toString()
                        .toLowerCase()
                        .includes(val)
                ) {
                    return true;
                }
            }
        });
        this.table.offset = 0;
    }


    private filterByComune() {
        if (this.comuneSelected !== 'Tutti') {
            this.rows = this.rows.filter(
                item => item.comune_is === this.comuneSelected.toUpperCase()
            );
        }

    }

    private filterByGrado() {
        if (this.gradoSelected !== 'Tutti') {
            this.rows = this.rows.filter(
                item => item.grado_istr === this.gradoSelected
            );
        }

    }

    private filterTable() {
        if (this.selectAmbito !== 'Tutti') {
            this.rows = this.rows.filter(
                item => item.ambito === this.selectAmbito
            );

            this.statisticaAmbitoSelected = this.statisticheAmbito.filter(item => item.codice == this.selectAmbito)[0];
        } else {
            this.statisticaAmbitoSelected = null;
        }
    }

    private sortRowsByProvincia() {
        this.rows = _.sortBy(this.rows, 'provincia');
    }

    ngOnDestroy() {
        if (this.annoScolasticoSubscription) {
            this.annoScolasticoSubscription.unsubscribe();
        }
    }


    ngAfterViewInit() {
        this.spinner.show();

    }
}
