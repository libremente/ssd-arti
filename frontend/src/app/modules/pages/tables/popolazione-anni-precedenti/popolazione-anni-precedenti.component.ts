/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, OnDestroy, OnInit} from '@angular/core';
import {AnnoScolastico} from '../../../../shared/models/AnnoScolastico';
import {AnnoScolasticoService} from '../../../anno-scolastico/anno-scolastico.service';
import {Subscription} from 'rxjs';
import {DemografieService} from '../demografia/demografie.service';
import {Provincia} from '../../../../shared/models/Provincia';
import {Comune} from '../../../../shared/models/Comune';
import {ComuniService} from '../../../comuni/comuni.service';
import * as _ from 'lodash';
import {NgxSpinnerService} from 'ngx-spinner';
import {NdPipe} from '../../../../shared/pipe/nd.pipe';
import {ExportToolsService} from '../../../../shared/utils/export-tools.service';

@Component({
    selector: 'app-popolazione-anni-precedenti',
    templateUrl: './popolazione-anni-precedenti.component.html',
    styleUrls: ['./popolazione-anni-precedenti.component.css']
})
export class PopolazioneAnniPrecedentiComponent implements OnInit, OnDestroy {


    public province: Provincia[];
    public comuni: Comune[];
    public comuniTot: Comune[];
    public title = 'Popolazione Scolastica';

    public provinciaSelected = 'Tutti';
    public comuneSelected = 'Tutti';

    public columns;
    public rows;
    public notFiltered;
    public annoScolastico: AnnoScolastico;
    public annoScolasticoSubscription: Subscription;

    ndPipe = new NdPipe();

    constructor(private annoScolasticoService: AnnoScolasticoService,
                private comuniService: ComuniService,
                private  trendService: DemografieService,
                private spinner: NgxSpinnerService,
                private exportUtilsService: ExportToolsService
    ) {
        this.rows = [];
        this.columns = [];

        this.province = [{id: 1, label: 'Bari'},
            {id: 2, label: 'Foggia'},
            {id: 3, label: 'Lecce'},
            {id: 4, label: 'Taranto'},
            {id: 5, label: 'Brindisi'},
            {id: 6, label: 'Barletta-Andria-Trani'}];

    }


    ngOnInit() {
        this.comuniService.list().subscribe(resp => {
            this.comuni = _.sortBy(resp, 'nome');
            this.comuniTot = this.comuni;
        });

        this.annoScolasticoSubscription = this.annoScolasticoService.getAnnoScolasticoObservable().subscribe(anno => {
            this.spinner.show();
            this.rows = [];
            this.columns = [];
            if (anno) {
                this.annoScolastico = anno;
                this.trendService.popolazioneScolastica(this.annoScolastico).subscribe(resp => {
                        resp.header.forEach(item => {

                            this.columns.push({
                                prop: item.toString(),
                                name: item.toString()
                            });
                        });
                        this.rows = resp.data;
                        this.notFiltered = this.rows;
                        console.log('popolazione anni precendenti', this.rows);
                        console.log(this.columns);
                    }
                );
            }
            this.spinner.hide();
        });
        this.spinner.hide();

    }


    filter() {
        this.rows = this.notFiltered;
        this.filterByProvincia();
        this.filterByComune();

    }

    filterByProvincia() {
        if (this.provinciaSelected !== 'Tutti') {
            this.rows = this.rows.filter(item => item['Provincia PE'] === this.provinciaSelected);
            this.comuni = this.comuniTot.filter(item => item.provincia === this.provinciaSelected);
        }
    }


    filterByComune() {
        if (this.comuneSelected !== 'Tutti') {
            this.rows = this.rows.filter(item => item['Comune PE'] === this.comuneSelected);
        }
    }

    public exportCsv() {
        let columnsHeadersForExport = [];
        this.columns.forEach(item => columnsHeadersForExport.push(item.name));
        this.exportUtilsService.exportTableData(this.title + this.annoScolastico.label,
            this.notFiltered, columnsHeadersForExport);

    }


    ngOnDestroy(): void {
        if (this.annoScolasticoSubscription) {
            this.annoScolasticoSubscription.unsubscribe();
        }
    }

}
