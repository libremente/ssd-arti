/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, OnInit} from '@angular/core';
import {MapsTable} from '../../../../shared/settings/table.settings';


@Component({
    selector: 'app-popolazione-scolastica',
    templateUrl: './popolazione-scolastica.component.html',
    styleUrls: ['./popolazione-scolastica.component.css']
})
export class PopolazioneScolasticaComponent implements OnInit {

    public columns;
    public rows;

    constructor() {
        this.rows = [];
        this.columns = MapsTable.popolazioneScolastica14_18;
    }


    public getRowClass = row => {
        /* if (row.cod_ambito == this.selectAmbito) {
             return {
                 'row-color': true
             };
         } else {

         }*/
    };

    ngOnInit() {
    }

}
