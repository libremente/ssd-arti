/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, OnInit} from '@angular/core';
import {RichiestePareriPianiService} from '../../../shared/services/documentazione2021/richieste-pareri-piani.service';

@Component({
    selector: 'app-richieste-pareri-piani-file-table',
    templateUrl: './richieste-pareri-piani-file-table.component.html',
    styleUrls: ['./richieste-pareri-piani-file-table.component.scss']
})
export class RichiestePareriPianiComponentFileTable implements OnInit {

    public messages = {
        emptyMessage: 'Nessun dato da visualizzare',
        totalMessage: '',
        selectedMessage: 'selezionato'
    };
    public columns;
    public rows;
    private savedAllRows;

    public searchTextComune: string;
    public searchTextCodiceIstituzione: string;


    constructor(private richiestePareriPianiService: RichiestePareriPianiService) {
        this.rows = [];
    }


    private updateFilterCodiceIstituzione() {

        this.rows = this.rows.filter(item => item['Codice istituzione scolastica'].toLowerCase()
            .includes(this.searchTextCodiceIstituzione.toLowerCase()));

    }


    private updateFilterComune() {

        this.rows = this.rows.filter(item => item['Comune/i punti di erogazione'].toLowerCase()
            .includes(this.searchTextComune.toLowerCase()));

    }


    public filterText($event) {
        this.rows = this.savedAllRows;
        this.updateFilterComune();
        this.updateFilterCodiceIstituzione();
    }


    ngOnInit() {


        this.richiestePareriPianiService.indexRichiestePareriPiani().map(item => {
            this.columns = item.header;
            this.rows = item.data;
            this.savedAllRows = this.rows;
            return this.rows;
        }).subscribe(resp => {
                console.log('tabella', resp);
                this.rows.map(item => {
                    const keys = Object.keys(item);
                    for (let i = 4; i < keys.length; i++) {
                        if (item[keys[i]]) {
                            item[keys[i]] = '<a   href="' + item[keys[i]] + '" download><i class="btn btn-outline-info btn-sm fa fa-save btn-custom-save"</i></a>';
                        }
                    }

                });
            }
        );
    }

}
