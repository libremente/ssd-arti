/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-linee-guida',
    templateUrl: './linee-guida.component.html',
    styleUrls: ['./linee-guida.component.css']
})
export class LineeGuidaComponent implements OnInit {

    allegati = [
        {
            nome: 'DGR n. 1786 del 07/10/2019',
            link: '/assets/files/documentazione2021/dgr 1786_2019_10_07.pdf',
            descrizione: 'Piano regionale di dimensionamento scolastico e programmazione dell\'offerta formativa. ' +
                'Linee di indirizzo per il biennio 2020/21 e 2021/22.'
        },
        {
            nome: 'Allegato A - Linee di indirizzo',
            link: '/assets/files/documentazione2021/Allegato_A-LINEE-DI-INDIRIZZO.pdf',
            descrizione: 'Piano regionale di dimensionamento scolastico e programmazione dell’offerta formativa.<br>' +
                'Linee di indirizzo per il biennio 2020/21 e 2021/22.'
        },
        {
            nome: 'Allegato A.1',
            link: '/assets/files/documentazione2021/Allegato_A.1.pdf',
            descrizione: 'Prima ricognizione disallineamenti tra situazioni in punto di fatto e dati SIDI e ARES.'

        },
        {
            nome: 'Allegato A.2',
            link: '/assets/files/documentazione2021/Allegato_A.2.pdf',
            descrizione: 'Scenario d’assetto delle Istituzioni Scolastiche pugliesi.'

        },
        {
            nome: 'Allegato A.3',
            link: '/assets/files/documentazione2021/Allegato_A.3.pdf',
            descrizione: 'Le vocazioni produttive e l’offerta formativa in Puglia.'

        }
    ];

    constructor() {
    }

    ngOnInit() {
    }

}
