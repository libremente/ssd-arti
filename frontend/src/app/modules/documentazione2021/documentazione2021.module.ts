/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {Documentazione2021Component} from './documentazione2021.component';
import {WipComponent} from './wip/wip.component';
import {SharedModule} from '../shared.module';
import {LineeGuidaComponent} from './linee-guida/linee-guida.component';
import {RichiestaPareriPianiComponent} from './richiesta-pareri-piani/richiesta-pareri-piani.component';
import {RichiestePareriPianiComponentFileTable} from './richieste-pareri-piani-file-table/richieste-pareri-piani.component-file-table';
import {RichiestePareriPianiService} from '../../shared/services/documentazione2021/richieste-pareri-piani.service';
import {DocumentiUtiliComponent} from './documenti-utili/documenti-utili.component';


const routes: Routes = [
    {
        path: '',
        data: {
            title: 'documentazione2021',
            urls: [
                {title: 'Documentazione2021Module', url: '/'},
            ]
        },
        component: Documentazione2021Component,
        children: [
            {
                path: 'lineeGuida',
                component: LineeGuidaComponent,
                data: {
                    title: 'Documentazione 20/21: linee guida',
                    showSelettoreAnno: true
                }
            },
            {
                path: 'documentiUtili',
                component: DocumentiUtiliComponent,
                data: {
                    title: 'Documentazione 20/21: documenti utili',
                    showSelettoreAnno: true
                }
            },
            {
                path: 'richiestePareriPiani',
                component: RichiestaPareriPianiComponent,
                data: {
                    title: 'Documentazione 20/21: richieste pareri piani',
                    showSelettoreAnno: true
                }
            },

        ]
    }
];

@NgModule({
    declarations: [Documentazione2021Component, WipComponent, LineeGuidaComponent,
        RichiestaPareriPianiComponent, RichiestePareriPianiComponentFileTable, DocumentiUtiliComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
    ],
    providers: [RichiestePareriPianiService]
})
export class Documentazione2021Module {
}
