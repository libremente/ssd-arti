import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {NotFoundComponent} from './404/not-found.component';
import {LockComponent} from './lock/lock.component';
import {LoginComponent} from './login/login.component';
import {SignupComponent} from './signup/signup.component';

import {AuthenticationRoutes} from './authentication.routing';
import {CommonModule} from '@angular/common';

@NgModule({
    imports: [CommonModule, RouterModule.forChild(AuthenticationRoutes), NgbModule],
    declarations: [NotFoundComponent, LoginComponent, SignupComponent, LockComponent]
})
export class AuthenticationModule {
}
