/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {AnnoScolastico} from '../../shared/models/AnnoScolastico';

@Injectable({
    providedIn: 'root'
})
export class AnnoScolasticoService {

    private url = environment.apiUrl + '/annoScolastico';


    private annoCorrente: BehaviorSubject<AnnoScolastico> = new BehaviorSubject(null);

    constructor(private http: HttpClient) {

    }

    public list(): Observable<Array<AnnoScolastico>> {
        return this.http.get<Array<AnnoScolastico>>(this.url + '/list');
    }

    public get(idAnnoScolastico: number): Observable<AnnoScolastico> {
        return this.http.get<AnnoScolastico>(this.url + '/' + idAnnoScolastico);

    }

    public getAnnoCorrente(): Observable<AnnoScolastico> {
        return this.http.get<AnnoScolastico>(this.url + '/corrente');

    }


    selezionaAnno(anno: AnnoScolastico) {
        this.annoCorrente.next(anno);
    }

    getAnnoScolasticoObservable(): Observable<AnnoScolastico> {
        return this.annoCorrente.asObservable();
    }

    getAnnoScolastico(): AnnoScolastico {
        return this.annoCorrente.getValue();
    }
}
