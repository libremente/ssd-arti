/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {AfterViewInit, Component, OnInit} from '@angular/core';
import {AnnoScolasticoService} from '../../anno-scolastico.service';
import {AnnoScolastico} from '../../../../shared/models/AnnoScolastico';


@Component({
    selector: 'app-selettore-anno-scolastico',
    templateUrl: './selettore-anno.component.html',
    styleUrls: ['./selettore-anno.component.css']
})
export class SelettoreAnnoComponent implements OnInit, AfterViewInit {


    // Attenzione: l'anno scolastico va preso a livello di servizio!
    // @Output()
    // yearSelected: EventEmitter<AnnoScolastico> = new EventEmitter<AnnoScolastico>();

    public anniScolastici: AnnoScolastico[] = [];

    constructor(public annoScolasticoService: AnnoScolasticoService) {
    }

    ngOnInit() {
        this.annoScolasticoService.list().subscribe(anniScolastici => {
            this.anniScolastici = anniScolastici;
            this.annoScolasticoService.selezionaAnno(anniScolastici[0]);
        });
    }

    ngAfterViewInit(): void {

    }

    public selectYear(anno: AnnoScolastico) {
        this.annoScolasticoService.selezionaAnno(anno);
        // this.yearSelected.emit(anno);

    }

}
