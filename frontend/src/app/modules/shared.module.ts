/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SedeCiclo1Service} from '../shared/services/sedeciclo1/sede-ciclo1.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {BuildMapService} from '../maps/mappa-puglia/build-map/build-map.service';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {SpinnerComponent} from '../shared/components/spinner.component';
import {NgxSpinnerModule} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {FormatRowTableService} from '../shared/services/format-row-table.service';
import {DemografieService} from './pages/tables/demografia/demografie.service';
import {DispersioneService} from '../shared/services/dispersione/dispersione.service';
import {NdPipe} from '../shared/pipe/nd.pipe';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {EdificiUtilsService} from '../shared/services/edifici/edifici-utils.service';
import {MappaPugliaUtilsService} from '../shared/services/mappa-puglia/mappa-puglia-utils.service';
import {SediCiclo2Service} from '../shared/services/sediciclo2/sedi-ciclo2.service';

import {TableModule} from 'primeng/table';
import {ChartModule, TreeTableModule} from 'primeng/primeng';
import {CpiaService} from './pages/tables/cpia/cpia.service';

@NgModule({
    declarations: [SpinnerComponent, NdPipe],
    imports: [
        CommonModule,
        FormsModule,
        NgxDatatableModule,
        NgbModule.forRoot(),
        NgxSpinnerModule,
        PerfectScrollbarModule,
        HttpClientModule,
        TableModule,
        TreeTableModule,
        ChartModule

    ],
    exports: [CommonModule, NgxDatatableModule, NgbModule, NgxSpinnerModule, ChartModule, PerfectScrollbarModule, FormsModule, HttpClientModule,
        TableModule, TreeTableModule, NdPipe, SpinnerComponent],
    providers: [SedeCiclo1Service, SediCiclo2Service, BuildMapService, CpiaService,
        ToastrService, FormatRowTableService, DemografieService, DispersioneService, EdificiUtilsService, MappaPugliaUtilsService]
})
export class SharedModule {
}
