import {Route, RouterModule} from '@angular/router';

import {FullComponent} from './layouts/full/full.component';
import {NgModule} from '@angular/core';
import {NotFoundComponent} from './modules/authentication/404/not-found.component';
import {WorkInProgressComponent} from './shared/components/work-in-progress/work-in-progress.component';

const AppRoutes: Route[] = [

    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'pages',
    },
    {
        path: '',
        component: FullComponent,
        children: [
            {
                path: 'pages',
                loadChildren: './modules/pages/pages.module#PagesModule'
            },
            {
                path: 'access',
                loadChildren: './modules/authentication/authentication.module#AuthenticationModule'
            },
            /*   {
                   path: 'component',
                   loadChildren: './modules/component/component.module#ComponentsModule'
               }*/
            {
                path: 'comuni',
                loadChildren: './modules/comuni/comuni.module#ComuniModule'
            },
            {
                path: 'documentazione2021',
                loadChildren: './modules/documentazione2021/documentazione2021.module#Documentazione2021Module'
            },
            {path: 'modellazione', component: WorkInProgressComponent},
        ]
    },
    {path: '**', component: NotFoundComponent}

];


@NgModule({
    imports: [RouterModule.forRoot(AppRoutes, {
        useHash: false
        //enableTracing: !environment.production
    })],
    exports: [RouterModule],
})
export class AppRoutingModule {
}
