import {Component, HostListener, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {PerfectScrollbarConfigInterface} from 'ngx-perfect-scrollbar';

@Component({
    selector: 'app-full-layout',
    templateUrl: './full.component.html',
    styleUrls: ['./full.component.scss']
})
export class FullComponent implements OnInit {
    color = 'defaultdark';
    showSettings = false;
    showMinisidebar = false;
    showDarktheme = false;
    showRtl = false;
    public _opened = true;

    public innerWidth: any;

    public config: PerfectScrollbarConfigInterface = {};

    constructor(public router: Router) {
        console.log('carico');
    }

    ngOnInit() {
        console.log('url', this.router.url);
        this.handleLayout();
    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.handleLayout();
    }

    toggleSidebar() {
        this.showMinisidebar = !this.showMinisidebar;
    }

    handleLayout() {
        this.innerWidth = window.innerWidth;
        if (this.innerWidth < 1170) {
            this.showMinisidebar = true;
        } else {
            this.showMinisidebar = false;
        }
    }
}
