/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {SedeCiclo1} from '../../models/SedeCiclo1';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SedeCiclo1Service {

    private url = environment.apiUrl + '/sediciclo1';

    constructor(private http: HttpClient) {

    }

    public index(query?: string): Observable<Array<SedeCiclo1>> {
        if (query) {
            return this.http.get<Array<SedeCiclo1>>(this.url + '?filtra=' + query);
        } else {
            return this.http.get<Array<SedeCiclo1>>(this.url);
        }

    }

    public get(idSede: number): Observable<SedeCiclo1> {
        return this.http.get<SedeCiclo1>(this.url + '/' + idSede);
    }

    public cerca(codice: string, idAnno: number): Observable<Array<SedeCiclo1>> {
        return this.http.get<Array<SedeCiclo1>>(this.url + '/cerca/' + codice + '?idAnno=' + idAnno);
    }


}
