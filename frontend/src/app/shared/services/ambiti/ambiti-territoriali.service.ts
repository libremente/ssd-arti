/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AmbitoTerritoriale} from '../../models/AmbitoTerritoriale';
import {StatisticaAmbito} from '../../models/StatisticaAmbito';
import {AnnoScolastico} from '../../models/AnnoScolastico';


@Injectable({
    providedIn: 'root'
})
export class AmbitiTerritorialiService {
    private url = environment.apiUrl + '/ambiti';

    constructor(private http: HttpClient) {

    }


    public index(idAnno: AnnoScolastico): Observable<Array<AmbitoTerritoriale>> {
        return this.http.get<Array<AmbitoTerritoriale>>(this.url + '?idAnno=' + idAnno.id);

    }

    public codiciList(idAnno: AnnoScolastico): Observable<Array<AmbitoTerritoriale>> {
        return this.http.get<Array<AmbitoTerritoriale>>(this.url + '/codici?idAnno=' + idAnno.id);

    }


    public statistiche(idAnno: AnnoScolastico): Observable<Array<StatisticaAmbito>> {

        return this.http.get<Array<StatisticaAmbito>>(this.url + '/statistiche/' + '?idAnno=' + idAnno.id);

    }
}
