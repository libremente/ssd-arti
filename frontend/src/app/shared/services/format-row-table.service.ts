/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Injectable} from '@angular/core';
import {IstituzioneScolastica} from '../models/IstituzioneScolastica';
import {SedeCiclo2} from '../models/SedeCiclo2';
import {Sede} from '../models/Sede';
import {Comune} from '../models/Comune';
import {StatisticaAmbito} from '../models/StatisticaAmbito';
import {AmbitoTerritoriale} from '../models/AmbitoTerritoriale';
import * as _ from 'lodash';

@Injectable({
    providedIn: 'root'
})
export class FormatRowTableService {

    constructor() {
    }

    public organicoDiDiritto1c(ist: IstituzioneScolastica, sede: Sede, comuni: Comune[]) {


        let elem = -1;
        if (sede.comune) {
            comuni.forEach((item, index) => {
                if (item.nome.toLowerCase() === sede.comune.toLowerCase()) {
                    elem = index;
                    return;
                }

            });
        }


        const temp = {
            grado_istr: sede.tipologiaScuola,
            provincia: '',
            ambito: '',
            comune_is: ist.comuneSedeDirigenza,
            codice_is: ist.codice,
            denominazione_is: ist.denominazione,
            comune__punto_di_erog: sede.comune,
            denominazione_punto_erog: sede.denominazione,
            alunni: 'ND',
            alunni_handicap: 'ND',
            num_sez: 'ND',
            num_alunni_per_organico: 'ND'
        };

        if (elem >= 0) {
            temp.provincia = comuni[elem].provincia.toUpperCase();
            temp.ambito = comuni[elem].ambitoTerritoriale;
        }

        if (!sede.alunni) {
        } else {
            (sede.alunni.totaleAlunni) ? temp.alunni = sede.alunni.totaleAlunni.toString() : temp.alunni = 'ND';
            (sede.alunni.totaleClassi) ? temp.num_sez = sede.alunni.totaleClassi.toString() : temp.num_sez = 'ND';
            (sede.alunni.totaleAlunniDisabili) ? temp.alunni_handicap = ist.alunni.totaleAlunniDisabili.toString() : temp.alunni_handicap = 'ND';
        }

        return temp;

    }


    public organicoDiDiritto1ab(statistica: StatisticaAmbito, ambiti: AmbitoTerritoriale[], comuni: Comune[]) {


        let indiceAmbito = -1;
        if (statistica.codice) {
            indiceAmbito = _.findIndex(ambiti, {codice: statistica.codice});
        }

        let indiceComune = -1;
        if (statistica.codice) {
            indiceComune = _.findIndex(comuni, {ambitoTerritoriale: ambiti[indiceAmbito].codice});
        }


        const temp = {
            provincia: '',
            ambito: '',
            comune: '',
            cod_is: statistica.codice,
            tot_ist: 'ND',
            tot_alunni: 'ND',
            tot_alunni_disabili: 'ND',
            tot_classi: 'ND',
        };

        (comuni[indiceComune] && comuni[indiceComune].provincia) ? temp.provincia = comuni[indiceComune].provincia : temp.provincia = '';
        (ambiti[indiceAmbito] && ambiti[indiceAmbito].codice) ? temp.ambito = ambiti[indiceAmbito].codice : temp.ambito = '';
        (comuni[indiceComune] && comuni[indiceComune].nome) ? temp.comune = comuni[indiceComune].nome : temp.comune = '';


        if (!statistica) {
        } else {
            (statistica.totaleIs) ? temp.tot_ist = statistica.totaleIs.toString() : temp.tot_ist = 'ND';

            (statistica.totaleAlunniAmbito) ? temp.tot_alunni = statistica.totaleAlunniAmbito.toString() : temp.tot_alunni = 'ND';
            (statistica.totaleClassiAmbito) ? temp.tot_classi = statistica.totaleClassiAmbito.toString() : temp.tot_classi = 'ND';
            // tslint:disable-next-line:max-line-length
            (statistica.totaleAlunniDisabiliAmbito) ? temp.tot_alunni_disabili = statistica.totaleAlunniDisabiliAmbito.toString() : temp.tot_alunni_disabili = 'ND';
        }

        return temp;

    }

    public popolazione3AnniprecedentiFormat(sede: Sede, ist: IstituzioneScolastica, comuni: Comune[]) {


        let elem = -1;
        if (sede.comune) {
            comuni.forEach((item, index) => {
                if (item.nome.toLowerCase() === sede.comune.toLowerCase()) {
                    elem = index;
                    return;
                }

            });
        }


        const temp = {
            provincia: '',
            comune: '',
            codMeccanograficoOIS: ist.codice,
            denominazioneIs: ist.denominazione,
            codMeccanograficoOPuntoDiErog: sede.codice,
            denominazionePuntoDiErog: sede.denominazione,
            alunni3anniPrec: 'ND',
            alunni2AnniPrec: 'ND',
            alunni1AnnoPrec: 'ND',
            totaleIsAlunni3AnniPrec: 'ND',
            totaleIsAlunni2AnniPrec: 'ND',
            totaleIsAlunni1AnnoPrec: 'ND'
        };


        if (elem > 0) {
            temp.comune = ist.comuneSedeDirigenza;
            temp.provincia = comuni[elem].provincia;
        }

        if (!sede.alunni) {
        } else {

            temp.alunni3anniPrec = '';
            temp.alunni2AnniPrec = '';
            temp.alunni1AnnoPrec = '';

        }

        if (!ist.alunni) {
            temp.totaleIsAlunni3AnniPrec = '',
                temp.totaleIsAlunni2AnniPrec = '';
            temp.totaleIsAlunni1AnnoPrec = '';
        }

        return temp;
    }


    public provenienze(ist: IstituzioneScolastica, sede: SedeCiclo2, comuni: Comune[]) {


        let elem = -1;
        if (sede.comune) {
            comuni.forEach((item, index) => {
                if (item.nome.toLowerCase() === sede.comune.toLowerCase()) {
                    elem = index;
                    return;
                }

            });
        }

        const temp = {
            provincia: comuni[elem].provincia,
            comune: sede.comune,
            denIS: ist.denominazione,
            codicePuntoDiErog: sede.codice,
            denPuntoDiErog: sede.denominazione,
            comuneDiProvenienza: '',
            numAlunni: sede.alunni.totaleAlunni,

        };
        return temp;
    }
}
