/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {SedeCiclo2} from '../../models/SedeCiclo2';
import {IndirizzoDiStudio} from '../../models/IndirizzoDiStudio';
import {AnnoScolastico} from '../../models/AnnoScolastico';

@Injectable({
    providedIn: 'root'
})
export class SediCiclo2Service {
    private url = environment.apiUrl + '/sediciclo2';

    constructor(private http: HttpClient) {

    }

    public list(query?: string): Observable<Array<SedeCiclo2>> {
        if (query) {
            return this.http.get<Array<SedeCiclo2>>(this.url + '?filtra=' + query);
        } else {
            return this.http.get<Array<SedeCiclo2>>(this.url);
        }

    }

    public get(idSede: number): Observable<SedeCiclo2> {
        return this.http.get<SedeCiclo2>(this.url + '/' + idSede);

    }

    public cerca(codice: string, idAnno: number): Observable<Array<SedeCiclo2>> {
        return this.http.get<Array<SedeCiclo2>>(this.url + '/cerca/' + codice + '?idAnno=' + idAnno);
    }

    public getIndirizzi(): Observable<Array<IndirizzoDiStudio>> {
        return this.http.get<Array<IndirizzoDiStudio>>(this.url + '/indirizzi');
    }

    public getIndirizziConFigli(): Observable<Array<IndirizzoDiStudio>> {
        return this.http.get<Array<IndirizzoDiStudio>>(this.url + '/indirizzi?aggrega=1');
    }

    public getByIndirizzoDiStudio(codiceIndirizzo: string, idAnno: number): Observable<Array<SedeCiclo2>> {
        return this.http.get<Array<SedeCiclo2>>(this.url + '?filtra=indirizzoDiStudio&indirizzo=' + codiceIndirizzo + '&idAnno=' + idAnno);
    }

    public getByIndirizziDiStudio(codici: string[], idAnno: number): Observable<Array<SedeCiclo2>> {
        return this.http.get<Array<SedeCiclo2>>(this.url + '?filtra=indirizziDiStudio&indirizzi=' + codici + '&idAnno=' + idAnno);
    }

    public provenienze(annoScolastico: AnnoScolastico): Observable<Array<any>> {
        return this.http.get<Array<any>>(this.url + '/provenienze?idAnno=' + annoScolastico.id);
    }

    public indirizziRaggiungibiliUrl(idAnno: number, indirizzo: String): String {
        return `${this.url}/indirizziRaggiungibili?idAnno=${idAnno}&indirizzo=${indirizzo}`;
    }

}
