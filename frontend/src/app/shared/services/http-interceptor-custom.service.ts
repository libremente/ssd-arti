/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';


@Injectable()
export class HttpInterceptorCustom implements HttpInterceptor {

    constructor(private toasterService: ToastrService) {
    }


    public intercept(request: HttpRequest<any>,
                     next: HttpHandler): Observable<HttpEvent<any>> {

        request = request.clone({
            setHeaders: {
                Authorization: `Bearer `
            }
        });


        return next.handle(request).pipe(tap(evt => {
                if (!(evt instanceof HttpResponse)) {
                } else {

                    return;
                }
            }),
            catchError((err: any) => {
                if (err instanceof HttpErrorResponse) {
                    console.error('Errore contattando il server', err);
                    this.toasterService.error(err.message, 'Errore contattando il server');
                }
                return of(err);
            }));

    }
}




