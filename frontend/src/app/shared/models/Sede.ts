/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {IstituzioneScolastica} from './IstituzioneScolastica';
import {Coordinate} from './Coordinate';
import {fromLonLat} from 'ol/proj';
import {Alunni} from './Alunni';


export class Sede {
    id: number;
    codice: string;
    coordinate: Coordinate;
    codiceCatastaleComune: string;
    idIstituzioneScolastica: number;
    comune: string;
    indirizzo: string;
    denominazione: string;
    tipo: string;
    sedeDirezioneAmministrativa: boolean;
    verticalizzazione: string;
    tipologiaScuola: string;
    codiceEdificio: string;
    istituzioneScolastica: IstituzioneScolastica;
    alunni: Alunni;

    /**
     * Può essere diverso da quello delle coordinate a causa della disposizione dei punti
     */
    coordinateSuMappa: number[];
    coordinateSparpagliate: Boolean = false;


    static setCoordinateSuMappa(sede: Sede, coordinates: number[]) {
        sede.coordinateSparpagliate = (sede.coordinate.coordinates !== coordinates);
        sede.coordinateSuMappa = fromLonLat(coordinates);
    }
}
