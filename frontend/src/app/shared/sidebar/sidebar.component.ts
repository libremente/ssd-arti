/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ROUTES} from './menu-items';
import {ActivatedRoute, Router} from '@angular/router';
import {RouteInfo} from './sidebar.metadata';

declare var $: any;

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
    showMenu = '';
    showSubMenu = '';
    @Output() toggleSidebar = new EventEmitter<void>();
    public sidebarnavItems: RouteInfo[];

    // this is for the open close
    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    addActiveClass(element: any) {
        if (element === this.showSubMenu) {
            this.showSubMenu = '0';
        } else {
            this.showSubMenu = element;
        }
    }

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private route: ActivatedRoute
    ) {
    }

    private findSubMenuByPath(data: RouteInfo[], path: String): RouteInfo {
        for (let i = 0; i < data.length; i++) {
            if (data[i].path === path) {
                return data[i];

            } else if (data[i].submenu && data[i].submenu.length) {
                const result = this.findSubMenuByPath(data[i].submenu, path);
                if (result) {
                    return data[i];
                }
            }
        }
    }

    // End open close
    ngOnInit() {
        this.sidebarnavItems = ROUTES.filter(sidebarnavItem => sidebarnavItem);

        const currentSubMenu: RouteInfo = this.findSubMenuByPath(this.sidebarnavItems, this.router.url);
        if (currentSubMenu) {
            this.addExpandClass(currentSubMenu.title);
        }
    }
}
