import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TrendChartLineComponent} from './trend-chart-line.component';

describe('TrendChartLineComponent', () => {
    let component: TrendChartLineComponent;
    let fixture: ComponentFixture<TrendChartLineComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TrendChartLineComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TrendChartLineComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
