import {Component, Input, OnInit} from '@angular/core';
import {ChartDataSets, ChartPoint} from 'chart.js';
import {Label} from 'ng2-charts';


@Component({
    selector: 'app-trend-chart-line',
    templateUrl: './trend-chart-line.component.html',
    styleUrls: ['./trend-chart-line.component.css']
})


export class TrendChartLineComponent implements OnInit {


    @Input()
    dataChart: any;


    constructor() {
    }


    public lineChartOptions1: any = {
        scales: {
            yAxes: [

                {
                    ticks: {
                        beginAtZero: true
                    },
                    gridLines: {
                        color: 'rgba(0, 0, 0, 0.1)'
                    }
                }
            ],
            xAxes: [

                {

                    gridLines: {
                        color: 'transparent'
                    }
                }
            ]
        },
        lineTension: 10,
        responsive: true,
        maintainAspectRatio: false,
        elements: {line: {tension: 0}}
    };


    public lineChartColors1: Array<any> = [
        {
            // grey
            backgroundColor: 'rgba(6,215,156,0.1)',
            borderColor: 'rgba(6,215,156,1)',
            pointBackgroundColor: 'rgba(6,215,156,1)',
            pointBorderColor: '#fff',

            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(6,215,156,0.5)'
        },
        {
            // dark grey
            backgroundColor: 'rgba(57,139,247,0.1)',
            borderColor: 'rgba(57,139,247,1)',
            pointBackgroundColor: 'rgba(57,139,247,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(57,139,247,0.5)'
        }
    ];
    public lineChartLegend1 = true;
    public lineChartType1 = 'line';
    public lineChartData1: ChartDataSets [] = [];

    public lineChartLabels1: Label[] = [];


    ngOnInit() {


        const sort = function (series) {
            series.sort((obj1, obj2) => {
                    const anno1 = obj1.label.split('/')[0];
                    const anno2 = obj2.label.split('/')[0];

                    if (anno1 > anno2) {
                        return 1;
                    }
                    if (anno1 < anno2) {
                        return -1;
                    }

                    return 0;
                }
            );
        };


        let dataPrec: ChartPoint[] = [];
        let dataProie: ChartPoint[] = [];
        if (this.dataChart) {
            // bisogna assicurare che i dati siano ordinati per label
            sort(this.dataChart.precedenti);

            if (this.dataChart.precedenti.length > 0) {
                this.dataChart.precedenti.forEach(item => {
                    dataPrec.push({
                        x: item.label, y: item.alunni
                    });
                    this.lineChartLabels1.push(item.label);
                });
            }
            if (this.dataChart.proiezione.length > 0) {
                sort(this.dataChart.proiezione);
                this.dataChart.proiezione.forEach(item => {
                    dataProie.push({x: item.label, y: item.alunni});
                    this.lineChartLabels1.push(item.label);
                });
            }


            this.lineChartData1.push({data: dataPrec, label: 'Storico'});
            this.lineChartData1.push({data: dataProie, label: 'Proiezione'});
        }


    }

}
