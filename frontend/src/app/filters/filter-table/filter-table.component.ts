import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Provincia} from '../../shared/models/Provincia';
import {Comune} from '../../shared/models/Comune';
import {AmbitoTerritoriale} from '../../shared/models/AmbitoTerritoriale';
import {ComuniService} from '../../modules/comuni/comuni.service';
import {AmbitiTerritorialiService} from '../../shared/services/ambiti/ambiti-territoriali.service';

@Component({
    selector: 'app-filter-table',
    templateUrl: './filter-table.component.html',
    styleUrls: ['./filter-table.component.css']
})
export class FilterTableComponent implements OnInit {

    public denominazioni: string[];
    public gradi: string[];
    public province: Provincia[];
    public comuni: Comune[];
    public ambiti: AmbitoTerritoriale[];

    public gradoSelected: string;
    public provinciaSelected: string;
    public ambitoSelected: string;
    public comuneSelected: string;
    public searchText: string;

    public denominazioneSelected: string;

    @Input()
    showGradi = false;

    @Input()
    showProvince = false;

    @Input()
    showAmbiti = false;

    @Input()
    showDenominazioni = false;

    @Input()
    showComuni = false;

    @Input()
    showRicerca = false;

    @Input()
    loader = false;

    @Input()
    rows = [];


    @Output()
    rowsFiltered: EventEmitter<Array<any>>;


    constructor(private comuniService: ComuniService, private ambitiService: AmbitiTerritorialiService) {
    }

    ngOnInit() {
        this.rowsFiltered = new EventEmitter();
    }

    filter() {

        if (this.showGradi) {
            this.filterByGradi();
        }

        if (this.showProvince) {
            this.filterByProvincia();
        }


        if (this.showAmbiti) {
            this.filterByAmbiti();
        }

        if (this.showDenominazioni) {
            this.filterByDenominazioni();
        }

        if (this.showComuni) {
            this.filterByComuni();
        }

    }


    filterByGradi() {
        this.rows = this.rows.filter(item => item.grado === this.gradoSelected);

    }

    filterByProvincia() {
        this.rows = this.rows.filter(item => item.provincia === this.provinciaSelected);

    }

    filterByAmbiti() {
        this.rows = this.rows.filter(item => item.ambito === this.ambitoSelected);

    }

    filterByComuni() {
        this.rows = this.rows.filter(item => item.comune === this.comuneSelected);

    }

    filterByDenominazioni() {
        this.rows = this.rows.filter(item => item.denominazioni === this.denominazioneSelected);

    }

    updateFilterWithText() {


    }


}
