/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {MapActionTypes, ModificaFiltri} from './map-action.actions';
import {tap} from 'rxjs/operators';


@Injectable()
export class MapEffects {

    @Effect({dispatch: false})
    storeDataMap$ = this.actions$.pipe(
        ofType<ModificaFiltri>(MapActionTypes.ModificaFiltri),
        tap(action => {
            localStorage.setItem('map-state', JSON.stringify(action.payload));
        })
    );


    constructor(private actions$: Actions) {
    }

}
