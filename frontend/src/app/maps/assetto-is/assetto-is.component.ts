/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {AfterViewInit, Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import OlMap from 'ol/Map';
import OlTileLayer from 'ol/layer/Tile';
import OlView from 'ol/View';
import Feature from 'ol/Feature.js';

import {Control} from 'ol/control.js';
import {Vector} from 'ol/source.js';
import VectorLayer from 'ol/layer/Vector.js';
import {fromLonLat} from 'ol/proj.js';
import {Extent} from 'ol';
import LineString from 'ol/geom/LineString.js';
import {Router} from '@angular/router';
import Overlay from 'ol/Overlay.js';
import {IstituzioneScolastica} from '../../shared/models/IstituzioneScolastica';
import MultiLineString from 'ol/geom/MultiLineString';
import {ISService} from '../../shared/services/istituzioniScolastiche/is.service';
import Point from 'ol/geom/Point';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SedeAnalisiDatiModalComponent} from '../modal-maps/sede-analisi-dati-modal/sede-analisi-dati-modal.component';
import {BuildMapService} from '../mappa-puglia/build-map/build-map.service';
import {Sede} from '../../shared/models/Sede';
import {PuntiUnivoci} from '../../shared/helper/punti-univoci';
import {delay} from 'q';
import {AnnoScolasticoService} from '../../modules/anno-scolastico/anno-scolastico.service';
import {AnnoScolastico} from '../../shared/models/AnnoScolastico';

import {Subscription} from 'rxjs';
import {SedeCiclo2} from '../../shared/models/SedeCiclo2';
import {Store} from '@ngrx/store';
import {AppState} from '../../reducers';
import {ModificaFiltri} from './map-action.actions';
import {InfoSedi} from '../mappa-puglia/Utils';
import {EdificiService} from '../../shared/services/edifici/edifici.service';
import {FiltraConfiniMap} from '../../shared/models/confini/FiltraConfiniMap';
import {ConfiniService} from '../../shared/services/confini/confini.service';
import {FiltraTipoIsOrganico} from '../../shared/models/confini/FiltraTipoIsOrganico';
import {FiltraSedi} from '../../shared/models/sedi/FiltraSedi';
import {OpzioniMap} from '../../shared/models/opzioni-map/OpzioniMap';
import {EdificiUtilsService} from '../../shared/services/edifici/edifici-utils.service';
import {LegendField, LineLegendField, PointLegendField} from '../mappa-puglia/map-legenda/model';
import {SedeCiclo1} from '../../shared/models/SedeCiclo1';

@Component({
    selector: 'app-assetto-is',
    templateUrl: './assetto-is.component.html',
    styleUrls: ['./assetto-is.component.scss']
})


export class AssettoISComponent implements OnInit, AfterViewInit, OnDestroy {

    private static PREFISSO_IS = 'IS_';
    private static PREFISSO_SEDE = 'SEDE_';


    private custom: any = document.createElement('div');
    private confiniPugliaLayer = this.buildMapsService.confiniPugliaLayer;
    private confiniAmbitiLayer = this.buildMapsService.confiniAmbitiLayer;
    private confiniDistrettiLayer = this.buildMapsService.confiniDistrettiLayer;
    private confiniProvinceLayer = this.buildMapsService.confiniProvinceLayer;
    private confiniComuniLayer = this.buildMapsService.confiniComuniLayer;
    private confiniUnioneComuniLayer = this.buildMapsService.confiniUnioneComuni;
    private pointsUtility = new PuntiUnivoci();
    private isList1: IstituzioneScolastica[] = null;
    private sediCiclo1Layer: Vector = null;
    private linkLayer1: Vector = null;
    // rete scolastica 2
    private sediCiclo2Layer: Vector = null;
    private isList2: IstituzioneScolastica[] = null;
    private linkLayer2: Vector = null;
    private edificiInutilizzatiLayer: VectorLayer = null;
    private spostamentiLayer: Vector = this.buildMapsService.spostamentiLayer;
    private lastExtent: Extent;
    private lastZoom: number;
    private baseLayer: OlTileLayer;
    private view: OlView;
    private openPopoverTimer: any = null;
    private overlay: Overlay;
    private annoScolasticoSubscription: Subscription;
    private codiciIsSottoDimensionate: Array<number> = Array();
    private codiciIsSovraDimensionate: Array<number> = Array();
    private spostamentiStyle = this.buildMapsService.getSpostamentiStyle();

    public opzioniMap: OpzioniMap = new OpzioniMap();
    public filtraSedi: FiltraSedi = new FiltraSedi();
    public showNav = true;
    public infoSedi = [InfoSedi.TUTTI, InfoSedi.NOMI, InfoSedi.OD_OF, InfoSedi.DISATTIVA];
    public infoSediSelected: string;
    public showTabella = false;
    public showConfini: FiltraConfiniMap;
    public showTipoIsOrganico: FiltraTipoIsOrganico;
    public selectedIs: IstituzioneScolastica = null;
    public selectedSede: Sede = null;
    public map: OlMap;
    legendFields: LegendField[] = [];


    constructor(private modalService: NgbModal,
                private isService: ISService,
                private buildMapsService: BuildMapService,
                private router: Router,
                private confiniService: ConfiniService,
                private annoScolasticoService: AnnoScolasticoService,
                private store: Store<AppState>,
                private edificiService: EdificiService,
                private edificiUtilsService: EdificiUtilsService) {

        this.showConfini = new FiltraConfiniMap();
        this.showTipoIsOrganico = new FiltraTipoIsOrganico();
        this.showTabella = false;
        this.initLegend();
    }

    private static getISId(string: string): number | undefined {

        if (string.startsWith(this.PREFISSO_IS)) {

            console.log(string.substr(3));
            return parseInt(string.substr(3), 10);
        }
        return undefined;
    }

    private static getSedeId(string: string): string | undefined {
        if (string.startsWith(this.PREFISSO_SEDE)) {
            return string.substr(5);
        }
        return undefined;
    }

    ngOnInit() {


        const component = this;

        this.baseLayer = this.buildMapsService.configBaseLayer();

        const container = document.getElementById('popup');
        this.overlay = this.buildMapsService.configOverlay(container);

        const layers = [this.baseLayer];

        // setting debug layer
        if (false) {
            layers.push(this.buildMapsService.debugLayerMode());
        }

        this.custom.className = 'custom-div ol-zoom  ol-unselectable  ol-control';

        const toggleNav = document.createElement('button');
        toggleNav.innerHTML = 'N';

        this.custom.appendChild(toggleNav);
        toggleNav.addEventListener('click', function () {
            component.toggleNav();

        });

        const toggleLegenda = document.createElement('button');
        toggleLegenda.innerHTML = 'L';


        this.custom.appendChild(toggleLegenda);
        toggleLegenda.addEventListener('click', function () {
            component.toggleLegenda();
        });

        this.store.subscribe(state => {
                this.opzioniMap.showLegend = state.mapDinamicaState.opzioni.mostraLegenda;
                this.opzioniMap.showSpostamenti = state.mapDinamicaState.opzioni.mostraSpostamenti;
                this.opzioniMap.applicaLeggibilita = state.mapDinamicaState.opzioni.filtroLeggibilita;
                this.opzioniMap.mostraEdificiInutilizzati = state.mapDinamicaState.opzioni.mostraEdificiInutilizzati;

                this.filtraSedi.filtraSediCiclo1.showOnlySediCiclo1 = state.mapDinamicaState.filterCiclo.showOnlySediCiclo1;
                this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1AA = state.mapDinamicaState.filterCiclo.showOnlyCiclo1AA;
                this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1EE = state.mapDinamicaState.filterCiclo.showOnlyCiclo1EE;
                this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1MM = state.mapDinamicaState.filterCiclo.showOnlyCiclo1MM;

                this.filtraSedi.filtraSediCiclo2.showOnlySediCiclo2 = state.mapDinamicaState.filterCiclo.showOnlySediCiclo2;
                this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2LI = state.mapDinamicaState.filterCiclo.showOnlyCiclo2LI;
                this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IP = state.mapDinamicaState.filterCiclo.showOnlyCiclo2IP;
                this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IT = state.mapDinamicaState.filterCiclo.showOnlyCiclo2IT;

                this.showTipoIsOrganico.showOnlySovraDimensionate = state.mapDinamicaState.dimensionate.showOnlySovraDimensionate;
                this.showTipoIsOrganico.showOnlySottoDimensionate = state.mapDinamicaState.dimensionate.showOnlySottoDimensionate;
                this.infoSediSelected = state.mapDinamicaState.infoSedi;
                this.lastZoom = state.mapDinamicaState.lastZoom;
                this.lastExtent = state.mapDinamicaState.lastExtent;


                this.view = this.buildMapsService.configDefaultView(this.lastZoom, this.lastExtent, state.mapDinamicaState.center);
                if (this.opzioniMap.mostraEdificiInutilizzati) {
                    this.caricaLayerEdificiInutilizzati();
                }
            }
        );


        this.map = this.buildMapsService.configDefaultMap(layers, this.overlay, this.view);
        this.map.controls.push(new Control({element: this.custom}));
        this.map.on('moveend', (event) => this.onMoveEnd(event));
        // this.map.on('pointermove', (event) => component.onPointerMove(event));
        this.map.on('click', (event) => component.onMapClicked(event));


        this.map.addLayer(this.spostamentiLayer);


    }

    goInIstituzioniPage() {
        this.router.navigate(['pages/analisi'], {
            queryParams: {
                ist: this.selectedIs.codice,
                anno: this.annoScolasticoService.getAnnoScolastico().id
            }
        });

    }

    ngAfterViewInit(): void {
        this.map.setTarget('map');

        if (this.opzioniMap.applicaLeggibilita) {
            // abilito di default il layer di leggibilità
            this.baseLayer.addEventListener('postcompose', BuildMapService.applicaFiltroLeggibilita);
            this.map.render();
        }
        this.annoScolasticoSubscription = this.annoScolasticoService.getAnnoScolasticoObservable().subscribe(annoScolastico => {
            if (annoScolastico != null) {
                console.log('anno scolastico selezionato ', annoScolastico);
                this.clearFeatures();
                this.caricaTutteLeSedi(annoScolastico);
            }
        });

        this.reloadSovradimensionate();

    }

    ngOnDestroy(): void {
        this.aggiornaOpzioni();
        this.annoScolasticoSubscription.unsubscribe();
        this.buildMapsService.freeStyles();
    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        setTimeout(() => {
            this.map.updateSize();
        }, 20);
    }

    closePopup() {
        this.selectedSede = null;
        this.selectedIs = null;
        this.overlay.setPosition(undefined);
    }

    public caricaSediCiclo2(annoScolastico: AnnoScolastico) {
        this.isService.ciclo2(annoScolastico).subscribe(isList => {
            this.isList2 = isList;
            this.popolaReteScolasticaCiclo2();
            this.popolaLayerSpostamenti2();
        });
    }

    public caricaLinkTraSediESediAmministrative1(isList: Array<IstituzioneScolastica>) {

        let i = 0;
        const features = [];

        isList.forEach(is => {
            let sedeAmministrativa = is.sediCiclo1.find(element =>
                element.sedeDirezioneAmministrativa === true
            );
            i++;
            if (sedeAmministrativa === undefined) {
                console.log('Direzione amministrativa non trovata per ', is);
                sedeAmministrativa = is.sediCiclo1[0];
            }
            const isMultiLineString = new MultiLineString([]);
            is.sediCiclo1.forEach((sede) => {
                if (sede.id !== sedeAmministrativa.id &&
                    (!this.opzioniMap.showSpostamenti ||
                        sedeAmministrativa.coordinate.coordinates.toString() !== sede.coordinate.coordinates.toString())) {
                    // in caso di coordinate uguali viene mostrato un tratteggio
                    isMultiLineString.appendLineString(
                        new LineString([sedeAmministrativa.coordinateSuMappa, sede.coordinateSuMappa])
                    );
                }
            });
            const feature = new Feature({
                geometry: isMultiLineString
            });
            feature.setStyle(() => {

                let visualizza = true;
                if (this.showTipoIsOrganico.showOnlySovraDimensionate) {
                    visualizza = this.codiciIsSovraDimensionate.find(x => x === is.id) !== undefined;
                } else if (this.showTipoIsOrganico.showOnlySottoDimensionate) {
                    visualizza = this.codiciIsSottoDimensionate.find(x => x === is.id) !== undefined;
                }

                if (!visualizza ||
                    (!this.filtraSedi.filtraSediCiclo1.showOnlySediCiclo1 && this.filtraSedi.filtraSediCiclo2.showOnlySediCiclo2)) {
                    return null;
                }
                return this.buildMapsService.getLinkStyle(i);
            });
            feature.setId(AssettoISComponent.PREFISSO_IS + sedeAmministrativa.idIstituzioneScolastica);
            features.push(feature);
        });

        this.linkLayer1 = new VectorLayer({
            source: new Vector({
                features: features,
            })
        });
        this.linkLayer1.setZIndex(10);
        this.map.addLayer(this.linkLayer1);
    }

    public caricaLinkTraSediESediAmministrative2(isList2: Array<IstituzioneScolastica>) {


        let i = 0;
        const features = [];

        isList2.forEach(is => {
            let sedeAmministrativa = is.sediCiclo2.find(element =>
                element.sedeDirezioneAmministrativa === true
            );
            i++;
            if (is.sediCiclo2.length > 1) {
                if (sedeAmministrativa === undefined) {
                    console.log('Direzione amministrativa non trovata per ', is);
                    sedeAmministrativa = is.sediCiclo2[0];

                }
                const isMultiLineString = new MultiLineString([]);
                is.sediCiclo2.forEach((sede) => {
                    if (sede.id !== sedeAmministrativa.id &&
                        (!this.opzioniMap.showSpostamenti ||
                            sedeAmministrativa.coordinate.coordinates.toString() !== sede.coordinate.coordinates.toString())) {
                        // in caso di coordinate uguali viene mostrato un tratteggio
                        isMultiLineString.appendLineString(
                            new LineString([
                                sedeAmministrativa.coordinateSuMappa,
                                sede.coordinateSuMappa])
                        );

                    }
                });
                const feature = new Feature({
                    geometry: isMultiLineString
                });
                feature.setStyle(() => {

                    let visualizza = true;
                    if (this.showTipoIsOrganico.showOnlySovraDimensionate) {
                        visualizza = this.codiciIsSovraDimensionate.find(x => x === is.id) !== undefined;
                    } else if (this.showTipoIsOrganico.showOnlySottoDimensionate) {
                        visualizza = this.codiciIsSottoDimensionate.find(x => x === is.id) !== undefined;
                    }
                    if (!visualizza ||
                        (!this.filtraSedi.filtraSediCiclo2.showOnlySediCiclo2 && this.filtraSedi.filtraSediCiclo1.showOnlySediCiclo1)) {
                        return null;
                    }

                    return this.buildMapsService.getLinkStyle(i);
                });
                feature.setId(AssettoISComponent.PREFISSO_IS + sedeAmministrativa.id);
                features.push(feature);
            }
        });

        this.linkLayer2 = new VectorLayer({
            source: new Vector({
                features: features,
            })
        });
        this.linkLayer2.setZIndex(10);
        this.map.addLayer(this.linkLayer2);
    }


    public toggleNav() {
        this.showNav = !this.showNav;
    }

    public updateEtichetteSelected(event) {
        this.infoSediSelected = event;
        this.sediCiclo1Layer.changed();
        this.sediCiclo2Layer.changed();
    }

    public filterConfini(event: FiltraConfiniMap) {
        this.showConfini = event;
        this.resetConfini();
        this.reloadConfini();
    }


    updateOpzioniMap(event: OpzioniMap) {
        this.opzioniMap = event;
        this.reloadFiltroLeggibilita();
        this.reloadSpostamenti();
        this.reloadEdificiInutilizzati();
    }


    private caricaLayerEdificiInutilizzati() {


        this.edificiService.edificiInutilizzati(null).subscribe(edifici => {
            console.log('carico gli indirizzi', edifici);
            if (edifici && edifici.length > 0) {
                this.edificiInutilizzatiLayer = this.edificiUtilsService.buildLayerEdificiInutilizzati(edifici);
                this.map.addLayer(this.edificiInutilizzatiLayer);
            }
        });
    }


    private removeEdificiInutilizzati() {
        if (this.edificiInutilizzatiLayer) {
            console.log('rimuovo edifici inutilizzati');
            this.map.removeLayer(this.edificiInutilizzatiLayer);
        }
    }

    reloadEdificiInutilizzati() {
        this.removeEdificiInutilizzati();
        if (this.opzioniMap.mostraEdificiInutilizzati) {
            this.caricaLayerEdificiInutilizzati();
        }


    }


    public reloadFiltroLeggibilita() {
        if (this.opzioniMap.applicaLeggibilita) {
            this.baseLayer.addEventListener('postcompose', BuildMapService.applicaFiltroLeggibilita);
        } else {
            this.baseLayer.removeEventListener('postcompose', BuildMapService.applicaFiltroLeggibilita);
        }
        this.map.render();
    }

    public reloadSpostamenti() {

        (this.opzioniMap.showSpostamenti) ? this.map.removeLayer(this.spostamentiLayer) : this.map.addLayer(this.spostamentiLayer);
        this.linkLayer1.getSource().clear();
        this.linkLayer2.getSource().clear();
        this.caricaLinkTraSediESediAmministrative1(this.isList1);
        this.caricaLinkTraSediESediAmministrative2(this.isList2);

    }


    public toggleShowTabella(element: any) {
        this.showTabella = !this.showTabella;
        if (this.showTabella) {
            element.scrollIntoView();
        }
    }


    public updateShowLegenda(showLegenda: boolean) {
        this.opzioniMap.showLegend = showLegenda;
    }

    public toggleLegenda() {
        this.opzioniMap.showLegend = !this.opzioniMap.showLegend;
    }


    private reloadSottoDimensionate() {
        if (this.showTipoIsOrganico.showOnlySottoDimensionate) {
            this.isService.sottoDimensionate(this.annoScolasticoService.getAnnoScolastico())
                .subscribe(sottoDimensionate => {
                    sottoDimensionate.forEach((is) => {
                        this.codiciIsSottoDimensionate.push(is.id);
                    });
                    this.reloadReteCartografica();
                });
        }

    }

    private reloadSovradimensionate() {
        if (this.showTipoIsOrganico.showOnlySovraDimensionate) {
            this.isService.sovraDimensionate(this.annoScolasticoService.getAnnoScolastico()).subscribe(
                sovradimensionate => {
                    sovradimensionate.forEach((is) => {
                        this.codiciIsSovraDimensionate.push(is.id);
                    });
                    this.reloadReteCartografica();
                });
        } else {
            this.reloadSottoDimensionate();
        }
    }


    reloadFilterTipoIsOrganico(showTipoIsOrganico) {
        this.showTipoIsOrganico = showTipoIsOrganico;
        (!this.showTipoIsOrganico.showOnlySovraDimensionate && !this.showTipoIsOrganico.showOnlySottoDimensionate)
            ? this.reloadReteCartografica() : this.reloadSovradimensionate();

    }


    public dettaglioSedeCiclo1(idSede: number) {
        const activeModal = this.modalService.open(SedeAnalisiDatiModalComponent, {size: 'lg'});
        (activeModal.componentInstance as SedeAnalisiDatiModalComponent).loadDataSedeCiclo1(idSede);
    }

    public dettaglioSedeCiclo2(idSede: number) {
        const activeModal = this.modalService.open(SedeAnalisiDatiModalComponent, {size: 'lg'});
        (activeModal.componentInstance as SedeAnalisiDatiModalComponent).loadDataSedeCiclo2(idSede);
    }

    apriPopupSede(sede: Sede) {
        const featureId = AssettoISComponent.PREFISSO_SEDE + sede.codice;
        this.openPopupByFeatureId(featureId, sede.coordinateSuMappa);

    }


    /**
     * Tengo traccia del livello di zoom e di extent durante un movimento
     * @param evt
     */
    private onMoveEnd(evt) {
        const map = evt.map;
        this.moveOnPositionFocused(map);
    }

    private moveOnPositionFocused(map) {
        const currentZoom = map.getView().getZoom();
        this.lastExtent = map.getView().calculateExtent(map.getSize());
        console.log('last Ext = ', this.lastExtent, 'last zoom ', this.lastZoom);
        if (this.selectedSede && this.lastZoom !== currentZoom) {
            this.overlay.setPosition(fromLonLat(this.selectedSede.coordinate.coordinates));
        }
        this.lastZoom = currentZoom;
    }

    private onMapClicked(evt) {
        if (evt.dragging) {
            return;
        }

        const feature = BuildMapService.getFeatureFromEvt(evt);

        if (feature) {
            const id = feature.getId();
            if (!id) {
                // id non settato
                return;
            }
            this.openPopupByFeatureId(id, evt.coordinate);
        } else {
            this.closePopup();
        }
    }

    private onPointerMove(evt) {

        if (evt.dragging) {
            return;
        }

        const feature = BuildMapService.getFeatureFromEvt(evt);

        clearTimeout(this.openPopoverTimer);
        if (feature) {
            const id = feature.getId();
            if (!id) {
                // id non settato
                return;
            }
            this.openPopoverTimer = setTimeout(() => {
                this.openPopupByFeatureId(id, evt.coordinate);
            }, 200);

        } else {
            // disattivato nascondi popup
            // this.closePopup()
        }
    }

    private openPopupByFeatureId(id: string, coordinate: any) {

        const component = this;

        const idIs = AssettoISComponent.getISId(id);
        const idSede = AssettoISComponent.getSedeId(id);

        let istituzioneScolasticaSelezionata: IstituzioneScolastica;
        let sedeSelezionata: Sede;

        if (idIs) {

            if (component.isList1) {
                istituzioneScolasticaSelezionata = component.isList1.filter(x => x.id === idIs)[0];
            }
            if (!istituzioneScolasticaSelezionata && component.isList2) {
                istituzioneScolasticaSelezionata = component.isList2.filter(x => x.id === idIs)[0];
            }

        } else if (idSede) {
            // popup su sede
            if (component.isList1) {

                istituzioneScolasticaSelezionata = component.isList1.filter(function (item) {
                    return item.sediCiclo1.find(x => x.codice === idSede);
                })[0];

                if (istituzioneScolasticaSelezionata) {
                    sedeSelezionata = istituzioneScolasticaSelezionata.sediCiclo1.filter(x => x.codice === idSede)[0];
                    if (!sedeSelezionata) {
                        sedeSelezionata = istituzioneScolasticaSelezionata.sediCiclo2.filter(x => x.codice === idSede)[0];
                    }
                }
            }

            if (!istituzioneScolasticaSelezionata && component.isList2) {

                istituzioneScolasticaSelezionata = component.isList2.filter(function (item) {
                    return item.sediCiclo2.find(x => x.codice === idSede);
                })[0];

                if (istituzioneScolasticaSelezionata) {
                    sedeSelezionata = istituzioneScolasticaSelezionata.sediCiclo2.filter(x => x.codice === idSede)[0];
                }
            }
        } else {
            // popup su altro
            return;
        }
        if (istituzioneScolasticaSelezionata) {
            // Se l'istituzione scolastica è stata trovata, mostro il popup
            // ma solo se diverso da quello attualmente mostrato

            console.log('istituzione selezionata', istituzioneScolasticaSelezionata);

            // if (istituzioneScolasticaSelezionata !== component.selectedIs) {
            component.selectedIs = istituzioneScolasticaSelezionata;
            component.selectedSede = sedeSelezionata;
            // delay necessario per far funzionare il posizionamento del popup
            delay(50).done(() => {
                component.overlay.setPosition(coordinate);
            });

            // }
        }
    }


    private resetPuntiUnivoci() {
        this.pointsUtility.resetPunti();
        this.spostamentiLayer.getSource().clear();
    }

    private clearFeatures() {
        this.resetPuntiUnivoci();
        [
            this.sediCiclo1Layer,
            this.sediCiclo2Layer,
            this.linkLayer1,
            this.linkLayer2,
            this.spostamentiLayer
        ].forEach(
            (vector) => {
                if (vector != null) {
                    vector.getSource().clear();
                }
            }
        );

    }

    private popolaLayerSpostamenti() {

        const features: Array<Feature> = [];

        this.isList1.forEach((is) => {

            is.sediCiclo1.forEach((sede) => {
                if (sede.coordinateSparpagliate) {
                    // console.log(sede);
                    const feature = new Feature({
                        geometry: new LineString([sede.coordinateSuMappa, fromLonLat(sede.coordinate.coordinates)])
                    });
                    feature.setStyle((feature, resolution) => {
                        const visualizza = this.visualizzaSedeCiclo1(sede);
                        if (visualizza) {
                            return this.spostamentiStyle;
                        } else {
                            return null;
                        }

                    });
                    features.push(feature);
                }
            });

        });

        this.spostamentiLayer.getSource().addFeatures(features);

    }

    private popolaLayerSpostamenti2() {

        const features = [];

        this.isList2.forEach((is) => {

            is.sediCiclo2.forEach((sede) => {
                if (sede.coordinateSparpagliate) {
                    // console.log(sede);

                    const feature = new Feature({
                        geometry: new LineString([sede.coordinateSuMappa, fromLonLat(sede.coordinate.coordinates)])
                    });
                    feature.setStyle((feature, resolution) => {
                        const visualizza = this.visualizzaSedeCiclo2(sede);
                        if (visualizza) {
                            return this.spostamentiStyle;
                        } else {
                            return null;
                        }

                    });
                    features.push(feature);
                }
            });

        });

        this.spostamentiLayer.getSource().addFeatures(features);

    }


    public caricaTutteLeSedi(annoScolastico) {
        if (annoScolastico != null) {

            this.caricaSediCiclo1(annoScolastico);
            this.caricaSediCiclo2(annoScolastico);
        } else {
            console.error('annoScolastico null');
        }
    }


    private caricaSediCiclo1(annoScolastico: AnnoScolastico) {
        this.isService.ciclo1(annoScolastico).subscribe(isList => {

            this.isList1 = isList;
            this.popolaReteScolasticaCiclo1();
            this.popolaLayerSpostamenti();
        });
    }

    /**
     * Valuta se visaulizzare la sede passata in input
     * @param sede
     */
    private visualizzaSedeCiclo1(sede: SedeCiclo1): boolean {
        let visualizza = true;
        if (this.showTipoIsOrganico.showOnlySovraDimensionate) {
            visualizza = this.codiciIsSovraDimensionate.find(x => x === sede.idIstituzioneScolastica) !== undefined;
        } else if (this.showTipoIsOrganico.showOnlySottoDimensionate) {
            visualizza = this.codiciIsSottoDimensionate.find(x => x === sede.idIstituzioneScolastica) !== undefined;
        }
        const performFiltering = this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1AA
            || this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1EE || this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1MM;
        visualizza = visualizza && !this.filtraSedi.filtraSediCiclo2.showOnlySediCiclo2 && (!performFiltering ||
            (this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1AA && sede.tipologiaScuola === 'AA') ||
            (this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1EE && sede.tipologiaScuola === 'EE') ||
            (this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1MM && sede.tipologiaScuola === 'MM'));
        return visualizza;
    }

    private visualizzaSedeCiclo2(sede: SedeCiclo2): boolean {
        const performFiltering = this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2LI
            || this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IP || this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IT;
        let visualizza = true;

        if (this.showTipoIsOrganico.showOnlySovraDimensionate) {
            visualizza = this.codiciIsSovraDimensionate.find(x => x === sede.idIstituzioneScolastica) !== undefined;
        } else if (this.showTipoIsOrganico.showOnlySottoDimensionate) {
            visualizza = this.codiciIsSottoDimensionate.find(x => x === sede.idIstituzioneScolastica) !== undefined;
        }

        if (sede.indirizziDiStudio.length > 0) {
            visualizza = visualizza && visualizza && !this.filtraSedi.filtraSediCiclo1.showOnlySediCiclo1 && (!performFiltering ||
                (this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IP && sede.indirizziDiStudio[0].codice.substr(0, 2) === 'IP') ||
                (this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IT && sede.indirizziDiStudio[0].codice.substr(0, 2) === 'IT') ||
                (this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2LI && sede.indirizziDiStudio[0].codice.substr(0, 2) === 'LI'));
        } else {
            console.error('Punto di erogazione senza indirizzi ', sede);
        }
        return visualizza;
    }

    private popolaReteScolasticaCiclo1() {
        const start = performance.now();

        let i = 0;
        let isList: IstituzioneScolastica[];
        if (this.sediCiclo1Layer != null) {
            this.sediCiclo1Layer.getSource().clear();
            this.linkLayer1.getSource().clear();
        }

        isList = this.isList1;

        const features = [];
        isList.forEach((is) => {
            BuildMapService.ordinaSedi(is.sediCiclo1);
            is.sediCiclo1.forEach((sede) => {
                if (sede.coordinate) {

                    const coordinates = this.pointsUtility.assicuraPuntoUnivoco(sede.coordinate.coordinates);
                    Sede.setCoordinateSuMappa(sede, coordinates);
                    const p = new Point(sede.coordinateSuMappa);
                    const feature1 = new Feature({geometry: p, sede: sede});
                    feature1.setId(AssettoISComponent.PREFISSO_SEDE + sede.codice);

                    feature1.setStyle((feature, resolution) => {
                        const displayText = resolution <= this.buildMapsService.getMinResolutionForText();
                        let label: String = '';
                        if (displayText) {
                            const labelODOF = BuildMapService.getODOF(is, sede);

                            switch (this.infoSediSelected) {
                                case InfoSedi.TUTTI:
                                    label = sede.denominazione + '\n' + sede.tipologiaScuola + ' ' + labelODOF;
                                    break;
                                case InfoSedi.NOMI:
                                    label = sede.denominazione;
                                    break;
                                case InfoSedi.OD_OF:
                                    label = sede.tipologiaScuola + ' ' + labelODOF;
                                    break;
                                case InfoSedi.DISATTIVA:
                                    label = '';
                                    break;

                            }
                        }
                        const visualizza = this.visualizzaSedeCiclo1(sede);
                        return this.buildMapsService.getStyleSede(feature, sede, displayText, label, visualizza);
                    });
                    feature1.setProperties({
                        'codice': sede.codice,
                        'tipologiaScuola': sede.tipologiaScuola
                    });
                    features[i++] = feature1;

                }
            });
        });
        this.sediCiclo1Layer = new VectorLayer({
            source: new Vector({
                features: features,
            }),
        });
        this.map.addLayer(this.sediCiclo1Layer);
        this.sediCiclo1Layer.setZIndex(12);

        const t1 = performance.now();
        console.log('rete Ciclo 1: ' + features.length + ' sedi caricate in ' + (t1 - start) + 'ms');
        this.caricaLinkTraSediESediAmministrative1(isList);
        const t2 = performance.now();
        console.log('rete Ciclo 1: link in ' + (t2 - t1) + 'ms');
    }

    private popolaReteScolasticaCiclo2() {

        const start = performance.now();

        let i = 0;
        let isList2: IstituzioneScolastica[];
        if (this.sediCiclo2Layer != null) {
            this.sediCiclo2Layer.getSource().clear();
            this.linkLayer2.getSource().clear();
        }

        isList2 = this.isList2;

        const features2 = [];
        isList2.forEach((is) => {
            BuildMapService.ordinaSedi(is.sediCiclo2);
            is.sediCiclo2.forEach((sede: SedeCiclo2) => {
                if (sede.coordinate) {

                    const coordinates = this.pointsUtility.assicuraPuntoUnivoco(sede.coordinate.coordinates);
                    Sede.setCoordinateSuMappa(sede, coordinates);
                    const p = new Point(sede.coordinateSuMappa);
                    const feature2 = new Feature({geometry: p, sede: sede});
                    feature2.setId(AssettoISComponent.PREFISSO_SEDE + sede.codice);
                    feature2.setStyle((feature, resolution) => {
                        const displayText = resolution <= this.buildMapsService.getMinResolutionForText();
                        let label: String = '';
                        if (displayText) {
                            const labelODOF = BuildMapService.getODOF(is, sede);

                            switch (this.infoSediSelected) {
                                case InfoSedi.TUTTI:
                                    label = sede.denominazione + '\n' +
                                        sede.tipologiaScuola + ' ' + labelODOF;
                                    break;
                                case InfoSedi.NOMI:
                                    label = sede.denominazione;
                                    break;
                                case InfoSedi.OD_OF:
                                    label = sede.tipologiaScuola + ' ' + labelODOF;
                                    break;
                                case InfoSedi.DISATTIVA:
                                    label = '';
                                    break;

                            }
                        }
                        const visualizza = this.visualizzaSedeCiclo2(sede);
                        return this.buildMapsService.getStyleSede(feature, sede, displayText, label, visualizza);

                    });
                    features2[i++] = feature2;
                }
            });
        });
        this.sediCiclo2Layer = new VectorLayer({
            source: new Vector({
                features: features2,
            })
        });
        this.map.addLayer(this.sediCiclo2Layer);
        this.sediCiclo2Layer.setZIndex(12);
        const t1 = performance.now();
        console.log('rete Ciclo 2: ' + features2.length + ' sedi caricate in ' + (t1 - start) + 'ms');
        this.caricaLinkTraSediESediAmministrative2(isList2);
        const t2 = performance.now();
        console.log('rete Ciclo 2: link in ' + (t2 - t1) + 'ms');
    }

    private reloadConfini() {
        if (this.showConfini.showConfiniRegionali) {
            this.map.addLayer(this.confiniPugliaLayer);
        }
        if (this.showConfini.showConfiniAmbiti) {
            this.map.addLayer(this.confiniAmbitiLayer);
        }
        if (this.showConfini.showConfiniDistretti) {
            this.map.addLayer(this.confiniDistrettiLayer);
        }
        if (this.showConfini.showConfiniProvinciali) {
            this.map.addLayer(this.confiniProvinceLayer);
        }
        if (this.showConfini.showConfiniComunali) {
            this.map.addLayer(this.confiniComuniLayer);
        }
        if (this.showConfini.showConfiniUnioniComune) {
            this.map.addLayer(this.confiniUnioneComuniLayer);
        }
    }

    private resetConfini() {
        this.map = this.confiniService.resetConfiniByMap(this.map, [this.confiniPugliaLayer, this.confiniAmbitiLayer, this.confiniDistrettiLayer,
            this.confiniProvinceLayer, this.confiniComuniLayer, this.confiniUnioneComuniLayer]);
    }

    private onFeatureSelected(e) {

        console.log(e);
        if (e.selected.length > 0) {
            const id = e.selected[0].getId();
            const idIs = AssettoISComponent.getISId(id);
            const idSede = AssettoISComponent.getSedeId(id);

            console.log('selected id', id);

            if (idSede) {
                if (id) {
                    //TODO: visualizzare modal
                    // this.router.navigate(['/pages/analisi'],
                    //     {queryParams: {id_feature: idSede}});
                    console.log('Selezionato ', idSede);
                }
            }
        }


    }


    public updateFiltroSedi(event: FiltraSedi) {
        this.closePopup();
        this.filtraSedi = event;
        this.reloadReteCartografica();
        console.info('aggiorno filtro sedi', this.filtraSedi);
    }

    private reloadReteCartografica() {
        this.sediCiclo1Layer.changed();
        this.sediCiclo2Layer.changed();
        this.linkLayer1.changed();
        this.linkLayer2.changed();
        this.spostamentiLayer.changed();

    }


    private aggiornaOpzioni() {
        this.store.dispatch(new ModificaFiltri(
            {
                opzioni: {
                    mostraLegenda: this.opzioniMap.showLegend,
                    mostraSpostamenti: this.opzioniMap.showSpostamenti,
                    filtroLeggibilita: this.opzioniMap.applicaLeggibilita,
                    mostraEdificiInutilizzati: this.opzioniMap.mostraEdificiInutilizzati
                },
                filterCiclo: {
                    showOnlySediCiclo1: this.filtraSedi.filtraSediCiclo1.showOnlySediCiclo1,
                    showOnlySediCiclo2: this.filtraSedi.filtraSediCiclo2.showOnlySediCiclo2,
                    showOnlyCiclo1AA: this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1AA,
                    showOnlyCiclo1EE: this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1EE,
                    showOnlyCiclo1MM: this.filtraSedi.filtraSediCiclo1.showOnlyCiclo1MM,
                    showOnlyCiclo2LI: this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2LI,
                    showOnlyCiclo2IP: this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IP,
                    showOnlyCiclo2IT: this.filtraSedi.filtraSediCiclo2.showOnlyCiclo2IT,

                }, dimensionate: {
                    showOnlySovraDimensionate: this.showTipoIsOrganico.showOnlySovraDimensionate,
                    showOnlySottoDimensionate: this.showTipoIsOrganico.showOnlySottoDimensionate
                },
                infoSedi: this.infoSediSelected,
                lastZoom: this.lastZoom,
                lastExtent: this.lastExtent,
                center: this.map.getView().getCenter()
            }));

    }

    private initLegend() {
        //Primo  ciclo
        this.legendFields.push(new PointLegendField('P.E. Infanzia', BuildMapService.getCircleColorByTipologia('AA')));
        this.legendFields.push(new PointLegendField('P.E. Primaria', BuildMapService.getCircleColorByTipologia('EE')));
        this.legendFields.push(new PointLegendField('P.E. Secondaria I ciclo', BuildMapService.getCircleColorByTipologia('MM')));
        //Secondo  ciclo
        this.legendFields.push(new PointLegendField('P.E. II ciclo - Liceo', BuildMapService.getCircleColori2('li')));
        this.legendFields.push(new PointLegendField('P.E. II ciclo - Professionale', BuildMapService.getCircleColori2('ip')));
        this.legendFields.push(new PointLegendField('P.E. II ciclo - Tecnico', BuildMapService.getCircleColori2('it')));
        //
        this.legendFields.push(new PointLegendField('P.E. spostato', '#756CB6', true));
        this.legendFields.push(new PointLegendField('Edificio inutilizzato', '#646464'));
        this.legendFields.push(new LineLegendField('Istituzione Scolastica', '#800080'));
        this.legendFields.push(new LineLegendField('Spostamento P.E.', '#4D5259', true));
    }
}
