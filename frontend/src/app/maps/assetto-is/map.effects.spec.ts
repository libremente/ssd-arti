/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {TestBed} from '@angular/core/testing';
import {provideMockActions} from '@ngrx/effects/testing';
import {Observable} from 'rxjs';

import {MapEffects} from './map.effects';

describe('MapEffects', () => {
    let actions$: Observable<any>;
    let effects: MapEffects;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                MapEffects,
                provideMockActions(() => actions$)
            ]
        });

        effects = TestBed.get<MapEffects>(MapEffects);
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });
});
