/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FiltraConfiniMap} from '../../../shared/models/confini/FiltraConfiniMap';

@Component({
    selector: 'app-map-confini',
    templateUrl: './map-confini.component.html',
    styleUrls: ['./map-confini.component.css']
})
export class MapConfiniComponent implements OnInit {

    @Input()
    showConfini: FiltraConfiniMap;
    @Output()
    filtraConfini: EventEmitter<FiltraConfiniMap> = new EventEmitter();

    constructor() {
    }

    ngOnInit() {


    }

    toggleRegioni() {
        this.showConfini.showConfiniRegionali = !this.showConfini.showConfiniRegionali;
        this.filtraConfini.emit(this.showConfini);
    }

    toggleAmbiti() {
        this.showConfini.showConfiniAmbiti = !this.showConfini.showConfiniAmbiti;
        this.filtraConfini.emit(this.showConfini);

    }

    toggleDistretti() {
        this.showConfini.showConfiniDistretti = !this.showConfini.showConfiniDistretti;
        this.filtraConfini.emit(this.showConfini);

    }

    toggleProvince() {
        this.showConfini.showConfiniProvinciali = !this.showConfini.showConfiniProvinciali;
        this.filtraConfini.emit(this.showConfini);

    }

    toggleUnioniComuni() {
        this.showConfini.showConfiniUnioniComune = !this.showConfini.showConfiniUnioniComune;
        this.filtraConfini.emit(this.showConfini);

    }

    toggleComuni() {
        this.showConfini.showConfiniComunali = !this.showConfini.showConfiniComunali;
        this.filtraConfini.emit(this.showConfini);

    }

}
