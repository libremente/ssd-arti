/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
    selector: 'app-map-etichette',
    templateUrl: './map-etichette.component.html',
    styleUrls: ['./map-etichette.component.css']
})
export class MapEtichetteComponent implements OnInit {

    @Input()
    infoSedi: Array<any>;

    @Input()
    infoSediSelected: string;

    @Output()
    updateEtichetteSelected: EventEmitter<string>;

    constructor() {
        this.updateEtichetteSelected = new EventEmitter<string>();
    }

    ngOnInit() {
    }


    public changeLabelSedi(index) {
        this.infoSediSelected = this.infoSedi[index];
        this.updateEtichetteSelected.emit(this.infoSediSelected);


    }

}
