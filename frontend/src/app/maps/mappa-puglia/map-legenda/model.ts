/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

export interface LegendField {
    label: String;
    color: string;
}

export class AreaLegendField implements LegendField {

    color: string;
    label: string;

    constructor(label: string, color: string) {
        this.label = label;
        this.color = color;
    }

}

export class PointLegendField implements LegendField {

    color: string;
    label: string;
    spostato: boolean;

    constructor(label: string, color: string, spostato: boolean = false) {
        this.label = label;
        this.color = color;
        this.spostato = spostato;
    }

}


export class LineLegendField implements LegendField {

    color: string;
    label: string;
    dashed: boolean;

    constructor(label: string, color: string, dashed: boolean = false) {
        this.label = label;
        this.color = color;
        this.dashed = dashed;
    }

}
