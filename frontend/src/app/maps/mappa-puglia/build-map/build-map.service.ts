/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import OlMap from 'ol/Map';
import OlTileLayer from 'ol/layer/Tile';
import OlView from 'ol/View';
import OSM from 'ol/source/OSM';
import VectorLayer from 'ol/layer/Vector.js';
import {TileDebug, Vector} from 'ol/source.js';
import GeoJSON from 'ol/format/GeoJSON.js';
import Overlay from 'ol/Overlay.js';
import Center, {Extent} from 'ol';
import {fromLonLat} from 'ol/proj.js';
import TileLayer from 'ol/layer/Tile.js';
import {defaults as defaultControls, FullScreen, ScaleLine} from 'ol/control.js';
import {Injectable} from '@angular/core';
import {Circle as CircleStyle, Fill, Stroke, Style, Text} from 'ol/style';
import {Sede} from '../../../shared/models/Sede';
import {defaults as defaultInteractions} from 'ol/interaction.js';
import {SedeCiclo2} from '../../../shared/models/SedeCiclo2';
import {IstituzioneScolastica} from '../../../shared/models/IstituzioneScolastica';


/*@Injectable({
  providedIn: 'root'
})*/


@Injectable()
export class BuildMapService {

    static ZOOM_SOGLIA = 14;
    private _confiniUnioneComuniLayer: VectorLayer;
    private spostamentiStyle = new Style({
        stroke: new Stroke({
            color: '#4D5259',
            width: 2,
            lineDash: [5, 10]
        })
    });
    private cachedStyles = {};

    constructor() {

        this._confiniPugliaLayer = new VectorLayer({
            source: new Vector({
                url: '/assets/maps/regioni.json',
                format: new GeoJSON()
            }),
            zIndex: 1
        });

        // var defaultEditingStyleFunction = new this._confiniPugliaLayer.getStyleFunction();

        this._confiniAmbitiLayer = new VectorLayer({
            source: new Vector({
                url: '/assets/maps/ambiti.json',
                format: new GeoJSON()
            }),
            zIndex: 2,
            style: (feature) => this.getStyleConfine(feature)
        });

        this._confiniDistrettiLayer = new VectorLayer({
            source: new Vector({
                url: '/assets/maps/distretti.json',
                format: new GeoJSON()
            }),
            zIndex: 3,
            style: (feature) => this.getStyleConfine(feature)
        });


        this._confiniProvinceLayer = new VectorLayer({
            source: new Vector({
                url: '/assets/maps/province.json',
                format: new GeoJSON()
            }),
            zIndex: 4,
            style: (feature) => this.getStyleConfine(feature)
        });

        this._confiniComuniLayer = new VectorLayer({
            source: new Vector({
                url: '/assets/maps/comuni.json',
                format: new GeoJSON()
            }),
            zIndex: 5,
            style: (feature) => this.getStyleConfine(feature)
        });

        this._confiniUnioneComuniLayer = new VectorLayer({
            source: new Vector({
                url: '/assets/maps/unioniComuni.json',
                format: new GeoJSON()
            }),
            zIndex: 6,
            style: (feature) => this.getStyleConfine(feature)
        });


        this._spostamentiLayer = new VectorLayer({
            source: new Vector({
                features: [],
            }),
            zIndex: 11
        });
        this._overlay = new Overlay({});


    }

    private _view: OlView = new OlView();

    public get view(): OlView {
        return this._view;
    }

    public set view(view: OlView) {
        this._view = view;
    }

    private _map: OlMap = new OlMap();

    public get map(): OlMap {
        return this._map;
    }

    public set map(map: OlMap) {
        this._map = map;
    }

    private _confiniComuniLayer: VectorLayer;

    public get confiniComuniLayer(): VectorLayer {
        return this._confiniComuniLayer;
    }

    public set confiniComuniLayer(confiniComuniLayer: VectorLayer) {
        this._confiniComuniLayer = confiniComuniLayer;
    }

    private _confiniPugliaLayer: VectorLayer;

    public get confiniPugliaLayer(): VectorLayer {
        return this._confiniPugliaLayer;
    }

    public set confiniPugliaLayer(confiniPugliaLayer: VectorLayer) {
        this._confiniPugliaLayer = confiniPugliaLayer;
    }

    private _confiniAmbitiLayer: VectorLayer;

    public get confiniAmbitiLayer(): VectorLayer {
        return this._confiniAmbitiLayer;
    }

    private _confiniDistrettiLayer: VectorLayer;

    public get confiniDistrettiLayer(): VectorLayer {
        return this._confiniDistrettiLayer;
    }

    private _confiniProvinceLayer: VectorLayer;

    public get confiniProvinceLayer(): VectorLayer {
        return this._confiniProvinceLayer;
    }

    public set confiniProvinceLayer(confiniProvinceLayer: VectorLayer) {
        this._confiniProvinceLayer = confiniProvinceLayer;
    }

    private _spostamentiLayer: VectorLayer;

    public get spostamentiLayer(): VectorLayer {
        return this._spostamentiLayer;
    }

    public set spostamentiLayer(spostamentiLayer: VectorLayer) {
        this._spostamentiLayer = spostamentiLayer;
    }

    private _overlay: Overlay;

    public get overlay(): Overlay {
        return this._overlay;
    }

    public set overlay(overlay: Overlay) {
        this._overlay = overlay;
    }

    private _baseLayer: OlTileLayer;

    public get baseLayer(): OlTileLayer {
        return this._baseLayer;
    }

    public set baseLayer(baseLayer: OlTileLayer) {
        this._baseLayer = baseLayer;
    }

    public get confiniUnioneComuni(): VectorLayer {
        return this._confiniUnioneComuniLayer;
    }

    static ordinaSedi(sedi: Sede[]) {
        sedi.sort((a, b) => {
            const x = a.sedeDirezioneAmministrativa;
            const y = b.sedeDirezioneAmministrativa;
            return (x === y) ? 0 : x ? -1 : 1;
        });
    }

    static getFeatureFromEvt(evt) {
        const pixel = evt.map.getEventPixel(evt.originalEvent);
        return evt.map.forEachFeatureAtPixel(pixel, function (feature) {
            return feature;
        });
    }

    static applicaFiltroLeggibilita(event) {
        // console.log('applica filtro ', event);

        const context = event.context;
        const preComposite = context.globalCompositeOperation;

        // scoloro la mappa
        context.globalCompositeOperation = 'color';
        context.fillStyle = 'rgba(255,255,255,1)';
        context.fill();
        // la rendo più gradevole
        context.globalCompositeOperation = 'screen';
        context.fillStyle = 'rgba(255,255,255,0.5)';
        context.fill();
        // resetto l'operazione di composizione
        context.globalCompositeOperation = preComposite;

    }

    static getTipoSediCiclo2IndrizzoDiStudio(item: SedeCiclo2): string {


        if (item.indirizziDiStudio.length > 0) {
            const macroTipo = item.indirizziDiStudio[0].codice.substr(0, 2);


            const numDiIndiStudio = item.indirizziDiStudio.length;
            let cont = 0;
            item.indirizziDiStudio.forEach(elem => {
                if (elem.codice.substr(0, 2).toLowerCase() === macroTipo.toLowerCase()) {
                    cont++;
                }
            });

            if (cont === numDiIndiStudio) {
                return BuildMapService.getCircleColori2(macroTipo.toLowerCase());
            } else {
                return BuildMapService.getCircleColori2('no color');
            }

        } else {
            return BuildMapService.getCircleColori2('no color');
        }

    }

    static getCircleColori2(tipoIndiCiclo2): string {

        switch (tipoIndiCiclo2) {
            case 'it':
                return '#ffc107';
            case 'ip':
                return '#d9534f';
            case 'li':
                return '#8b4513';
            default:
                return '#343a40';

        }

    }

    static getCircleColor(item: Sede) {

        switch (item.tipologiaScuola) {
            case 'AA':
            case 'MM':
            case 'EE':
                return BuildMapService.getCircleColorByTipologia(item.tipologiaScuola);
            case 'SS':
                return BuildMapService.getTipoSediCiclo2IndrizzoDiStudio(item as SedeCiclo2);
        }

    }

    static getCircleColorByTipologia(tipologiaScuola: String) {

        switch (tipologiaScuola) {
            case 'AA':
                return '#428bca';
            case 'MM':
                return '#5cb85c';
            case 'EE':
                return '#5bc0de';
        }

    }

    static getCircleRadius(item: Sede) {
        return 10;
    }

    static getODOF(is: IstituzioneScolastica, sede: Sede): String {
        const isAlunni = (is.alunni && is.alunni.totaleAlunni) ? is.alunni.totaleAlunni : 'ND';
        const alunniOD = (sede.alunni && sede.alunni.totaleAlunni) ? sede.alunni.totaleAlunni : 'ND';
        const alunniOF = (sede.alunni && sede.alunni.totaleAlunniOF) ? sede.alunni.totaleAlunniOF : 'ND';
        return alunniOD + '(' + alunniOF + ')/' + isAlunni;
    }

    getStyleSede(feature, sede: Sede, displayText: boolean, label: String, visualizza: boolean = true) {

        if (!visualizza) {
            return null;
        }

        const key = '' + sede.id + sede.tipologiaScuola + displayText + label;

        const cached = this.cachedStyles[key];
        if (cached) {
            return cached;
        }


        const nLine = 1 + label.split('\n').length;

        const offsetY = -(BuildMapService.getCircleRadius(sede) + 2 + 4 * nLine);

        const styles = [new Style({
            visibility: 'hidden',
            image: new CircleStyle({
                radius: BuildMapService.getCircleRadius(sede),
                fill: new Fill({color: BuildMapService.getCircleColor(sede)})
            }),
            text: new Text({
                text: displayText ? label : '',
                offsetY: offsetY,
                offsetX: 0,
                scale: 1.3,
                fill: new Fill({
                    color: '#000000'
                }),
                stroke: new Stroke({
                    color: '#FFFF',
                    width: 3.5
                })
            })
        })];
        if (sede.coordinateSparpagliate) {
            styles.push(
                new Style({
                    text: new Text({
                        text: displayText ? 'I️' : '',
                        offsetY: 0,
                        offsetX: 0,
                        scale: 1,
                        fill: new Fill({
                            color: '#FFFFFF'
                        })
                    })
                }));
        }

        this.cachedStyles[key] = styles;
        return styles;

    }

    getStyleEdificio(feature, displayText: boolean, label: String, visualizza: boolean = true) {


        if (!visualizza) {
            return null;
        }
        const key = 'edifici' + displayText + label;
        const cached = this.cachedStyles[key];
        if (cached) {
            return cached;
        }

        const nLine = 1 + label.split('\n').length;

        const radius = 8;
        const offsetY = -(radius + 2 + 4 * nLine);

        const style = [new Style({
            visibility: 'hidden',
            image: new CircleStyle({
                radius: radius,
                fill: new Fill({color: '#646464'})
            }),
            text: new Text({
                text: displayText ? label : '',
                offsetY: offsetY,
                offsetX: 0,
                scale: 1.3,
                fill: new Fill({
                    color: '#000000'
                }),
                stroke: new Stroke({
                    color: '#FFFF',
                    width: 3.5
                })
            })
        })];

        this.cachedStyles[key] = style;

        return style;

    }

    getLinkColor(colorGroup: number) {
        // return '#4A4A4A';
        // switch (colorGroup % 3) {
        //     case 0:
        //         return '#145380';
        //     case 1:
        //         return '#7c800b';
        //     case 2:
        //         return '#801c32';
        // }
        return '#800080';

    }

    getLinkWidth() {
        return 2;
    }

    getSpostamentiStyle() {

        return this.spostamentiStyle;
    }

    freeStyles() {
        console.log('N cached styles: ', Object.keys(this.cachedStyles).length);
        this.cachedStyles = {};
    }

    public getMinResolutionForText(): number {
        return this.view.getResolutionForZoom(BuildMapService.ZOOM_SOGLIA);
    }

    public configDefaultView(lastZoom: number, lastExtent?: Extent, center?: Center) {

        const bounds = [1658453, 4125314.896182091, 2300000, 5246254.224389329];
        const centerDefault = fromLonLat([16.866342, 41.118151]);

        this._view = new OlView({
            center: (center || centerDefault),
            zoom: lastZoom,
            minZoom: 8,
            maxZoom: 18,
            extent: (lastExtent || bounds)
        });
        return this._view;
    }

    public configOverlay(container: any) {

        return this.overlay = new Overlay({
            element: container,
            autoPan: true,
            autoPanAnimation: {
                duration: 200
            }
        });
    }

    public configBaseLayer() {
        return this._baseLayer = new OlTileLayer({
            source: new OSM()
        });

    }

    public debugLayerMode(): TileLayer {
        const source = new OSM();
        return new TileLayer({
            source: new TileDebug({
                projection: 'EPSG:27700',
                tileGrid: source.getTileGrid()
            }),
            zIndex: 20

        });
    }

    public configDefaultMap(layers, overlay: Overlay, view: OlView): OlMap {

        const interactions = defaultInteractions({
            pinchRotate: false
        });

        return new OlMap({
            target: 'map',
            layers: layers,
            view: view,
            overlays: [overlay],
            controls: defaultControls().extend([
                new ScaleLine(), new FullScreen()]),
            interactions: interactions
        });


    }

    getLinkStyle(i: number): Style {

        const key = 'link' + i;
        const cached = this.cachedStyles[key];
        if (cached) {
            return cached;
        }

        const style = new Style({
            stroke: new Stroke({
                color: this.getLinkColor(i),
                width: this.getLinkWidth()
            })
        });

        this.cachedStyles[key] = style;

        return style;
    }

    private getStyleConfine(feature) {
        const text = feature.values_.NOME_COM || feature.values_.NOME_PRO || feature.values_.nome || feature.values_.codice
            || '' + feature.values_.numero;
        const key = '' + feature.id + text;

        const cached = this.cachedStyles[key];
        if (cached) {
            return cached;
        }
        const fill = new Fill({
            color: 'rgba(255,255,255,0.4)'
        });
        const stroke = new Stroke({
            color: '#3399CC',
            width: 1.25
        });
        this.cachedStyles[key] = new Style({
            image: new CircleStyle({
                fill: fill,
                stroke: stroke,
                radius: 5
            }),
            fill: fill,
            stroke: stroke,
            text: new Text({
                text: text,
                offsetY: 0,
                offsetX: 0,
                scale: 1.3,
                fill: new Fill({
                    color: '#000000'
                }),
                stroke: new Stroke({
                    color: '#FFFF',
                    width: 3.5
                })
            })
        });
        return this.cachedStyles[key];
    }
}
