/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {
    AfterContentInit,
    AfterViewInit,
    Component,
    ContentChild,
    ElementRef,
    EventEmitter,
    HostListener,
    Input,
    OnDestroy,
    OnInit,
    Output,
    TemplateRef
} from '@angular/core';
import OlMap from 'ol/Map';
import OlTileLayer from 'ol/layer/Tile';
import OlView from 'ol/View';

import {Control} from 'ol/control.js';
import {Extent} from 'ol';

import Select from 'ol/interaction/Select.js';
import {Router} from '@angular/router';
import Overlay from 'ol/Overlay.js';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {BuildMapService} from './build-map/build-map.service';
import {Sede} from '../../shared/models/Sede';
import {SedeAnalisiDatiModalComponent} from '../modal-maps/sede-analisi-dati-modal/sede-analisi-dati-modal.component';
import {fromLonLat} from 'ol/proj';
import {delay} from 'q';
import {MappaPugliaUtilsService} from '../../shared/services/mappa-puglia/mappa-puglia-utils.service';
import {SediCiclo2Service} from '../../shared/services/sediciclo2/sedi-ciclo2.service';
import {AnnoScolasticoService} from '../../modules/anno-scolastico/anno-scolastico.service';
import {FiltraConfiniMap} from '../../shared/models/confini/FiltraConfiniMap';
import {ConfiniService} from '../../shared/services/confini/confini.service';
import {FiltraSedi} from '../../shared/models/sedi/FiltraSedi';
import {InfoSedi} from './Utils';
import {OpzioniMap} from '../../shared/models/opzioni-map/OpzioniMap';
import {LegendField} from './map-legenda/model';


export interface PopupResultContext {
    /**
     * Your typeahead result data model
     */
    result: any;

}


@Component({
    selector: 'app-mappa-puglia',
    templateUrl: './mappa-puglia.component.html',
    styleUrls: ['./mappa-puglia.component.css']
})
export class MappaPugliaComponent implements OnInit, AfterViewInit, AfterContentInit, OnDestroy {

    private static PREFISSO_IS = 'IS_';
    private static PREFISSO_SEDE = 'SEDE_';

    public selectedSede: Sede = null;
    public coordinateClicked: any;
    public opzioniMap: OpzioniMap = new OpzioniMap();

    public filtraSedi: FiltraSedi = new FiltraSedi();

    @ContentChild(TemplateRef) popupTemplate: TemplateRef<ElementRef>;

    @Input() fields: LegendField[] = [];

    @Output() onSelected: EventEmitter<any> = new EventEmitter();
    @Output() updateEtichette: EventEmitter<any> = new EventEmitter();

    @Output() updateOpzioniMap: EventEmitter<any> = new EventEmitter();
    @Output() updateFiltraSedi: EventEmitter<any> = new EventEmitter();


    private custom: any = document.createElement('div');

    public applicaLeggibilita: Boolean = false;

    public showConfini: FiltraConfiniMap;


    private confiniAmbitiLayer = this.buildMapsService.confiniAmbitiLayer;
    private confiniDistrettiLayer = this.buildMapsService.confiniDistrettiLayer;
    private confiniUnioneComuniLayer = this.buildMapsService.confiniUnioneComuni;
    private confiniPugliaLayer = this.buildMapsService.confiniPugliaLayer;
    private confiniProvinceLayer = this.buildMapsService.confiniProvinceLayer;
    private confiniComuniLayer = this.buildMapsService.confiniComuniLayer;

    public infoSedi = [InfoSedi.TUTTI, InfoSedi.NOMI, InfoSedi.OD_OF, InfoSedi.DISATTIVA];
    public infoSediSelected: string;

    private toggleFiltroLeggibilita = this.mappaPugliaUtilsService.createToggleFiltroLeggibilita();

    private lastExtent: Extent = null;
    lastZoom: 8;

    map: OlMap;
    private baseLayer: OlTileLayer;
    view: OlView;
    private openPopoverTimer: any = null;
    public overlay: Overlay;


    constructor(private modalService: NgbModal,
                public buildMapsService: BuildMapService,
                private mappaPugliaUtilsService: MappaPugliaUtilsService,
                private sedeCiclo2Service: SediCiclo2Service,
                private annoScolasticoService: AnnoScolasticoService,
                private confiniService: ConfiniService,
                private router: Router) {
        this.showConfini = new FiltraConfiniMap();
        this.opzioniMap.showLegend = true;
    }

    ngAfterContentInit() {
        console.log('popupTemplate', this.popupTemplate);

    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        setTimeout(() => {
            this.map.updateSize();
        }, 20);
    }


    updateOpzioniOnMap(event) {
        console.info('update opzioni event', event);
    }


    private getDefaultSettingMap() {
        this.lastZoom = (this.lastZoom) ? this.lastZoom : this.lastZoom = 8;
    }


    aggiornaFiltroSedi(event: FiltraSedi) {
        this.filtraSedi = event;
        this.updateFiltraSedi.emit(event);
    }

    ngOnInit() {
        this.getDefaultSettingMap();
        const component = this;

        this.baseLayer = this.buildMapsService.configBaseLayer();

        this.view = this.buildMapsService.configDefaultView(this.lastZoom);
        this.view.on('change:resolution', (event) => {
            //  this.updateTextOnFeature(event.target.getZoom());


        });
        /**
         * Elements that make up the popup.
         */

        const container = document.getElementById('popup');

        /**
         * Create an overlay to anchor the popup to the map.
         */

        this.overlay = this.buildMapsService.configOverlay(container);


        const layers = [this.baseLayer];

        //setting debug layer
        if (false) {
            layers.push(this.buildMapsService.debugLayerMode());
        }

        this.custom.className = 'custom-div ol-zoom  ol-unselectable  ol-control';
        this.custom.appendChild(this.mappaPugliaUtilsService.createToggleFiltroLeggibilita());
        this.toggleFiltroLeggibilita.addEventListener('click', () => {
            this.baseLayer = this.mappaPugliaUtilsService.getToggleFiltroLeggibilita(this.applicaLeggibilita, this.baseLayer);
            this.map.render();
        });


        this.map = this.buildMapsService.configDefaultMap(layers, this.overlay, this.view);
        this.map.controls.push(new Control({element: this.custom}));
        this.map.on('moveend', (event) => this.onMoveEnd(event));
        // this.map.on('pointermove', (event) => component.onPointerMove(event));
        this.map.on('click', (event) => component.onMapClicked(event));

        const selectSingleClick = new Select();
        this.map.addInteraction(selectSingleClick);
        selectSingleClick.on('select', (event) => this.onFeatureSelected(event));

    }


    public dettaglioSedeCiclo1(idSede: number) {
        const activeModal = this.modalService.open(SedeAnalisiDatiModalComponent, {size: 'lg'});
        (activeModal.componentInstance as SedeAnalisiDatiModalComponent).loadDataSedeCiclo1(idSede);
    }

    public dettaglioSedeCiclo2(idSede: number) {
        const activeModal = this.modalService.open(SedeAnalisiDatiModalComponent, {size: 'lg'});
        (activeModal.componentInstance as SedeAnalisiDatiModalComponent).loadDataSedeCiclo2(idSede);
    }


    private static getISId(string: string): number | undefined {

        if (string.startsWith(this.PREFISSO_IS)) {
            console.log(string.substr(3));
            return parseInt(string.substr(3), 10);
        }
        return undefined;
    }

    private static getSedeId(string: string): string | undefined {
        if (string.startsWith(this.PREFISSO_SEDE)) {
            return string.substr(5);
        }
        return undefined;
    }


    public apriPopupSede(sede: Sede) {
        const featureId = MappaPugliaComponent.PREFISSO_SEDE + sede.codice;
        this.setSedePopup(featureId);

    }


    private onMoveEnd(evt) {
        const map = evt.map;
        this.moveOnPositionFocused(map);
    }

    private moveOnPositionFocused(map) {
        const currentZoom = map.getView().getZoom();
        this.lastExtent = map.getView().calculateExtent(map.getSize());
        console.log('last Ext = ', this.lastExtent, 'last zoom ', this.lastZoom);
        if (this.selectedSede && this.lastZoom !== currentZoom) {
            this.overlay.setPosition(fromLonLat(this.selectedSede.coordinate.coordinates));
        }
        this.lastZoom = currentZoom;
    }

    private onMapClicked(evt) {
        if (evt.dragging) {
            return;
        }
        const feature = BuildMapService.getFeatureFromEvt(evt);
        if (feature) {
            const id = feature.getId();


            if (!id) {
                // id non settato
                return;
            }
            this.coordinateClicked = evt.coordinate;
        } else {
            this.closePopup();
        }
    }

    private onPointerMove(evt) {

        if (evt.dragging) {
            return;
        }

        const feature = BuildMapService.getFeatureFromEvt(evt);

        clearTimeout(this.openPopoverTimer);
        if (feature) {
            const id = feature.getId();
            if (!id) {
                // id non settato
                return;
            }
            this.openPopoverTimer = setTimeout(() => {
                this.setSedePopup(id);
            }, 200);

        } else {
            // disattivato nascondi popup
            // this.closePopup()
        }
    }

    public setSedePopup(sede: any) {
        const component = this;
        delay(50).done(() => {
            this.coordinateClicked = fromLonLat(sede.coordinate.coordinates);
            component.overlay.setPosition(this.coordinateClicked);
        });
    }


    closePopup() {
        this.overlay.setPosition(undefined);
    }


    ngAfterViewInit(): void {
        this.map.setTarget('map');
        if (this.opzioniMap.applicaLeggibilita) {
            // abilito di default il layer di leggibilità
            this.baseLayer.addEventListener('postcompose', BuildMapService.applicaFiltroLeggibilita);
            this.map.render();
        }
    }

    ngOnDestroy(): void {
    }


    private reloadConfini() {
        if (this.showConfini.showConfiniRegionali) {
            this.map.addLayer(this.confiniPugliaLayer);
        }
        if (this.showConfini.showConfiniAmbiti) {
            this.map.addLayer(this.confiniAmbitiLayer);
        }
        if (this.showConfini.showConfiniDistretti) {
            this.map.addLayer(this.confiniDistrettiLayer);
        }
        if (this.showConfini.showConfiniProvinciali) {
            this.map.addLayer(this.confiniProvinceLayer);
        }
        if (this.showConfini.showConfiniComunali) {
            this.map.addLayer(this.confiniComuniLayer);
        }
        if (this.showConfini.showConfiniUnioniComune) {
            this.map.addLayer(this.confiniUnioneComuniLayer);
        }
    }

    private resetConfini() {
        this.map = this.confiniService.resetConfiniByMap(this.map,
            [
                this.confiniPugliaLayer, this.confiniAmbitiLayer, this.confiniDistrettiLayer, this.confiniProvinceLayer,
                this.confiniComuniLayer, this.confiniUnioneComuniLayer
            ]);
    }

    public filterConfini(filtraConfini: FiltraConfiniMap) {
        this.showConfini = filtraConfini;
        this.resetConfini();
        this.reloadConfini();
    }


    public updateEtichetteSelected(event) {
        this.infoSediSelected = event;
        this.updateEtichette.emit(this.infoSediSelected);

    }

    public updateShowLegenda(showLegenda: boolean) {
        this.opzioniMap.showLegend = showLegenda;
    }

    private onFeatureSelected(e) {
        if (e.selected.length > 0) {
            const id = e.selected[0].getId();

            console.log('ho selezionato questa sede ' + id);
            this.onSelected.emit(id);
        }
    }
}
