/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Table} from '../../shared/settings/table.settings';
import {Subscription} from 'rxjs';
import {ISService} from '../../shared/services/istituzioniScolastiche/is.service';
import {IstituzioneScolastica} from '../../shared/models/IstituzioneScolastica';
import {SedeCiclo2} from '../../shared/models/SedeCiclo2';
import {AnnoScolasticoService} from '../../modules/anno-scolastico/anno-scolastico.service';
import {AnnoScolastico} from '../../shared/models/AnnoScolastico';
import {DemografieService} from '../../modules/pages/tables/demografia/demografie.service';
import * as _ from 'lodash';

@Component({
    selector: 'app-istituzioni-analisi',
    templateUrl: './istituzioni-analisi.component.html',
    styleUrls: ['./istituzioni-analisi.component.css']
})
export class IstituzioniAnalisiComponent implements OnInit, OnDestroy {
    public codice: string;
    public is: IstituzioneScolastica;

    public rows: Array<any>;
    public columns: Array<any>;

    public buttonTableOrReview: string;
    private idAnno: number;
    private sub: Subscription;

    private annoScolastico: AnnoScolastico;

    public chartDataIs: any;
    public chartList = [];

    constructor(private route: ActivatedRoute, private istituzioniService: ISService,
                private annoScolasticoService: AnnoScolasticoService,
                private trendService: DemografieService) {
        this.buttonTableOrReview = 'review';
        this.columns = Table.sede;
    }

    ngOnInit() {
        this.sub = this.route.queryParams.subscribe(params => {
            this.codice = params['ist'];
            this.idAnno = params['anno'];


        });


        this.annoScolasticoService.get(this.idAnno).subscribe(resp => {

            console.log('mappo resp', resp);
            this.annoScolastico = resp;
            this.loadIstituzioneData();

        });
        this.annoScolasticoService.get(this.idAnno).subscribe(resp => {
            this.annoScolastico = resp;
            this.loadIstituzioneData();
        });


    }


    public loadIstituzioneData() {
        this.istituzioniService.cerca(this.codice, this.idAnno).subscribe(resp => {


            this.is = resp[0];

            this.trendService.trendIs(this.annoScolastico, this.is.codice).subscribe(resp2 => {

                this.chartDataIs = resp2;
            });


            console.log(this.is);
            this.is.sediCiclo2.forEach(item => {
                this.trendService.trendPe2(this.annoScolastico, item.codice).subscribe(resp3 => {
                    console.log(resp3);

                    this.chartList.push({codice: item.codice, data: resp3});
                });


            });


        });


    }


    public getChartData(codiceSede: String) {


        //  console.log("indice",_.findIndex(this.chartList, {codice: codiceSede});
        if (this.chartList.length > 0) {
            const chartData = this.chartList[_.findIndex(this.chartList, {codice: codiceSede})];
            if (chartData) {
                return chartData.data;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }


    public showTableOrReview(value: string) {
        return this.buttonTableOrReview == value ? true : false;
    }

    public toogleTableOrReview() {
        this.buttonTableOrReview == 'review'
            ? (this.buttonTableOrReview = 'table')
            : (this.buttonTableOrReview = 'review');
    }

    public getAlternative() {
        return this.buttonTableOrReview == 'review' ? 'Tabella' : 'Riepilogo';
    }

    public getLat(sede) {
        return sede.coordinate['coordinates'][0];
    }

    public getLon(sede) {
        return sede.coordinate['coordinates'][1];
    }

    public hasIndirizziDiStudio(sede: SedeCiclo2) {


        return (sede.indirizziDiStudio) ? true : false;
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }
}
