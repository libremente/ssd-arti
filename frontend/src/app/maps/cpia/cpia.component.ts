/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, OnInit} from '@angular/core';
import OlMap from 'ol/Map';
import OlXYZ from 'ol/source/XYZ';
import OlTileLayer from 'ol/layer/Tile';
import OlView from 'ol/View';
import OSM from 'ol/source/OSM';
import {BuildMapService} from '../mappa-puglia/build-map/build-map.service';
import Overlay from 'ol/Overlay.js';
import Select from 'ol/interaction/Select.js';


@Component({
    selector: 'app-cpia',
    templateUrl: './cpia.component.html',
    styleUrls: ['./cpia.component.css']
})
export class CpiaComponent implements OnInit {


    map: OlMap;
    private source: OlXYZ;
    private baseLayer: OlTileLayer;
    private view: OlView;
    private openPopoverTimer: any = null;
    private overlay: Overlay;

    private confiniPugliaLayer = this.buildMapsService.confiniPugliaLayer;
    private confiniProvinceLayer = this.buildMapsService.confiniProvinceLayer;
    private confiniComuniLayer = this.buildMapsService.confiniComuniLayer;

    constructor(private buildMapsService: BuildMapService) {
    }


    ngOnInit() {


        const component = this;

        this.source = new OSM();
        this.baseLayer = this.buildMapsService.configBaseLayer();

        this.view = this.buildMapsService.configDefaultView(8);
        /**
         * Elements that make up the popup.
         */










        const layers = [this.baseLayer];

        //setting debug layer
        if (false) {
            layers.push(this.buildMapsService.debugLayerMode());
        }
        this.map = this.buildMapsService.configDefaultMap(layers, this.overlay, this.view);


        const selectSingleClick = new Select();
        this.map.addInteraction(selectSingleClick);

        if (false) {
            // after rendering the layer, show an oversampled version around the pointer
            this.baseLayer.on('postcompose', function (event) {

                var context = event.context;
                const preComposite = context.globalCompositeOperation;
                context.globalCompositeOperation = 'multiply';

                context.fillStyle = '#ffd979';
                context.fillRect(0, 0, context.width, context.height);
                context.globalCompositeOperation = preComposite;
                component.map.render();
            });
        }

    }

}
