/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {OffertaFormativaActionTypes} from './offerta-formativa.actions';
import {SedeCiclo2} from '../../shared/models/SedeCiclo2';
import {TreeNode} from 'primeng/api';


export const offertaFormativaFeatureKey = 'offertaFormativa';


export interface OffertaFormativaState {
    selectedNode: Array<TreeNode>;
    sedeSelected: SedeCiclo2;
    coordinateClicked: any;


}

export const initialState: OffertaFormativaState = {
    selectedNode: null,
    sedeSelected: null,
    coordinateClicked: {},
};

export function offFormativaReducer(state: OffertaFormativaState = initialState, action): OffertaFormativaState {
    switch (action.type) {
        case OffertaFormativaActionTypes.IndirizziSelectedActions:
            return {
                ...state,
                selectedNode: action.payload.selectedNode,
                sedeSelected: action.payload.selectedSede,
                coordinateClicked: action.payload.coordinateClicked,


            };

        default:
            return state;
    }
}
