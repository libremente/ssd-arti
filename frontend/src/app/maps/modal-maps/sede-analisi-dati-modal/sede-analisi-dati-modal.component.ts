/*
 * SSD - Sistema per il dimensionamento scolastico
 * Copyright (c) 2020 ARTI - Agenzia Regionale per la Tecnologia e l'Innovazione della Regione Puglia.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.txt.
 */

import {Component, OnInit} from '@angular/core';
import {SedeCiclo1Service} from '../../../shared/services/sedeciclo1/sede-ciclo1.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {SediCiclo2Service} from '../../../shared/services/sediciclo2/sedi-ciclo2.service';
import {SedeCiclo2} from '../../../shared/models/SedeCiclo2';

@Component({
    selector: 'app-sede-analisi-dati-modal',
    templateUrl: './sede-analisi-dati-modal.component.html',
    styleUrls: ['./sede-analisi-dati-modal.component.css']
})
export class SedeAnalisiDatiModalComponent implements OnInit {

    constructor(private sedeCiclo1Service: SedeCiclo1Service,
                private sedeCiclo2Service: SediCiclo2Service,
                private activeModal: NgbActiveModal) {
    }


    public idSede: number;
    public sede: any;

    ngOnInit() {


    }

    public loadDataSedeCiclo1(idSede: number) {
        this.idSede = idSede;
        this.sedeCiclo1Service.get(this.idSede).subscribe(resp => {
            this.sede = resp;
            console.log(this.sede.alunni);
        });
    }

    public loadDataSedeCiclo2(idSede: number) {

        this.idSede = idSede;
        this.sedeCiclo2Service.get(this.idSede).subscribe(resp => {
            this.sede = resp;
            console.log('sede ciclo 2 ', this.sede);
        });
    }


    hasIndirizziDiStudio() {


        if (this.sede instanceof SedeCiclo2) {
            console.log('è una sede  ciclo 2');
        }

        return (this.sede.indirizziDiStudio) ? true : false;
    }

    public close() {
        this.activeModal.dismiss();
    }

}
