import {LOCALE_ID, NgModule} from '@angular/core';

import {RouterModule} from '@angular/router';

import {FullComponent} from './layouts/full/full.component';

import {NavigationComponent} from './shared/components/header-navigation/navigation.component';
import {SidebarComponent} from './shared/sidebar/sidebar.component';
import {BreadcrumbComponent} from './shared/components/breadcrumb/breadcrumb.component';
import {PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface} from 'ngx-perfect-scrollbar';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SharedModule} from './modules/shared.module';
import {AuthenticationModule} from './modules/authentication/authentication.module';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {HttpInterceptorCustom} from './shared/services/http-interceptor-custom.service';
import {SedeAnalisiDatiModalComponent} from './maps/modal-maps/sede-analisi-dati-modal/sede-analisi-dati-modal.component';
import {AnnoScolasticoModule} from './modules/anno-scolastico/anno-scolastico.module';
import {ComuniModule} from './modules/comuni/comuni.module';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {WorkInProgressComponent} from './shared/components/work-in-progress/work-in-progress.component';
import {ToastrModule} from 'ngx-toastr';
import localeIt from '@angular/common/locales/it';
import localeItExtra from '@angular/common/locales/extra/it';

import {registerLocaleData} from '@angular/common';
import {StoreModule} from '@ngrx/store';
import {metaReducers, reducers} from './reducers';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../environments/environment';
import {EffectsModule} from '@ngrx/effects';
import {MapEffects} from './maps/assetto-is/map.effects';


const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true,
    wheelSpeed: 2,
    wheelPropagation: true
};

@NgModule({
    declarations: [
        AppComponent,
        FullComponent,
        NavigationComponent,
        BreadcrumbComponent,
        SidebarComponent,
        SedeAnalisiDatiModalComponent,
        WorkInProgressComponent
    ],
    imports: [
        AppRoutingModule,
        AuthenticationModule,
        SharedModule,
        RouterModule,
        AnnoScolasticoModule,
        ComuniModule,
        BrowserModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot({positionClass: 'toast-top-right'}),
        StoreModule.forRoot(reducers, {
            metaReducers,
            runtimeChecks: {
                strictStateImmutability: true,
                strictActionImmutability: true,
            }
        }),
        !environment.production ? StoreDevtoolsModule.instrument() : [],
        //  EffectsModule.forFeature([MapEffects]),

        EffectsModule.forRoot([MapEffects])
    ],
    entryComponents: [SedeAnalisiDatiModalComponent],
    providers: [
        {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpInterceptorCustom,
            multi: true
        },
        {
            provide: LOCALE_ID,
            useValue: 'it-IT'
        }

    ],
    exports: [
        WorkInProgressComponent,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor() {
        console.log('App module');
        registerLocaleData(localeIt, 'it-IT', localeItExtra);

    }
}
