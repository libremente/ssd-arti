#!/usr/bin/env bash
git checkout production && git pull origin production && git merge dev -m "dev merged" --strategy-option theirs &&
  ng build --prod &&
  git add dist/ &&
  git commit -m "update build at $(date)" &&
  git push origin production &&
  git checkout dev &&
  echo "DONE"
